package com.amirhossein.myKonkorApp.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.amirhossein.myKonkorApp.R;
import com.amirhossein.myKonkorApp.model.DataFieldList;
import com.amirhossein.myKonkorApp.model.Field;
import com.amirhossein.myKonkorApp.widget.ShabnamTextView;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.PicassoProvider;

import static com.amirhossein.myKonkorApp.activity.Welcome.USER_INFO;

public class ReadField extends AppCompatActivity {

    ImageView img_field_read;
    FloatingActionButton buttonFavorite;
    AppBarLayout appbar_field_read;
    CollapsingToolbarLayout collapsing_toolbar_field;
    ShabnamTextView tv_title , tv_primaryEducation, tv_higherEducation, tv_summary, tv_jobs;
    LinearLayout ll_primaryEducation , ll_higherEducation;
    View v_p1,v_p2,v_h1,v_h2;

    int onOrOff ;
    int off = 1;
    int on = 2;
    int id;
    int idFav;
    int groupId;
    String title;
    Field field;


    public static final String PREFS_FIELD = "field_pref";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_field);
        findView();

        SharedPreferences preferences = getSharedPreferences(USER_INFO,MODE_PRIVATE);
        groupId = preferences.getInt("groupId",0);
        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        id = bundle.getInt("idField");
        title = bundle.getString("titleField");


        titleShow(title);
        field = DataFieldList.get().getDataSingle(id,groupId);

        String imageBaseUrl = "http://amirhosseinhamidian.ir/image/field_large/";
        Picasso.get().load(imageBaseUrl+field.getImageLarge()).into(img_field_read);
        tv_summary.setText(field.getDescription());
        tv_jobs.setText(field.getJobs());
        tv_primaryEducation.setText(field.getPrimaryEducation());
        tv_higherEducation.setText(field.getHigherEducation());
        if (tv_primaryEducation.getText().equals("ندارد")){
            ll_primaryEducation.setVisibility(View.GONE);
            tv_primaryEducation.setVisibility(View.GONE);
            v_p1.setVisibility(View.GONE);
            v_p2.setVisibility(View.GONE);
        }
        if (tv_higherEducation.getText().equals("ندارد")){
            ll_higherEducation.setVisibility(View.GONE);
            tv_higherEducation.setVisibility(View.GONE);
            v_h1.setVisibility(View.GONE);
            v_h2.setVisibility(View.GONE);
        }
        buttonFavoriteAction();

    }



    private void buttonFavoriteAction() {
        SharedPreferences preferences = getSharedPreferences(PREFS_FIELD,MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        idFav=preferences.getInt("field_favorite_id",-1);

        if (idFav == id ){
            buttonFavorite.setImageResource(R.drawable.ic_favorite_w);
            onOrOff = on;
        }else {
            onOrOff = off;
            buttonFavorite.setImageResource(R.drawable.ic_favorite_border);
        }
        buttonFavorite.setOnClickListener(view -> {
            if (onOrOff == off){
                buttonFavorite.setImageResource(R.drawable.ic_favorite_w);
                editor.putInt("field_favorite_id",id);
                editor.putInt("idField",id);
                editor.putString("titleField",title);
                editor.apply();

            }
            if (onOrOff == on){
                buttonFavorite.setImageResource(R.drawable.ic_favorite_border);
                editor.putInt("field_favorite_id",-1);
                editor.remove("idField");
                editor.remove("titleField");
                editor.apply();
            }

            if (onOrOff == off){
                onOrOff = on;
            }else {
                onOrOff = off;
            }
        });


    }


    private void findView() {
        img_field_read = findViewById(R.id.img_field_read);
        appbar_field_read = findViewById(R.id.appbar_field_read);
        collapsing_toolbar_field = findViewById(R.id.collapsing_toolbar_field);
        tv_title = findViewById(R.id.tv_title_filed);
        buttonFavorite = findViewById(R.id.btn_favorite);
        tv_summary = findViewById(R.id.tv_description_field_read);
        tv_jobs = findViewById(R.id.tv_description_field_read_2);
        tv_primaryEducation = findViewById(R.id.tv_description_field_read_3);
        tv_higherEducation = findViewById(R.id.tv_description_field_read_4);
        ll_primaryEducation = findViewById(R.id.ll_primary_education);
        ll_higherEducation = findViewById(R.id.ll_higher_education);
        v_h1 = findViewById(R.id.view_higher_education1);
        v_h2 = findViewById(R.id.view_higher_education2);
        v_p1 = findViewById(R.id.view_primary_education1);
        v_p2 = findViewById(R.id.view_primary_education2);
    }

    public static void startAlphaAnimation (View v, long duration, int visibility) {
        AlphaAnimation alphaAnimation = (visibility == View.VISIBLE)
                ? new AlphaAnimation(0f, 1f)
                : new AlphaAnimation(1f, 0f);

        alphaAnimation.setDuration(duration);
        alphaAnimation.setFillAfter(true);
        v.startAnimation(alphaAnimation);
    }

    private void titleShow(String title) {
        appbar_field_read.addOnOffsetChangedListener(new AppBarLayout.BaseOnOffsetChangedListener() {

            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int i) {

                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                    tv_title.setText(title);
                    startAlphaAnimation(tv_title,100,View.VISIBLE);
                }

                if (scrollRange + i == 0) {
                    collapsing_toolbar_field.setTitle(title);
                    isShow = true;
                    startAlphaAnimation(tv_title,100,View.INVISIBLE);

                } else if(isShow) {
                    collapsing_toolbar_field.setTitle("");
                    isShow = false;
                    startAlphaAnimation(tv_title,100,View.VISIBLE);
                }
            }
        });


    }


}
