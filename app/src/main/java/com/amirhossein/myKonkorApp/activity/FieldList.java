package com.amirhossein.myKonkorApp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.amirhossein.myKonkorApp.R;
import com.amirhossein.myKonkorApp.adapter.AdapterListField;
import com.amirhossein.myKonkorApp.model.DataFieldList;
import com.amirhossein.myKonkorApp.model.Field;
import com.amirhossein.myKonkorApp.widget.ShabnamTextView;

import java.util.List;

import static com.amirhossein.myKonkorApp.activity.Welcome.USER_INFO;

public class FieldList extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ShabnamTextView textViewTitle;

    int groupId;
    List<Field> fields;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filed_list);
        findView();

        SharedPreferences preferences = getSharedPreferences(USER_INFO,MODE_PRIVATE);
        groupId = preferences.getInt("groupId",0);

        switch (groupId){
            case 1:
                textViewTitle.setText("ریاضی و فیزیک");
                break;
            case 2:
                textViewTitle.setText("تجربی");
                break;
            case 3:
                textViewTitle.setText("انسانی");
                break;
        }
        fields = DataFieldList.get().getDataList(groupId);
        setupRecyclerView(fields);


    }





    private void setupRecyclerView(List<Field> fieldList) {
        AdapterListField adapter = new AdapterListField(getContext(),fieldList);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));
        recyclerView.setAdapter(adapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());


    }


    private Context getContext() {
        return FieldList.this;
    }

    private void findView() {
        recyclerView = findViewById(R.id.rv_field_list);
        textViewTitle =findViewById(R.id.tv_title_group);
    }


}
