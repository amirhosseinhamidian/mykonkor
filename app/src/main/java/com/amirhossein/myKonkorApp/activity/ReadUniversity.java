package com.amirhossein.myKonkorApp.activity;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.amirhossein.myKonkorApp.R;
import com.amirhossein.myKonkorApp.model.DataUniversityRead;
import com.amirhossein.myKonkorApp.model.University;
import com.amirhossein.myKonkorApp.webService.APIClient;
import com.amirhossein.myKonkorApp.webService.APIInterface;
import com.amirhossein.myKonkorApp.widget.ShabnamTextView;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import static com.amirhossein.myKonkorApp.activity.ReadField.startAlphaAnimation;

public class ReadUniversity extends AppCompatActivity {

    ImageView imageUniversity,locationUniversity;
    AppBarLayout appBarLayout;
    FloatingActionButton buttonFavorite;
    CollapsingToolbarLayout collapsingToolbarLayout;
    ShabnamTextView textViewTitle,textViewLink,textViewCity,
            textViewState,textViewRate,textViewDescription,
            textViewMoreLocation , textViewCollege;
    RelativeLayout relativeLayoutMore;

    int id ,position;
    String name , city , state , rate;
    University university;
    int onOrOff ;
    int off = 1;
    int on = 2;
    int idFav;

    public static final String PREFS_UNIVERSITY= "university_pref";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_university);
        findViews();

        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        id = bundle.getInt("id");
        position = bundle.getInt("position");
        name = bundle.getString("name");
        city = bundle.getString("city");
        state = bundle.getString("state");
        rate = bundle.getString("rate");


        university= DataUniversityRead.get().getData(position);

        String imageBaseUrl = "http://amirhosseinhamidian.ir/image/university_large/";
        Picasso.get().load(imageBaseUrl + university.getImageLarge()).into(imageUniversity);

        titleShow(name);
        textViewCity.setText(city);
        textViewState.setText(state);
        textViewRate.setText(rate);
        textViewLink.setText(university.getLink());
        textViewDescription.setText(university.getDescription());
        textViewCollege.setText(university.getCollege());
        textViewLink.setOnClickListener(new OnClickUrl(university));
        requestUniversity(id);
        buttonFavoriteAction();

    }

    private void buttonFavoriteAction() {

        SharedPreferences preferences = getSharedPreferences(PREFS_UNIVERSITY,MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        idFav=preferences.getInt("university_favorite_id",-1);
        if (idFav == position ){
            buttonFavorite.setImageResource(R.drawable.ic_favorite_w);
            onOrOff = on;
        }else {
            onOrOff = off;
            buttonFavorite.setImageResource(R.drawable.ic_favorite_border);
        }
        buttonFavorite.setOnClickListener(view -> {
            if (onOrOff == off){
                buttonFavorite.setImageResource(R.drawable.ic_favorite_w);
                editor.putInt("university_favorite_id",position);
                editor.putInt("id",id);
                editor.putInt("position",position);
                editor.putString("rate",rate);
                editor.putString("name",name);
                editor.putString("city",city);
                editor.putString("state",state);
                editor.apply();

            }
            if (onOrOff == on){
                buttonFavorite.setImageResource(R.drawable.ic_favorite_border);
                editor.putInt("university_favorite_id",-1);
                editor.remove("id");
                editor.remove("position");
                editor.remove("rate");
                editor.remove("name");
                editor.remove("city");
                editor.remove("state");
                editor.apply();
            }

            if (onOrOff == off){
                onOrOff = on;
            }else {
                onOrOff = off;
            }
        });
    }

    private void requestUniversity(int id) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<University> call = apiInterface.getUniversity(id);
        call.enqueue(new Callback<University>() {
            @Override
            public void onResponse(@NotNull Call<University> call, @NotNull Response<University> response) {
                if (response.isSuccessful()){
                    university = response.body();
                    assert university != null;

                    Picasso.get()
                            .load(university.getLocation())
                            .placeholder(R.drawable.location_wait)
                            .into(locationUniversity);
                    textViewMoreLocation.setOnClickListener(view -> {
                        String myLatitude = university.getLatitude() ;
                        String myLongitude = university.getLongitude();
                        String labelLocation = name;

                        Uri gmmIntentUri = Uri.parse("geo:<" + myLatitude  + ">,<" + myLongitude + ">?q=<" +
                                myLatitude  + ">,<" + myLongitude + ">(" + labelLocation + ")");

                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.setPackage("com.google.android.apps.maps");
                        if (mapIntent.resolveActivity(getPackageManager()) != null) {
                            startActivity(mapIntent);
                        }
                    });
                }
            }

            @Override
            public void onFailure(@NotNull Call<University> call, @NotNull Throwable t) {
                relativeLayoutMore.setVisibility(View.GONE);
            }
        });
    }

    private void titleShow(String title) {
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.BaseOnOffsetChangedListener() {

            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int i) {

                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                    textViewTitle.setText(title);
                    startAlphaAnimation(textViewTitle,100, View.VISIBLE);
                }

                if (scrollRange + i == 0) {
                    collapsingToolbarLayout.setTitle(title);
                    isShow = true;
                    startAlphaAnimation(textViewTitle,100,View.INVISIBLE);

                } else if(isShow) {
                    collapsingToolbarLayout.setTitle("");
                    isShow = false;
                    startAlphaAnimation(textViewTitle,100,View.VISIBLE);
                }
            }
        });


    }

    private void findViews() {
        imageUniversity = findViewById(R.id.img_university_read);
        appBarLayout = findViewById(R.id.appbar_university_read);
        collapsingToolbarLayout = findViewById(R.id.collapsing_toolbar_university);
        textViewTitle = findViewById(R.id.tv_name_university_read);
        textViewCity = findViewById(R.id.tv_city_university_read);
        textViewState = findViewById(R.id.tv_state_university_read);
        textViewRate = findViewById(R.id.tv_rate_university_read);
        textViewLink = findViewById(R.id.tv_link_university_read);
        textViewDescription = findViewById(R.id.tv_description_university_read);
        locationUniversity = findViewById(R.id.img_location_university);
        textViewMoreLocation = findViewById(R.id.tv_more_location);
        buttonFavorite = findViewById(R.id.btn_favorite_university);
        textViewCollege = findViewById(R.id.tv_description_university_read_2);
        relativeLayoutMore = findViewById(R.id.rl_more_location);
    }

    private class OnClickUrl implements View.OnClickListener {
        University university1;

        public OnClickUrl(University university1) {
            this.university1 = university1;
        }

        @Override
        public void onClick(View view) {
            String url = "http://"+university1.getLink();
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        }
    }
}
