package com.amirhossein.myKonkorApp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import com.amirhossein.myKonkorApp.R;
import com.amirhossein.myKonkorApp.adapter.AdapterListUniversity;
import com.amirhossein.myKonkorApp.model.DataUniversityList;
import com.amirhossein.myKonkorApp.model.University;
import com.amirhossein.myKonkorApp.widget.MyEditText;

import java.util.List;

public class UniversityList extends AppCompatActivity {


    MyEditText search;
    RecyclerView recyclerView;
    List<University> universityList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_university_list);
        findViews();
        universityList = DataUniversityList.get().getData();
        setupRecyclerView(universityList);

    }


    private void setupRecyclerView(List<University> universityList) {
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getContext(),R.anim.layout_animation);
        recyclerView.setLayoutAnimation(animation);
        AdapterListUniversity adapter = new AdapterListUniversity(getContext(),universityList );
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adapter.getFilter().filter(charSequence.toString());


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }



    private void findViews() {
        recyclerView = findViewById(R.id.rv_university_list);
        search = findViewById(R.id.edt_search_university_list);
    }

    private Context getContext(){
        return UniversityList.this;
    }
}
