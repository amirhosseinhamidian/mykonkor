package com.amirhossein.myKonkorApp.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

import com.amirhossein.myKonkorApp.R;
import com.amirhossein.myKonkorApp.webService.APIClient;
import com.amirhossein.myKonkorApp.webService.APIInterface;
import com.amirhossein.myKonkorApp.widget.MyButton;
import com.amirhossein.myKonkorApp.widget.Session;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.Random;

import static com.amirhossein.myKonkorApp.common.Constants.SIGN_IN_GOOGLE;
import static com.amirhossein.myKonkorApp.common.Constants.SIGN_IN_GUEST;

public class Welcome extends AppCompatActivity {

    MyButton enterGuest;
    RadioButton mathematics , empiric , humanities;
    LinearLayout googleLogin;
    RelativeLayout relativeLayout;

    int groupId;
    private static final int RC_SIGN_IN =1;
    @SuppressLint("StaticFieldLeak")
    static Welcome welcome;
    public static final String USER_INFO = "MyPrefs";
    GoogleSignInClient mGoogleSignInClient;
    GoogleSignInOptions gso;
    String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        findViews();
        typeFace();
        welcome = this;

        Session session = new Session(Welcome.this);
        if (session.isLoggedIn()){
            Intent intent = new Intent(Welcome.this,MainActivity.class);
            startActivity(intent);
            finish();
        }

        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        googleLogin.setOnClickListener(view -> {
            onRadioButtonClicked(view);

            if (groupId>0){
                signInGoogle();
                SharedPreferences.Editor editor = getSharedPreferences(USER_INFO, MODE_PRIVATE).edit();
                editor.putInt("groupId",groupId);
                editor.putString("signIn",SIGN_IN_GOOGLE);
                editor.apply();

            }else {
                Snackbar snackbar = Snackbar.make(relativeLayout,"رشته تحصیلی خودت رو انتخاب کن!",Snackbar.LENGTH_SHORT);
                snackbar.show();
            }

        });

        enterGuest.setOnClickListener(view -> {
            onRadioButtonClicked(view);
            if (groupId>0){
                @SuppressLint("HardwareIds")
                String android_id = Settings.Secure.getString(this.getContentResolver(),
                        Settings.Secure.ANDROID_ID);

                Random random = new Random();
                int chance = 1000+random.nextInt(9000);
                username = "مهمان-"+chance;
                registerUserGuest(username,groupId,android_id);

                SharedPreferences.Editor editor = getSharedPreferences(USER_INFO, MODE_PRIVATE).edit();
                editor.putInt("groupId",groupId);
                editor.putString("signIn",SIGN_IN_GUEST);
                editor.putString("androidId",android_id);
                editor.putString("username",username);
                editor.apply();

            }else {
                Snackbar snackbar = Snackbar.make(relativeLayout,"رشته تحصیلی خودت رو انتخاب کن!",Snackbar.LENGTH_SHORT);
                snackbar.show();
            }
        });
    }

    private void registerUserGuest(String username, int groupId, String android_id) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<Void> call = apiInterface.registerUserGuest(username,groupId,android_id, com.amirhossein.myKonkorApp.common.Constants.NOT_PROFILE_PICTURE);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NotNull Call<Void> call, @NotNull Response<Void> response) {
                if (response.isSuccessful()){
                    Intent intent = new Intent(getContext(),MainActivity.class);
                    startActivity(intent);
                    finish();
                    Intro.getInstance().finish();

                    Session session = new Session(getContext());
                    session.setLoggedIn(true);
                }
            }

            @Override
            public void onFailure(@NotNull Call<Void> call, @NotNull Throwable t) {
                Snackbar snackbar = Snackbar.make(relativeLayout,"اتصال اینترنت برقرار نیست!",Snackbar.LENGTH_SHORT);
                snackbar.show();

            }
        });
    }

    private void signInGoogle(){
        Intent intent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(intent,RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN){
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleResult(task);
        }
    }

    private void handleResult(Task<GoogleSignInAccount> task) {
        GoogleSignInAccount account = task.getResult();

        assert account != null;
        username = account.getGivenName();
        String email = Objects.requireNonNull(account.getEmail()).substring(0,account.getEmail().indexOf("@"));
        String image = String.valueOf(account.getPhotoUrl());
        registerUserGoogle(username,email,image);
        SharedPreferences.Editor editor = getSharedPreferences(USER_INFO, MODE_PRIVATE).edit();
        editor.putString("username",username);
        editor.putString("email",email);
        editor.putString("image",image);
        editor.apply();

    }

    private void registerUserGoogle(String username, String email, String image) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<Void> call = apiInterface.registerUserGoogle(username,groupId,email,image);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NotNull Call<Void> call, @NotNull Response<Void> response) {
                if (response.isSuccessful()) {
                    Intent intent = new Intent(getContext(),MainActivity.class);
                    startActivity(intent);
                    finish();
                    Intro.getInstance().finish();
                    Session session = new Session(getContext());
                    session.setLoggedIn(true);
                }
            }

            @Override
            public void onFailure(@NotNull Call<Void> call, @NotNull Throwable t) {
                Snackbar snackbar = Snackbar.make(relativeLayout,"اتصال اینترنت برقرار نیست!",Snackbar.LENGTH_SHORT);
                snackbar.show();
            }
        });
    }


    private void typeFace() {
        Typeface typeface = Typeface.createFromAsset(getAssets(),"IRANSansBold.ttf");
        mathematics.setTypeface(typeface);
        empiric.setTypeface(typeface);
        humanities.setTypeface(typeface);
    }

    private void findViews() {
        enterGuest = findViewById(R.id.btn_enter_guest);
        mathematics = findViewById(R.id.rb_math_sign_up);
        empiric = findViewById(R.id.rb_rpi_sign_up);
        humanities = findViewById(R.id.rb_hum_sign_up);
        googleLogin = findViewById(R.id.ll_login_google);
        relativeLayout = findViewById(R.id.rv_welcome);
    }

    public void onRadioButtonClicked(View view) {
        switch (view.getId()){
            case R.id.rb_math_sign_up:
                groupId = 1;
                break;

            case R.id.rb_rpi_sign_up:
                groupId = 2;
                break;

            case R.id.rb_hum_sign_up:
                groupId = 3;
                break;
        }
    }

    private Context getContext(){
        return Welcome.this;
    }

}
