package com.amirhossein.myKonkorApp.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.amirhossein.myKonkorApp.R;
import com.amirhossein.myKonkorApp.widget.Session;

public class Intro extends AppCompatActivity {

    private ViewPager viewPager;
    private Button next,skip;
    private LinearLayout layoutDots;
    private int[] layouts;
    @SuppressLint("StaticFieldLeak")
    static Intro intro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        findViews();
        intro = this;

        Session session = new Session(getContext());
        if (session.isLoggedIn()){
            Intent intent = new Intent(getContext(),MainActivity.class);
            startActivity(intent);
            finish();
        }

        Typeface typeface = Typeface.createFromAsset(getAssets(),"IRANSansBold.ttf");
        next.setTypeface(typeface);
        skip.setTypeface(typeface);
        next.setTextSize(18);
        skip.setTextSize(18);


        layouts = new int[]{
                R.layout.welcome_1,
                R.layout.welcome_2,
                R.layout.welcome_3,
                R.layout.welcome_4};

        addBottomDots(0);
        MyViewPagerAdapter myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerListener);

        skip.setOnClickListener(view -> {
            Intent intent = new Intent(getContext(),Welcome.class);
            startActivity(intent);

        });

        next.setOnClickListener(view -> {
            int current = getItem();
            if (current < layouts.length) {
                viewPager.setCurrentItem(current);
                addBottomDots(current);
            } else {
                Intent intent = new Intent(getContext(),Welcome.class);
                startActivity(intent);

            }
        });
    }

    private int getItem() {
        return viewPager.getCurrentItem() + 1;
    }

    ViewPager.OnPageChangeListener viewPagerListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);

            if (position == layouts.length - 1) {

                next.setText("شروع");
                skip.setVisibility(View.GONE);

            } else {

                next.setText(getString(R.string.next));
                skip.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    private void addBottomDots(int pageActive){
        TextView[] dots = new TextView[layouts.length];
        layoutDots.removeAllViews();
        for (int i = 0; i< dots.length; i++){
            dots[i] = new TextView(getContext());
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(Color.parseColor("#DBDCFD"));
            layoutDots.addView(dots[i]);
        }

        if (dots.length>0){
            dots[pageActive].setTextColor(Color.parseColor("#3E50FD"));
        }
    }

    private void findViews() {
        viewPager = findViewById(R.id.view_pager_intro);
        next = findViewById(R.id.btn_next_intro);
        skip = findViewById(R.id.btn_skip_intro);
        layoutDots = findViewById(R.id.layout_dots_intro);
    }

    private Context getContext(){
        return Intro.this;
    }

    private class MyViewPagerAdapter extends PagerAdapter {
        MyViewPagerAdapter() {
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            assert layoutInflater != null;
            View view = layoutInflater.inflate(layouts[position],container,false);
            container.addView(view);
            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }

    public static Intro getInstance(){
        return intro;
    }

}
