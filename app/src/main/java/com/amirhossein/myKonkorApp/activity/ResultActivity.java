package com.amirhossein.myKonkorApp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.view.animation.LinearInterpolator;
import android.widget.LinearLayout;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.amirhossein.myKonkorApp.R;
import com.amirhossein.myKonkorApp.adapter.AdapterDailyRate;
import com.amirhossein.myKonkorApp.adapter.AdapterRateList;
import com.amirhossein.myKonkorApp.adapter.AdapterResultList;
import com.amirhossein.myKonkorApp.model.DatabaseHelperScore;
import com.amirhossein.myKonkorApp.model.Score;
import com.amirhossein.myKonkorApp.model.Test;
import com.amirhossein.myKonkorApp.model.User;
import com.amirhossein.myKonkorApp.webService.APIClient;
import com.amirhossein.myKonkorApp.webService.APIInterface;
import com.amirhossein.myKonkorApp.widget.IranSansBold;
import com.amirhossein.myKonkorApp.widget.MyButton;
import com.amirhossein.myKonkorApp.widget.ShabnamTextView;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.amirhossein.myKonkorApp.activity.Welcome.USER_INFO;
import static com.amirhossein.myKonkorApp.common.Constants.EDIT_PROFILE_PICTURE;
import static com.amirhossein.myKonkorApp.common.Constants.SIGN_IN_GOOGLE;
import static com.amirhossein.myKonkorApp.common.Constants.SIGN_IN_GUEST;
import static com.amirhossein.myKonkorApp.common.Constants.SOUND;
import static com.amirhossein.myKonkorApp.fragment.navigation.FragmentHome.SHARED_PREFS;

public class ResultActivity extends AppCompatActivity {

    int correct , wrong , numQuiz ,empty,remainingTime ,topicId;
    float multiDay;
    float usageTime;
    float maxScore;
    int totalScoreDay;
    int groupId;
    int day =0;
    int weekCounter=0;
    int monthCounter=0;
    int myWeekScore;
    int myMonthScore;
    int myTotalScore;
    int myWeeekRank;
    long remainingTimeDay;
    long getRemainingTimeDay;
    long getWeekendTime;
    long getEndOfMonth;
    long remainingWeekendTime =0;
    long remainingEndOfMonth=0;
    String email;
    String typeSignIn;
    ArrayList<Integer> scores ;

    private IranSansBold tv_rate;
    private MyButton continueButton;
    private LinearLayout resultExam , closeResultExam;
    private PieChart pieChart;
    private RoundCornerProgressBar progressBarRate;
    private RecyclerView recyclerViewRate,recyclerViewResult,recyclerViewDailyRate;
    private ShabnamTextView textViewMyNameRate, textViewMyRank, textViewMyScore;
    private LinearLayout linearLayoutImage,linearLayoutMyScore;
    private CircleImageView imageProfile;

    public static final String PREF_RATE = "examPreferences";
    SharedPreferences.Editor editor;
    SharedPreferences prefer;


    MediaPlayer mediaPlayer;
    ArrayList<Test> tests;
    ArrayList<String> correctList;
    ArrayList<String> userAnswerList;
    private DatabaseHelperScore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        findViews();
        appBarChange();

        @SuppressLint("HardwareIds")
        String android_id = Settings.Secure.getString(getContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);

        db = new DatabaseHelperScore(getContext());
        Bundle bundle = getIntent().getExtras();
        Bundle bundle1 = getIntent().getExtras();

        assert bundle != null;
        correct = bundle.getInt("correctAns");
        wrong = bundle.getInt("wrongAns");
        numQuiz = bundle.getInt("numQuiz");
        topicId = bundle.getInt("topicId");
        remainingTime = bundle.getInt("remainingTime");
        tests = (ArrayList<Test>) bundle1.getSerializable("examList");
        userAnswerList = (ArrayList<String>) bundle1.getSerializable("userAnswerList");
        correctList = (ArrayList<String>) bundle1.getSerializable("correctList");
        empty = numQuiz - (correct+wrong);
        usageTime = (float) (remainingTime*5)/numQuiz;

        prefer = getSharedPreferences(USER_INFO,MODE_PRIVATE);
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences preferences = getSharedPreferences(PREF_RATE,MODE_PRIVATE);
        editor = preferences.edit();

        groupId = prefer.getInt("groupId",0);
        typeSignIn = prefer.getString("signIn",SIGN_IN_GUEST);
        email = prefer.getString("email","");
        multiDay = sharedPreferences.getFloat("multi",1);
        remainingTimeDay = preferences.getLong("remainingTimeDay",0);
        totalScoreDay = preferences.getInt("totalScoreDay",0);
        remainingWeekendTime = preferences.getLong("weekendTime",0);
        remainingEndOfMonth = preferences.getLong("endOfMonth",0);
        weekCounter = preferences.getInt("weekCounter",0);
        monthCounter = preferences.getInt("monthCounter",0);

        getRemainingTimeDay = remainingTimeDay - timeNow();
        getWeekendTime = remainingWeekendTime - timeNow();
        getEndOfMonth = remainingEndOfMonth - timeNow();


        //management time and forward data to server
        if (getRemainingTimeDay<0){
            if (getWeekendTime<0){
                if (typeSignIn.equals(SIGN_IN_GOOGLE)){
                    requestUpdateWeekScoreByEmail(0,email);
                }else {
                    requestUpdateWeekScore(0, android_id);
                }
                remainingWeekendTime = timeNow()+remainingTimeUntilWeekend();
                weekCounter++;
                editor.putLong("weekendTime",remainingWeekendTime);
                editor.putInt("weekCounter",weekCounter);
                editor.apply();
            }
            if (getEndOfMonth<0){

                if (typeSignIn.equals(SIGN_IN_GOOGLE)){
                    requestUpdateMonthScoreByEmail(0,email);
                }else {
                    requestUpdateMonthScore(0, android_id);
                }
                remainingEndOfMonth = timeNow() + remainingTimeUntilMonth();
                monthCounter++;
                editor.putLong("endOfMonth",remainingEndOfMonth);
                editor.putInt("monthCounter",monthCounter);
                editor.apply();
            }


            day = preferences.getInt("day",0);
            day++;
            editor.putInt("day",day);
            editor.apply();
            totalScoreDay = 0;
            db.insertScore(totalScoreDay,day,weekCounter,monthCounter);

            long timeUntilMidnight = (86400 - remainingTime()) ;
            remainingTimeDay = timeNow()+timeUntilMidnight;
            editor.putLong("remainingTimeDay",remainingTimeDay);
            editor.putInt("totalScoreDay",totalScoreDay);
            editor.apply();

        }
        int s =calculateRate();
        getMyScore(android_id,s);
        chartResult();

        getMyScore(android_id,s);
        keyResultButton();
        dailyRate();

        continueButton.setOnClickListener(view ->{

            finish();
        });

    }

    private void getMyScore(String android_id,int score) {
        SharedPreferences prefer = getSharedPreferences(USER_INFO,MODE_PRIVATE);
        String email = prefer.getString("email","");
        String typeSignIn = prefer.getString("signIn",SIGN_IN_GUEST);

        if (typeSignIn.equals(SIGN_IN_GOOGLE)) {

            APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
            Call<User> call = apiInterface.getMyAllScoreByEmail(email,groupId);
            call.enqueue(new Callback<User>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(@NotNull Call<User> call, @NotNull Response<User> response) {
                    if (response.isSuccessful()) {
                        User user = response.body();

                        assert user != null;
                        myWeekScore =user.getScoreWeek()+score;
                        myMonthScore=user.getScoreMonth()+score;
                        myTotalScore=user.getScoreTotal()+score;
                        myWeeekRank=user.getRankWeek();
                        updateScore(myWeekScore,myMonthScore,myTotalScore);

                        }
                    }

                @Override
                public void onFailure(@NotNull Call<User> call, @NotNull Throwable t) {

                }
            });
        }else {
            APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
            Call<User> call = apiInterface.getMyAllScore(android_id,groupId);
            call.enqueue(new Callback<User>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(@NotNull Call<User> call, @NotNull Response<User> response) {
                    if (response.isSuccessful()) {
                        User user = response.body();

                        assert user != null;
                        myWeekScore=user.getScoreWeek()+score;
                        myMonthScore=user.getScoreMonth()+score;
                        myTotalScore=user.getScoreTotal()+score;
                        myWeeekRank=user.getRankWeek();
                        updateScore(myWeekScore,myMonthScore,myTotalScore);

                    }
                }

                @Override
                public void onFailure(@NotNull Call<User> call, @NotNull Throwable t) {

                }
            });
        }
    }

    private void requestUpdateTotalScoreByEmail(int totalScore, String email) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<Void> call = apiInterface.updateTotalScoreByEmail(totalScore,email);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NotNull Call<Void> call, @NotNull Response<Void> response) {

            }

            @Override
            public void onFailure(@NotNull Call<Void> call, @NotNull Throwable t) {

            }
        });
    }

    private void requestUpdateMonthScoreByEmail(int totalMonth, String email) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<Void> call = apiInterface.updateMonthScoreByEmail(totalMonth,email);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NotNull Call<Void> call, @NotNull Response<Void> response) {

            }

            @Override
            public void onFailure(@NotNull Call<Void> call, @NotNull Throwable t) {

            }
        });
    }

    private void requestUpdateWeekScoreByEmail(int totalWeek, String email) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<Void> call = apiInterface.updateWeekScoreByEmail(totalWeek,email);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NotNull Call<Void> call, @NotNull Response<Void> response) {
                requestMyScore();
                requestRateWeekly();
            }

            @Override
            public void onFailure(@NotNull Call<Void> call, @NotNull Throwable t) {

            }
        });
    }

    private void requestMyScore() {
        SharedPreferences prefer = getSharedPreferences(USER_INFO,MODE_PRIVATE);
        String email = prefer.getString("email","");
        String typeSignIn = prefer.getString("signIn",SIGN_IN_GUEST);

        if (typeSignIn.equals(SIGN_IN_GOOGLE)) {

            APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
            Call<User> call = apiInterface.getMyScoreWeeklyByEmail(email,groupId);
            call.enqueue(new Callback<User>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(@NotNull Call<User> call, @NotNull Response<User> response) {
                    if (response.isSuccessful()) {
                        User user = response.body();
                        assert user != null;
                        if (user.getRankWeek() > 3) {
                            linearLayoutMyScore.setVisibility(View.VISIBLE);
                            linearLayoutImage.setVisibility(View.VISIBLE);
                            textViewMyNameRate.setText(user.getUsername());
                            textViewMyRank.setText(user.getRankWeek() + "");
                            textViewMyScore.setText(user.getScoreWeek() + "");
                            linearLayoutMyScore.setBackgroundResource(R.color.colorPrimaryLight);

                            Picasso.get()
                                    .load(user.getImage())
                                    .error(R.drawable.notprofile)
                                    .placeholder(R.drawable.notprofile)
                                    .into(imageProfile);
                        } else {
                            linearLayoutMyScore.setVisibility(View.GONE);
                            linearLayoutImage.setVisibility(View.GONE);
                        }
                    }
                }

                @Override
                public void onFailure(@NotNull Call<User> call, @NotNull Throwable t) {

                }
            });
        }else {

            @SuppressLint("HardwareIds")
            String android_id = Settings.Secure.getString(getContext().getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
            Call<User> call = apiInterface.getMyScoreWeekly(android_id,groupId);
            call.enqueue(new Callback<User>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(@NotNull Call<User> call, @NotNull Response<User> response) {
                    if (response.isSuccessful()) {
                        User user = response.body();
                        assert user != null;
                        if (user.getRankWeek() > 3) {
                            linearLayoutMyScore.setVisibility(View.VISIBLE);
                            linearLayoutImage.setVisibility(View.VISIBLE);
                            textViewMyNameRate.setText(user.getUsername());
                            textViewMyRank.setText(user.getRankWeek() + "");
                            textViewMyScore.setText(user.getScoreWeek() + "");
                            linearLayoutMyScore.setBackgroundResource(R.color.colorPrimaryLight);
                            Picasso.get()
                                    .load(user.getImage())
                                    .error(R.drawable.notprofile)
                                    .placeholder(R.drawable.notprofile)
                                    .into(imageProfile);
                        } else {
                            linearLayoutMyScore.setVisibility(View.GONE);
                            linearLayoutImage.setVisibility(View.GONE);
                        }
                    }
                }

                @Override
                public void onFailure(@NotNull Call<User> call, @NotNull Throwable t) {

                }
            });
        }
    }

    private void requestUpdateMonthScore(int monthScore,String android_id) {

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<Void> call = apiInterface.updateMonthScore(monthScore,android_id);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NotNull Call<Void> call, @NotNull Response<Void> response) {

            }

            @Override
            public void onFailure(@NotNull Call<Void> call, @NotNull Throwable t) {

            }
        });
    }

    private void requestUpdateWeekScore(int WeekScore,String android_id) {

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<Void> call = apiInterface.updateWeekScore(WeekScore,android_id);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NotNull Call<Void> call, @NotNull Response<Void> response) {
                requestMyScore();
                requestRateWeekly();
            }

            @Override
            public void onFailure(@NotNull Call<Void> call, @NotNull Throwable t) {

            }
        });
    }

    private void requestUpdateTotalScore(int totalScore,String android_id) {

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<Void> call = apiInterface.updateTotalScore(totalScore,android_id);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NotNull Call<Void> call, @NotNull Response<Void> response) {

            }

            @Override
            public void onFailure(@NotNull Call<Void> call, @NotNull Throwable t) {

            }
        });
    }

    private void dailyRate() {
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getContext(),R.anim.layout_animation);

        List<Score> scores = db.getAllScore();
        recyclerViewDailyRate.setLayoutManager(new LinearLayoutManager(getContext(),RecyclerView.HORIZONTAL,true));
        recyclerViewDailyRate.getRecycledViewPool();
        recyclerViewDailyRate.setAdapter(new AdapterDailyRate(getContext(),scores));
        recyclerViewDailyRate.setLayoutAnimation(animation);

    }

    private void keyResultButton() {
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getContext(),R.anim.layout_animation);
        resultExam.setOnClickListener(view -> {
            recyclerViewResult.setVisibility(View.VISIBLE);
            recyclerViewResult.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerViewResult.setAdapter(new AdapterResultList(getContext(),correctList,userAnswerList));
            closeResultExam.setVisibility(View.VISIBLE);
            resultExam.setVisibility(View.GONE);
            recyclerViewResult.setLayoutAnimation(animation);

        });

        closeResultExam.setOnClickListener(view -> {
            recyclerViewResult.setVisibility(View.GONE);
            closeResultExam.setVisibility(View.GONE);
            resultExam.setVisibility(View.VISIBLE);

        });
    }

    private void appBarChange() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Window window = this.getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this,R.color.colorPrimaryLight));
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
    }

    private void requestRateWeekly() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<List<User>> call = apiInterface.getWeeklyScore(groupId);
        call.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(@NotNull Call<List<User>> call, @NotNull Response<List<User>> response) {
                if (response.isSuccessful()){
                    List<User> userList = response.body();
                    setupRecyclerView(userList);
                }
            }

            @Override
            public void onFailure(@NotNull Call<List<User>> call, @NotNull Throwable t) {

            }
        });
    }

    private void setupRecyclerView(List<User> userList) {
        recyclerViewRate.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewRate.setAdapter(new AdapterRateList(getContext(),userList,3,User.WEEK_REQUEST));
    }

    private int calculateRate() {
        float score=0;
        float multiTopic=0;
        int roundScore;
        switch (topicId){
            case 1 :
                if (usageTime > 165 ){
                    multiTopic = 2.5f;
                }else if (usageTime<=165 && usageTime>110){
                    multiTopic = 2f;
                }else if (usageTime<=110 && usageTime>55){
                    multiTopic = 1.5f;
                }else if (usageTime<=55){
                    multiTopic = 1f;
                }
                break;

            case 2 :
                switch (groupId){
                    case 1:
                        if (usageTime>275 ){
                            multiTopic = 3.5f;
                        }else if (usageTime<=275 && usageTime>180){
                            multiTopic = 3f;
                        }else if (usageTime<=180 && usageTime>171){
                            multiTopic = 2.5f;
                        }else if (usageTime<=171 && usageTime>85){
                            multiTopic = 2.5f;
                        }else if (usageTime<=85 && usageTime>40){
                            multiTopic = 2f;
                        }else if (usageTime<=40){
                            multiTopic = 1.5f;
                        }
                        break;

                    case 2:
                        if (usageTime>240 ){
                            multiTopic = 3.5f;
                        }else if (usageTime<=240 && usageTime>160){
                            multiTopic = 3f;
                        }else if (usageTime<=160 && usageTime>80){
                            multiTopic = 2.5f;
                        }else if (usageTime<=80 && usageTime>40){
                            multiTopic = 2f;
                        }else if (usageTime<=40){
                            multiTopic = 1.5f;
                        }
                        break;

                    case 3:
                        if (usageTime>202 ){
                            multiTopic = 3.5f;
                        }else if (usageTime<=202 && usageTime>134){
                            multiTopic = 3f;
                        }else if (usageTime<=134 && usageTime>66){
                            multiTopic = 2.5f;
                        }else if (usageTime<=66 && usageTime>35){
                            multiTopic = 2f;
                        }else if (usageTime<=35){
                            multiTopic = 1.5f;
                        }
                        break;
                }
                break;
            case 3 :
                switch (groupId){
                    case 1:
                        if (usageTime>200 ){
                            multiTopic = 3f;
                        }else if (usageTime<=200 && usageTime>100){
                            multiTopic = 2.5f;
                        }else if (usageTime<=100 && usageTime>45){
                            multiTopic = 2f;
                        }else if (usageTime<=45 ){
                            multiTopic = 1.5f;
                        }
                        break;

                    case 2:
                        if (usageTime>185 ){
                            multiTopic = 3f;
                        }else if (usageTime<=185 && usageTime>95){
                            multiTopic = 2.5f;
                        }else if (usageTime<=95 && usageTime>35){
                            multiTopic = 2f;
                        }else if (usageTime<=35 ){
                            multiTopic = 1.5f;
                        }
                        break;

                    case 3:
                        if (usageTime>175 ){
                            multiTopic = 3f;
                        }else if (usageTime<=175 && usageTime>90){
                            multiTopic = 2.5f;
                        }else if (usageTime<=90 && usageTime>35){
                            multiTopic = 2f;
                        }else if (usageTime<=35 ){
                            multiTopic = 1.5f;
                        }
                        break;
                }
                break;
        }

        if (percentResult() > 80){
            score = 100*multiDay*multiTopic;
        }else if (percentResult()<=80 && percentResult()>70){
            score = 85*multiDay*multiTopic;
        }else if (percentResult()<=70 && percentResult()>60){
            score = 70*multiDay*multiTopic;
        }else if (percentResult()<=60 && percentResult()>50){
            score = 55*multiDay*multiTopic;
        }else if (percentResult()<=50 && percentResult()>40){
            score = 40*multiDay*multiTopic;
        }else if (percentResult()<=40 && percentResult()>30){
            score = 25*multiDay*multiTopic;
        }else if (percentResult()<=30 && percentResult()>20){
            score = 15*multiDay*multiTopic;
        }else if (percentResult()<=20 && percentResult()>5){
            score = 10*multiDay*multiTopic;
        }
        roundScore = Math.round(score);
        tv_rate.setText(String.valueOf(roundScore));
        progressBarRate(roundScore);

        totalScoreDay += roundScore;
        editor.putInt("totalScoreDay",totalScoreDay);
        editor.apply();
        if (db.columnExists()){
            Score score1 =db.getLastScore();
            db.updateScore(score1.getId(),totalScoreDay);
        }

        boolean sound = prefer.getBoolean(SOUND,true);
        if (sound){
            if (percentResult() >= 40){
                mediaPlayer = MediaPlayer.create(getContext(),R.raw.win_sound);
                mediaPlayer.start();
            }else {
                mediaPlayer = MediaPlayer.create(getContext(),R.raw.game_over);
                mediaPlayer.start();
            }
        }
        return roundScore;
    }

    private void updateScore(int myWeekScore, int myMonthScore, int myTotalScore) {
        SharedPreferences prefer = getSharedPreferences(USER_INFO,MODE_PRIVATE);
        String email = prefer.getString("email","");
        String typeSignIn = prefer.getString("signIn",SIGN_IN_GUEST);
        @SuppressLint("HardwareIds")
        String android_id = Settings.Secure.getString(getContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);

        if (typeSignIn.equals(SIGN_IN_GOOGLE)){
            requestUpdateTotalScoreByEmail(myTotalScore,email);
            requestUpdateWeekScoreByEmail(myWeekScore,email);
            requestUpdateMonthScoreByEmail(myMonthScore,email);
        }else {
            requestUpdateTotalScore(myTotalScore, android_id);
            requestUpdateWeekScore(myWeekScore, android_id);
            requestUpdateMonthScore(myMonthScore, android_id);

        }
    }

    private void progressBarRate(float rate) {
        maxScore = 0;
        switch (topicId){
            case 1:
                maxScore = 100*multiDay*2.5f;
                break;
            case 2:
                maxScore = 100*multiDay*3.5f;
                break;
            case 3:
                maxScore = 100*multiDay*3f;
                break;
        }

        progressBarRate.setProgressColor(Color.parseColor("#5362FB"));
        progressBarRate.setProgressBackgroundColor(Color.parseColor("#F4F5F9"));
        progressBarRate.setMax(maxScore);
        progressBarRate.setProgress(rate);
        ObjectAnimator progressAnimator = ObjectAnimator.ofFloat(progressBarRate,"progress",0,rate);
        progressAnimator.setDuration(800);
        progressAnimator.setInterpolator(new LinearInterpolator());
        progressAnimator.start();

    }

    private void chartResult() {
        Typeface typeface = Typeface.createFromAsset(getAssets(),"IRANSans .ttf");
        Typeface typefaceBold = Typeface.createFromAsset(getAssets(),"IRANSansBold.ttf");
        ValueFormatter formatter = new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return String.valueOf((int) value);
            }
        };
        pieChart.spin(1200,0,-180f, Easing.EaseInSine);
        pieChart.setUsePercentValues(false);
        pieChart.setEntryLabelTypeface(typeface);
        pieChart.getDescription().setEnabled(false);
        pieChart.getLegend().setEnabled(false);
        pieChart.setHoleRadius(65f);
        pieChart.setEntryLabelTextSize(18f);
        pieChart.setTransparentCircleRadius(65f);
        pieChart.setDrawEntryLabels(false);
        pieChart.getLegend().setFormSize(12);
        pieChart.getLegend().setTextSize(16);
        pieChart.getLegend().setFormToTextSpace(10);
        pieChart.getLegend().setXEntrySpace(36);
        pieChart.getLegend().setTypeface(typeface);
        pieChart.setCenterTextColor(Color.parseColor("#5362FB"));
        pieChart.setCenterText(percentResult()+"%");
        pieChart.setCenterTextTypeface(typefaceBold);
        pieChart.setCenterTextSize(40);

        ArrayList<PieEntry> zon = new ArrayList<>();
        ArrayList<Integer> colors = new ArrayList<>();

        if (correct > 0){
            zon.add(new PieEntry(correct,"درست"));
            colors.add(Color.parseColor("#2e7d32"));
        }
        if (wrong > 0){
            zon.add(new PieEntry(wrong,"نادرست"));
            colors.add(Color.parseColor("#c62828"));
        }
        if (empty > 0){
            zon.add(new PieEntry(empty,"نزده"));
            colors.add(Color.parseColor("#f9a825"));
        }

        PieDataSet dataSet = new PieDataSet(zon,"");
        dataSet.setSliceSpace(2f);
        dataSet.setValueTypeface(typeface);
        dataSet.setColors(colors);
        dataSet.setValueFormatter(formatter);
        PieData data = new PieData((dataSet));

        data.setValueTextSize(28f);
        data.setValueTextColor(Color.WHITE);

        pieChart.setData(data);

    }

    private double percentResult() {
        double result;
        result = (((double)correct - ((double)wrong/3))/(double) numQuiz)*100;
        return round(result,2);
    }

    private static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    private long remainingTime(){
        Calendar rightNow = Calendar.getInstance();


        long offset = rightNow.get(Calendar.ZONE_OFFSET) +
                rightNow.get(Calendar.DST_OFFSET);

        long sinceMidnight = (rightNow.getTimeInMillis() + offset) %
                (24 * 60 * 60 * 1000);

        return  sinceMidnight/1000;
    }

    private long remainingTimeUntilWeekend(){
        Calendar calendar = Calendar.getInstance();
        long offset = calendar.get(Calendar.ZONE_OFFSET) + calendar.get(Calendar.DST_OFFSET);
        int fridayInMonth = calendar.get(Calendar.DAY_OF_MONTH) + (Calendar.FRIDAY - calendar.get(Calendar.DAY_OF_WEEK));
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), fridayInMonth,
                23, 59, 59);
        return ((calendar.getTimeInMillis()+offset)/1000)-timeNow();
    }

    private long remainingTimeUntilMonth(){
        Calendar calendar = Calendar.getInstance();
        long offset = calendar.get(Calendar.ZONE_OFFSET) + calendar.get(Calendar.DST_OFFSET);
        int lastDayOfMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), lastDayOfMonth,
                23, 59, 59);
        return ((calendar.getTimeInMillis()+offset)/1000)-timeNow();

    }

    private long timeNow() {

        Calendar nowGMT = Calendar.getInstance();
        long offset = nowGMT.get(Calendar.ZONE_OFFSET) +
                nowGMT.get(Calendar.DST_OFFSET);

        return  (nowGMT.getTimeInMillis() + offset)/1000;
    }

    private void findViews() {
        resultExam = findViewById(R.id.ll_show_more_result);
        pieChart = findViewById(R.id.pie_chart_result_exam);
        progressBarRate = findViewById(R.id.pb_rate_exam_result);
        tv_rate = findViewById(R.id.tv_rate_exam_result);
        recyclerViewRate = findViewById(R.id.rv_rates_exam_result);
        recyclerViewResult = findViewById(R.id.rv_result_test);
        closeResultExam = findViewById(R.id.ll_close_more_result);
        recyclerViewDailyRate =findViewById(R.id.rv_daily_rate);
        textViewMyScore = findViewById(R.id.tv_score_rate_weekly_result);
        textViewMyNameRate = findViewById(R.id.tv_name_rate_weekly_result);
        textViewMyRank = findViewById(R.id.tv_num_rate_weekly_result);
        linearLayoutImage = findViewById(R.id.ll_image_until_my_rank_result);
        linearLayoutMyScore = findViewById(R.id.ll_my_score_weekly_result);
        continueButton = findViewById(R.id.btn_continue_result);
        imageProfile = findViewById(R.id.img_profile_rate_result);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private Context getContext() {
        return ResultActivity.this;
    }
}
