package com.amirhossein.myKonkorApp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.amirhossein.myKonkorApp.R;
import com.amirhossein.myKonkorApp.fragment.navigation.BaseFragment;
import com.amirhossein.myKonkorApp.fragment.navigation.FragmentArticle;
import com.amirhossein.myKonkorApp.fragment.navigation.FragmentHome;
import com.amirhossein.myKonkorApp.fragment.navigation.FragmentProfile;
import com.amirhossein.myKonkorApp.fragment.navigation.FragmentRating;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Stack;

import static com.amirhossein.myKonkorApp.common.Constants.ACTION;
import static com.amirhossein.myKonkorApp.common.Constants.DATA_KEY_1;
import static com.amirhossein.myKonkorApp.common.Constants.DATA_KEY_2;
import static com.amirhossein.myKonkorApp.common.Constants.EXTRA_IS_ROOT_FRAGMENT;
import static com.amirhossein.myKonkorApp.common.Constants.TAB_ARTICLE;
import static com.amirhossein.myKonkorApp.common.Constants.TAB_HOME;
import static com.amirhossein.myKonkorApp.common.Constants.TAB_PROFILE;
import static com.amirhossein.myKonkorApp.common.Constants.TAB_RATING;
import static com.amirhossein.myKonkorApp.fragment.navigation.utils.FragmentUtils.addAdditionalTabFragment;
import static com.amirhossein.myKonkorApp.fragment.navigation.utils.FragmentUtils.addInitialTabFragment;
import static com.amirhossein.myKonkorApp.fragment.navigation.utils.FragmentUtils.addShowHideFragment;
import static com.amirhossein.myKonkorApp.fragment.navigation.utils.FragmentUtils.removeFragment;
import static com.amirhossein.myKonkorApp.fragment.navigation.utils.FragmentUtils.showHideTabFragment;
import static com.amirhossein.myKonkorApp.fragment.navigation.utils.StackListManager.updateStackIndex;
import static com.amirhossein.myKonkorApp.fragment.navigation.utils.StackListManager.updateStackToIndexFirst;
import static com.amirhossein.myKonkorApp.fragment.navigation.utils.StackListManager.updateTabStackIndex;

public class MainActivity extends AppCompatActivity implements BaseFragment.FragmentInteractionCallback {
    BottomNavigationView bn_navi;
    private Map<String, Stack<String>> tagStacks;
    private String currentTab;
    private List<String> stackList;
    private List<String> menuStacks;
    private Fragment currentFragment;
    private Fragment fragmentHome;
    private Fragment fragmentArticle;
    private Fragment fragmentRating;
    private Fragment fragmentProfile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findView();

        createStacks();



        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Window window = this.getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this,R.color.colorPrimaryLight));
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        }


    }

    private void findView() {
        bn_navi = findViewById(R.id.bn_navi);
    }


    private void createStacks() {
        bn_navi.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener);

        fragmentHome = FragmentHome.newInstance(true);
        fragmentArticle = FragmentArticle.newInstance(true);
        fragmentRating = FragmentRating.newInstance(true);
        fragmentProfile = FragmentProfile.newInstance(true);

        tagStacks = new LinkedHashMap<>();
        tagStacks.put(TAB_HOME, new Stack<>());
        tagStacks.put(TAB_ARTICLE, new Stack<>());
        tagStacks.put(TAB_RATING, new Stack<>());
        tagStacks.put(TAB_PROFILE, new Stack<>());

        menuStacks = new ArrayList<>();
        menuStacks.add(TAB_HOME);

        stackList = new ArrayList<>();
        stackList.add(TAB_HOME);
        stackList.add(TAB_ARTICLE);
        stackList.add(TAB_RATING);
        stackList.add(TAB_PROFILE);

        bn_navi.setSelectedItemId(R.id.fragment_home);
        bn_navi.setOnNavigationItemReselectedListener(onNavigationItemReselectedListener);
    }

    private final BottomNavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener = item -> {
        switch (item.getItemId()) {
            case R.id.fragment_home:
                selectedTab(TAB_HOME);
                return true;
            case R.id.fragment_article:
                selectedTab(TAB_ARTICLE);
                return true;
            case R.id.fragment_rating:
                selectedTab(TAB_RATING);
                return true;
            case R.id.fragment_profile:
                selectedTab(TAB_PROFILE);
                return true;
        }
        return false;
    };

    private final BottomNavigationView.OnNavigationItemReselectedListener onNavigationItemReselectedListener = menuItem -> {
        switch (menuItem.getItemId()) {
            case R.id.fragment_home:
                popStackExceptFirst();
                break;
            case R.id.fragment_article:
                popStackExceptFirst();
                break;
            case R.id.fragment_rating:
                popStackExceptFirst();
                break;
            case R.id.fragment_profile:
                popStackExceptFirst();
                break;
        }
    };

    @Override
    public void onBackPressed() {
        resolveBackPressed();

    }

    private void resolveBackPressed() {
        int stackValue = 0;
        if (Objects.requireNonNull(tagStacks.get(currentTab)).size() == 1) {
            Stack<String> value = tagStacks.get(stackList.get(1));
            assert value != null;
            if (value.size() > 1) {
                stackValue = value.size();
                popAndNavigateToPreviousMenu();
            }
            if (stackValue <= 1) {
                if (menuStacks.size() > 1) {
                    navigateToPreviousMenu();
                } else {
                    finish();
                }
            }
        } else {
            popFragment();
        }
    }

    private void selectedTab(String tabId) {
        currentTab = tabId;
        BaseFragment.setCurrentTab(currentTab);

        if (Objects.requireNonNull(tagStacks.get(tabId)).size() == 0) {
            /*
              First time this tab is selected. So add first fragment of that tab.
              We are adding a new fragment which is not present in stack. So add to stack is true.
             */
            switch (tabId) {
                case TAB_HOME:
                    addInitialTabFragment(getSupportFragmentManager(), tagStacks, TAB_HOME, fragmentHome, R.id.fl_fragment, true);
                    resolveStackLists(tabId);
                    assignCurrentFragment(fragmentHome);
                    break;
                case TAB_ARTICLE:
                    addAdditionalTabFragment(getSupportFragmentManager(), tagStacks, TAB_ARTICLE, fragmentArticle, currentFragment, R.id.fl_fragment, true);
                    resolveStackLists(tabId);
                    assignCurrentFragment(fragmentArticle);
                    break;
                case TAB_RATING:
                    addAdditionalTabFragment(getSupportFragmentManager(), tagStacks, TAB_RATING, fragmentRating, currentFragment, R.id.fl_fragment, true);
                    resolveStackLists(tabId);
                    assignCurrentFragment(fragmentRating);
                    break;
                case TAB_PROFILE:
                    addAdditionalTabFragment(getSupportFragmentManager(), tagStacks, TAB_PROFILE, fragmentProfile, currentFragment, R.id.fl_fragment, true);
                    resolveStackLists(tabId);
                    assignCurrentFragment(fragmentProfile);
                    break;
            }
        } else {
            /*
             * We are switching tabs, and target tab already has at least one fragment.
             * Show the target fragment
             */
            Fragment targetFragment = getSupportFragmentManager().findFragmentByTag(Objects.requireNonNull(tagStacks.get(tabId)).lastElement());
            showHideTabFragment(getSupportFragmentManager(), targetFragment, currentFragment);
            resolveStackLists(tabId);
            assignCurrentFragment(targetFragment);
        }
    }

    private void popFragment() {
        /*
         * Select the second last fragment in current tab's stack,
         * which will be shown after the fragment transaction given below
         */
        String fragmentTag = Objects.requireNonNull(tagStacks.get(currentTab)).elementAt(Objects.requireNonNull(tagStacks.get(currentTab)).size() - 2);
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(fragmentTag);

        /*pop current fragment from stack */
        Objects.requireNonNull(tagStacks.get(currentTab)).pop();

        removeFragment(getSupportFragmentManager(), fragment, currentFragment);

        assignCurrentFragment(fragment);
    }

    private void popAndNavigateToPreviousMenu() {
        String tempCurrent = stackList.get(0);
        currentTab = stackList.get(1);
        BaseFragment.setCurrentTab(currentTab);
        bn_navi.setSelectedItemId(resolveTabPositions(currentTab));
        Fragment targetFragment = getSupportFragmentManager()
                .findFragmentByTag(Objects.requireNonNull(tagStacks.get(currentTab)).lastElement());
        showHideTabFragment(getSupportFragmentManager(), targetFragment, currentFragment);
        assignCurrentFragment(targetFragment);
        updateStackToIndexFirst(stackList, tempCurrent);
        menuStacks.remove(0);
    }

    private void navigateToPreviousMenu() {
        menuStacks.remove(0);
        currentTab = menuStacks.get(0);
        BaseFragment.setCurrentTab(currentTab);
        bn_navi.setSelectedItemId(resolveTabPositions(currentTab));
        Fragment targetFragment = getSupportFragmentManager().findFragmentByTag(Objects.requireNonNull(tagStacks.get(currentTab)).lastElement());
        showHideTabFragment(getSupportFragmentManager(), targetFragment, currentFragment);
        assignCurrentFragment(targetFragment);
    }

    private void popStackExceptFirst() {
        if (Objects.requireNonNull(tagStacks.get(currentTab)).size() == 1) {
            return;
        }
        while (!Objects.requireNonNull(tagStacks.get(currentTab)).empty()
                && !Objects.requireNonNull(Objects.requireNonNull(getSupportFragmentManager()
                .findFragmentByTag(Objects.requireNonNull(tagStacks.get(currentTab)).peek()))
                .getArguments()).getBoolean(EXTRA_IS_ROOT_FRAGMENT)) {

            getSupportFragmentManager().beginTransaction()
                    .remove(Objects.requireNonNull(getSupportFragmentManager()
                            .findFragmentByTag(Objects.requireNonNull(tagStacks.get(currentTab)).peek())));
            Objects.requireNonNull(tagStacks.get(currentTab)).pop();
        }
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(Objects.requireNonNull(tagStacks.get(currentTab)).elementAt(0));
        removeFragment(getSupportFragmentManager(), fragment, currentFragment);
        assignCurrentFragment(fragment);
    }

    private void showFragment(Bundle bundle, Fragment fragmentToAdd) {
        String tab = bundle.getString(DATA_KEY_1);
        boolean shouldAdd = bundle.getBoolean(DATA_KEY_2);
        addShowHideFragment(getSupportFragmentManager(), tagStacks, tab, fragmentToAdd, getCurrentFragmentFromShownStack(), R.id.fl_fragment, shouldAdd);
        assignCurrentFragment(fragmentToAdd);
    }

    private int resolveTabPositions(String currentTab) {
        int tabIndex = 0;
        switch (currentTab) {
            case TAB_HOME:
                tabIndex = R.id.fragment_home;
                break;
            case TAB_ARTICLE:
                tabIndex = R.id.fragment_article;
                break;
            case TAB_RATING:
                tabIndex = R.id.fragment_rating;
                break;
            case TAB_PROFILE:
                tabIndex = R.id.fragment_profile;
                break;
        }
        return tabIndex;
    }

    private void resolveStackLists(String tabId) {
        updateStackIndex(stackList, tabId);
        updateTabStackIndex(menuStacks, tabId);
    }

    private Fragment getCurrentFragmentFromShownStack() {
        return getSupportFragmentManager().findFragmentByTag(Objects.requireNonNull(tagStacks.get(currentTab))
                .elementAt(Objects.requireNonNull(tagStacks.get(currentTab)).size() - 1));
    }

    private void assignCurrentFragment(Fragment current) {
        currentFragment = current;
    }


    @Override
    public void onFragmentInteractionCallback(Bundle bundle) {
        String action = bundle.getString(ACTION);

        if (action != null){
            switch (action){
                case FragmentHome.ACTION_ARTICLE:
                    showFragment(bundle, FragmentArticle.newInstance(false));
                    break;
                case FragmentArticle.ACTION_RATING:
                    showFragment(bundle, FragmentRating.newInstance(false));
                    break;
                case FragmentRating.ACTION_PROFILE:
                    showFragment(bundle, FragmentProfile.newInstance(false));
                    break;
                case FragmentProfile.ACTION_RATING:
                    showFragment(bundle, FragmentRating.newInstance(false));
                    break;
            }
        }
    }
}
