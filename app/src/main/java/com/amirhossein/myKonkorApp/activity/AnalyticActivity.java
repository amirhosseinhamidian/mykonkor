package com.amirhossein.myKonkorApp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.LinearLayout;
import com.amirhossein.myKonkorApp.R;
import com.amirhossein.myKonkorApp.adapter.AdapterAnalytic;
import com.amirhossein.myKonkorApp.model.Analytic;
import com.amirhossein.myKonkorApp.model.DBAnalytic;
import com.github.lzyzsd.circleprogress.DonutProgress;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import static com.amirhossein.myKonkorApp.activity.Welcome.USER_INFO;

public class AnalyticActivity extends AppCompatActivity {

    private DonutProgress progressAll,progressPublic,progressDedicated,progressAllNegative,
            progressPublicNegative,progressDedicatedNegative;
    private RecyclerView recyclerView;
    private AppBarLayout appBarLayout;
    private LinearLayout linearLayoutAllPositive, linearLayoutPublicPositive,linearLayoutDedicatedPositive,
            linearLayoutAllNegative, linearLayoutPublicNegative,linearLayoutDedicatedNegative;
    private CollapsingToolbarLayout collapsing;

    List<Analytic> analyticList ;
    ObjectAnimator animatorAll, animatorPublic, animatorDedicated;
    int groupId ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_analytic);
        findViews();
        Typeface typeface = Typeface.createFromAsset(getAssets(),"Lalezar-Regular.otf");

        collapsing.setCollapsedTitleTextColor(Color.WHITE);
        collapsing.setCollapsedTitleTypeface(typeface);


        appBarHandler();
        SharedPreferences prefer = getSharedPreferences(USER_INFO,MODE_PRIVATE);
        groupId = prefer.getInt("groupId",0);
        DBAnalytic dbAnalytic = new DBAnalytic(getContext(), groupId);
        analyticList = dbAnalytic.getAllAnalytic();
        calculateAppBarChart(analyticList);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.getRecycledViewPool().setMaxRecycledViews(0,0);
        recyclerView.setAdapter(new AdapterAnalytic(getContext(),analyticList,calculatePercent()));
    }

    private void calculateAppBarChart(List<Analytic> analyticList) {
        float publicLesson =0;
        float publicCorrect =0;
        float publicWrong =0;
        float dedicatedLesson =0;
        float dedicatedCorrect =0;
        float dedicatedWrong =0;

        for (int i=0; i<4 ; i++){
            Analytic analytic = analyticList.get(i);
            publicLesson +=(float) analytic.getTotal();
            publicCorrect +=(float) analytic.getCorrect();
            publicWrong +=(float) analytic.getWrong();
        }

        for (int i=4; i<analyticList.size(); i++){
            Analytic analytic = analyticList.get(i);
            dedicatedLesson +=(float) analytic.getTotal();
            dedicatedCorrect +=(float) analytic.getCorrect();
            dedicatedWrong +=(float) analytic.getWrong();
        }

        float percentPublic = ((publicCorrect - (publicWrong/3))/publicLesson)*100;
        float percentDedicated = ((dedicatedCorrect - (dedicatedWrong/3))/dedicatedLesson)*100;
        float percentAll = (((publicCorrect+dedicatedCorrect)-((publicWrong+dedicatedWrong)/3))/(publicLesson+dedicatedLesson))*100;

        if (!String.valueOf(percentPublic).equals("NaN")){
            percentPublic = round(percentPublic,2);
        }else {
            percentPublic = 0;
        }

        if (percentPublic>=0){
            animatorPublic = ObjectAnimator.ofFloat(progressPublic , "progress",0, percentPublic);
            animatorPublic.setDuration(1800);
            animatorPublic.start();
            progressPublic.setText(round(percentPublic,2)+"%");
        }else {
            linearLayoutPublicPositive.setVisibility(View.VISIBLE);
            linearLayoutPublicNegative.setVisibility(View.GONE);
            float minus = percentPublic*(-3);
            animatorPublic = ObjectAnimator.ofFloat(progressPublicNegative , "progress",0,minus);
            animatorPublic.setDuration(1800);
            animatorPublic.start();
            progressPublicNegative.setText(round(percentPublic,2)+"%");
        }

        if (!String.valueOf(percentDedicated).equals("NaN")){
            percentDedicated = round(percentDedicated,2);
        }else {
            percentDedicated = 0;
        }

        if (percentDedicated>=0){

            animatorDedicated = ObjectAnimator.ofFloat(progressDedicated , "progress",0,percentDedicated);
            animatorDedicated.setDuration(1800);
            animatorDedicated.start();
            progressDedicated.setText(round(percentDedicated,2)+"%");
        }else {
            float minus = percentDedicated*(-3);
            linearLayoutDedicatedNegative.setVisibility(View.VISIBLE);
            linearLayoutDedicatedPositive.setVisibility(View.GONE);
            animatorDedicated = ObjectAnimator.ofFloat(progressDedicatedNegative , "progress",0,minus);
            animatorDedicated.setDuration(1800);
            animatorDedicated.start();
            progressDedicatedNegative.setText(round(percentDedicated,2)+"%");
        }

        if (!String.valueOf(percentAll).equals("NaN")){
            percentAll = round(percentAll,2);
        }else {
            percentAll = 0;
        }
        if (percentAll>=0){
            animatorAll = ObjectAnimator.ofFloat(progressAll , "progress",0,percentAll);
            animatorAll.setDuration(1800);
            animatorAll.start();
            progressAll.setText(round(percentAll,2)+"%");
        }else {
            float minus = percentAll*(-3);
            linearLayoutAllNegative.setVisibility(View.VISIBLE);
            linearLayoutAllPositive.setVisibility(View.GONE);
            animatorAll = ObjectAnimator.ofFloat(progressAllNegative , "progress",0,minus);
            animatorAll.setDuration(1800);
            animatorAll.start();
            progressAllNegative.setText(round(percentAll,2)+"%");
        }

    }

    private List<Float> calculatePercent(){
        List<Float> percents = new ArrayList<>();
        for (int i=0; i<analyticList.size(); i++){
            Analytic analytic = analyticList.get(i);
            float total = (float) analytic.getTotal();
            float correct = (float) analytic.getCorrect();
            float wrong = (float) analytic.getWrong();
            float percent = ((correct - (wrong/3))/total)*100;
            percents.add(percent);

        }

        return percents;
    }

    private static float round(float value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return (float) bd.doubleValue();
    }

    private void appBarHandler() {
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.BaseOnOffsetChangedListener() {

            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int i) {

                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                    startAlphaAnimation(linearLayoutAllPositive,500,View.VISIBLE);
                    startAlphaAnimation(linearLayoutAllNegative,500,View.VISIBLE);
                    startAlphaAnimation(linearLayoutPublicPositive,500,View.VISIBLE);
                    startAlphaAnimation(linearLayoutPublicNegative,500,View.VISIBLE);
                    startAlphaAnimation(linearLayoutDedicatedPositive,500,View.VISIBLE);
                    startAlphaAnimation(linearLayoutDedicatedNegative,500,View.VISIBLE);
                }

                if (scrollRange + i == 0) {
                    collapsing.setTitle("       آنالیز");
                    isShow = true;
                    startAlphaAnimation(linearLayoutAllPositive,500,View.INVISIBLE);
                    startAlphaAnimation(linearLayoutAllNegative,500,View.INVISIBLE);
                    startAlphaAnimation(linearLayoutPublicPositive,500,View.INVISIBLE);
                    startAlphaAnimation(linearLayoutPublicNegative,500,View.INVISIBLE);
                    startAlphaAnimation(linearLayoutDedicatedPositive,500,View.INVISIBLE);
                    startAlphaAnimation(linearLayoutDedicatedNegative,500,View.INVISIBLE);

                } else if(isShow) {
                    collapsing.setTitle("");
                    isShow = false;
                    startAlphaAnimation(linearLayoutAllPositive,500,View.VISIBLE);
                    startAlphaAnimation(linearLayoutAllNegative,500,View.VISIBLE);
                    startAlphaAnimation(linearLayoutPublicPositive,500,View.VISIBLE);
                    startAlphaAnimation(linearLayoutPublicNegative,500,View.VISIBLE);
                    startAlphaAnimation(linearLayoutDedicatedPositive,500,View.VISIBLE);
                    startAlphaAnimation(linearLayoutDedicatedNegative,500,View.VISIBLE);
                }
            }
        });
    }

    public static void startAlphaAnimation (View v, long duration, int visibility) {
        AlphaAnimation alphaAnimation = (visibility == View.VISIBLE)
                ? new AlphaAnimation(0f, 1f)
                : new AlphaAnimation(1f, 0f);

        alphaAnimation.setDuration(duration);
        alphaAnimation.setFillAfter(true);
        v.startAnimation(alphaAnimation);
    }

    private void findViews() {
        progressAll = findViewById(R.id.pb_all_analytic);
        progressPublic = findViewById(R.id.pg_public_analytic);
        progressDedicated = findViewById(R.id.pb_dedicated_analytic);
        progressAllNegative = findViewById(R.id.pb_all_analytic_negative);
        progressPublicNegative = findViewById(R.id.pg_public_analytic_negative);
        progressDedicatedNegative = findViewById(R.id.pb_dedicated_analytic_negative);
        recyclerView = findViewById(R.id.rv_analytic);
        appBarLayout = findViewById(R.id.appbar_analytic);
        collapsing = findViewById(R.id.collapsing_toolbar_analytic);
        linearLayoutAllPositive = findViewById(R.id.ll_percent_all);
        linearLayoutPublicPositive = findViewById(R.id.ll_percent_public);
        linearLayoutDedicatedPositive = findViewById(R.id.ll_percent_dedicated);
        linearLayoutAllNegative = findViewById(R.id.ll_percent_all_negative);
        linearLayoutPublicNegative = findViewById(R.id.ll_percent_public_negative);
        linearLayoutDedicatedNegative = findViewById(R.id.ll_percent_dedicated_negative);
    }

    private Context getContext(){
        return AnalyticActivity.this;
    }
}
