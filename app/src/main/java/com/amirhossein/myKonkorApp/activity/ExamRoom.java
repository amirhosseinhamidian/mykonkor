package com.amirhossein.myKonkorApp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.airbnb.lottie.LottieAnimationView;
import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.amirhossein.myKonkorApp.widget.IranSansBold;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.amirhossein.myKonkorApp.R;
import com.amirhossein.myKonkorApp.fragment.DialogExitExam;
import com.amirhossein.myKonkorApp.model.DBAnalytic;
import com.amirhossein.myKonkorApp.model.Test;
import com.amirhossein.myKonkorApp.webService.APIClient;
import com.amirhossein.myKonkorApp.webService.APIInterface;
import com.amirhossein.myKonkorApp.widget.LalezarTextView;
import com.amirhossein.myKonkorApp.widget.MyButton;
import com.github.lzyzsd.circleprogress.DonutProgress;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static com.amirhossein.myKonkorApp.activity.Welcome.USER_INFO;
import static com.amirhossein.myKonkorApp.common.Constants.SOUND;

public class ExamRoom extends AppCompatActivity  {

    private IranSansBold tv_quiz, tv_ans1,tv_ans2,tv_ans3,tv_ans4,tv_sub_quiz;
    private LalezarTextView textViewNameLesson;
    private MyButton btn_next ;
    private DonutProgress progressTime;
    private LinearLayout ll_ans1, ll_ans2, ll_ans3, ll_ans4;
    private LottieAnimationView loading;
    private RoundCornerProgressBar progressBarQuestion;
    private ImageView image;

    int correctAns;
    int wrongAns;
    int click = 0;
    int groupId ;
    int topicId ;
    int numQuiz;
    int examTime;
    int totalRemainingTime;
    String timer = "0";
    Test test;
    int lessonId;
    int statePause =0;

    CountDownTimer countDownTimer;
    ObjectAnimator progressAnimator;
    MediaPlayer mediaPlayer;
    List<Test> testList;
    ArrayList<String> correctList = new ArrayList<>();
    ArrayList<String> userAnswerList = new ArrayList<>();
    SharedPreferences prefer;
    Handler handler = new Handler();
    private DBAnalytic dbAnalytic;


    @SuppressLint("CommitPrefEdits")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam_room);
        findViews();
        appBarChange();
        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        numQuiz = bundle.getInt("numberOfQuestion");
        topicId = bundle.getInt("topicId");

        prefer = getSharedPreferences(USER_INFO,MODE_PRIVATE);
        groupId = prefer.getInt("groupId",0);
        dbAnalytic = new DBAnalytic(getContext(),groupId);

        requestQuiz();

    }

    private void requestQuiz() {

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<List<Test>> call = apiInterface.getTest(topicId,groupId,numQuiz);
        call.enqueue(new Callback<List<Test>>() {
            @Override
            public void onResponse(@NotNull Call<List<Test>> call, @NotNull Response<List<Test>> response) {
                if (response.isSuccessful()){
                    loading.setVisibility(View.GONE);

                    testList = response.body();
                    assert testList != null;
                    fetchData(testList);
                    btn_next.setOnClickListener(view -> {
                        btn_next.setEnabled(false);
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (btn_next.getText().equals("نمی زنم!")||btn_next.getText().equals("نمی زنم!/تمام")){
                                    userAnswerList.add("notAns");
                                    int remaining = Integer.parseInt(timer);
                                    totalRemainingTime += remaining;
                                    if (tv_ans1.getText().equals(test.getCorrectAns())){
                                        correctList.add("A");
                                    }else if (tv_ans2.getText().equals(test.getCorrectAns())){
                                        correctList.add("B");
                                    }else if (tv_ans3.getText().equals(test.getCorrectAns())){
                                        correctList.add("C");
                                    }else if (tv_ans4.getText().equals(test.getCorrectAns())){
                                        correctList.add("D");
                                    }
                                }
                                if (click == numQuiz){

                                    Intent intent = new Intent(ExamRoom.this,ResultActivity.class);
                                    Bundle bundle = new Bundle();
                                    bundle.putSerializable("examList", (Serializable) testList);
                                    bundle.putSerializable("userAnswerList",userAnswerList);
                                    bundle.putSerializable("correctList",correctList);
                                    intent.putExtra("correctAns",correctAns);
                                    intent.putExtra("wrongAns",wrongAns);
                                    intent.putExtra("numQuiz",numQuiz);
                                    intent.putExtra("topicId",topicId);
                                    intent.putExtra("remainingTime",totalRemainingTime);
                                    intent.putExtras(bundle);
                                    startActivity(intent);
                                    countDownTimer.cancel();
                                    finish();
                                }
                                if (click < numQuiz){
                                    fetchData(testList);
                                }
                                btn_next.setEnabled(true);
                            }
                        },1000);


                    });



                }
            }

            @Override
            public void onFailure(@NotNull Call<List<Test>> call, @NotNull Throwable t) {

            }
        });
    }

    private void progressBarNumberQuestion(int click) {
        progressBarQuestion.setProgressColor(Color.parseColor("#5362FB"));
        progressBarQuestion.setProgressBackgroundColor(Color.parseColor("#F4F5F9"));
        progressBarQuestion.setMax(numQuiz);
        progressBarQuestion.setProgress(click);
        ObjectAnimator progressAnimator = ObjectAnimator.ofFloat(progressBarQuestion,"progress",0,click);
        progressAnimator.setDuration(0);
        progressAnimator.setInterpolator(new LinearInterpolator());
        progressAnimator.start();
    }

    private void fetchData(List<Test> testList) {

        test = testList.get(click);
        lessonId = test.getLessonId();
        if (click>0){
            countDownTimer.cancel();
            progressAnimator.pause();}
        progressBarNumberQuestion(click);
        returnBackgroundTextView();
        progressExamTime(testList,lessonId);
        animationText();

        tv_quiz.setText(test.getQuestion());
        if (test.getSubQuestion() != null){
            tv_sub_quiz.setVisibility(View.VISIBLE);
            tv_sub_quiz.setText(test.getSubQuestion());
        }else {
            tv_sub_quiz.setVisibility(View.GONE);
        }
        if (test.getImage() != null){
            image.setVisibility(View.VISIBLE);
            Picasso.get().load(test.getImage()).into(image);
        }else {
            image.setVisibility(View.GONE);
        }
        textViewNameLesson.setText(test.getNameTopic());
        dbAnalytic.updateTotalAnalytic(test.getNameTopic());
        ArrayList<String> answers = new ArrayList<>();
        answers.add(test.getCorrectAns());
        answers.add(test.getWrongAns1());
        answers.add(test.getWrongAns2());
        answers.add(test.getWrongAns3());

        Collections.shuffle(answers);
        tv_ans1.setText(answers.get(0));
        tv_ans2.setText(answers.get(1));
        tv_ans3.setText(answers.get(2));
        tv_ans4.setText(answers.get(3));
        if (click == numQuiz-1){
            btn_next.setText("نمی زنم!/تمام");
        }else {
            btn_next.setText("نمی زنم!");
    }

        handleCorrectOrWrongAnswer(test,click);
        click++;


    }

    private void progressExamTime(List<Test> testList, int lessonId) {
        timeExam(lessonId);
        progressTime.setMax(examTime);
        progressTime.setProgress(0);

        boolean sound = prefer.getBoolean(SOUND,true);

        progressAnimator = ObjectAnimator.ofFloat(progressTime,"progress",0,examTime);
        countDownTimer = new CountDownTimer(examTime, 1000) {

            @SuppressLint("DefaultLocale")
            public void onTick(long millisUntilFinished) {
                long secondsInMilli = 1000;


                long elapsedSeconds = millisUntilFinished / secondsInMilli;

                timer = String.format("%02d", elapsedSeconds);
                progressTime.setText(timer);
                int lastPartTime = Integer.parseInt(timer);
                if (statePause == 0 && sound){
                    if ( lastPartTime == 8 || lastPartTime == 5 || lastPartTime ==2){
                        mediaPlayer = MediaPlayer.create(getContext(),R.raw.countdown_sound);
                        mediaPlayer.start();
                    }
                }

            }
            public void onFinish() {

                progressTime.setText("00");
                YoYo.with(Techniques.Shake)
                        .duration(100)
                        .repeat(5)
                        .playOn(progressTime);
                if (statePause ==0 && sound){
                    mediaPlayer = MediaPlayer.create(getContext(),R.raw.end_time);
                    mediaPlayer.start();
                }

                if (btn_next.getText().equals("نمی زنم!")||btn_next.getText().equals("نمی زنم!/تمام")){
                    userAnswerList.add("notAns");
                    if (tv_ans1.getText().equals(test.getCorrectAns())){
                        correctList.add("A");
                    }else if (tv_ans2.getText().equals(test.getCorrectAns())){
                        correctList.add("B");
                    }else if (tv_ans3.getText().equals(test.getCorrectAns())){
                        correctList.add("C");
                    }else if (tv_ans4.getText().equals(test.getCorrectAns())){
                        correctList.add("D");
                    }
                }
                if (click == numQuiz){
                    if (statePause==0){
                        Intent intent = new Intent(ExamRoom.this,ResultActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("examList", (Serializable) testList);
                        bundle.putSerializable("correctList",correctList);
                        bundle.putSerializable("userAnswerList",userAnswerList);
                        intent.putExtra("correctAns",correctAns);
                        intent.putExtra("wrongAns",wrongAns);
                        intent.putExtra("numQuiz",numQuiz);
                        intent.putExtra("topicId",topicId);
                        intent.putExtra("remainingTime",totalRemainingTime);
                        intent.putExtras(bundle);
                        startActivity(intent);
                        finish();
                    }else if (statePause==1){
                        finish();
                    }
                }
                if (click < numQuiz){
                    fetchData(testList);
                }


            }
        }.start();
        progressAnimator.setDuration(examTime);
        progressAnimator.start();
    }

    private void handleCorrectOrWrongAnswer(Test test,int click) {

        ll_ans1.setOnClickListener(view -> {
            userAnswerList.add("A");
            countDownTimer.cancel();
            progressAnimator.pause();
            btn_next.setText("بعدی");
            int remaining = Integer.parseInt(timer);
            totalRemainingTime += remaining;
            if (click == numQuiz-1){
                btn_next.setText("تمام");
            }

            if(tv_ans1.getText().equals(test.getCorrectAns())){
                tv_ans1.setBackgroundResource(R.drawable.background_answer_true);
                tv_ans1.setTextColor(Color.WHITE);
                correctList.add("A");
                correctAns++;
                dbAnalytic.updateCorrectAnalytic(test.getNameTopic());

            }else {
                tv_ans1.setBackgroundResource(R.drawable.background_answer_false);
                tv_ans1.setTextColor(Color.WHITE);
                dbAnalytic.updateWrongAnalytic(test.getNameTopic());
                if (tv_ans2.getText().equals(test.getCorrectAns())){
                    tv_ans2.setBackgroundResource(R.drawable.background_answer_true);
                    tv_ans2.setTextColor(Color.WHITE);
                    correctList.add("B");
                }else if (tv_ans3.getText().equals(test.getCorrectAns())){
                    tv_ans3.setBackgroundResource(R.drawable.background_answer_true);
                    tv_ans3.setTextColor(Color.WHITE);
                    correctList.add("C");
                }else {
                    tv_ans4.setBackgroundResource(R.drawable.background_answer_true);
                    tv_ans4.setTextColor(Color.WHITE);
                    correctList.add("D");
                }

                wrongAns++;
            }
            clickOff();
        });
        ll_ans2.setOnClickListener(view -> {
            userAnswerList.add("B");
            countDownTimer.cancel();
            progressAnimator.pause();
            btn_next.setText("بعدی");
            int remaining = Integer.parseInt(timer);
            totalRemainingTime += remaining;
            if (click == numQuiz-1){
                btn_next.setText("تمام");
            }

            if(tv_ans2.getText().equals(test.getCorrectAns())){
                tv_ans2.setBackgroundResource(R.drawable.background_answer_true);
                tv_ans2.setTextColor(Color.WHITE);
                correctList.add("B");
                correctAns++;
                dbAnalytic.updateCorrectAnalytic(test.getNameTopic());

            }else {
                tv_ans2.setBackgroundResource(R.drawable.background_answer_false);
                tv_ans2.setTextColor(Color.WHITE);
                dbAnalytic.updateWrongAnalytic(test.getNameTopic());
                if (tv_ans1.getText().equals(test.getCorrectAns())){
                    tv_ans1.setBackgroundResource(R.drawable.background_answer_true);
                    tv_ans1.setTextColor(Color.WHITE);
                    correctList.add("A");
                }else if (tv_ans3.getText().equals(test.getCorrectAns())){
                    tv_ans3.setBackgroundResource(R.drawable.background_answer_true);
                    tv_ans3.setTextColor(Color.WHITE);
                    correctList.add("C");
                }else {
                    tv_ans4.setBackgroundResource(R.drawable.background_answer_true);
                    tv_ans4.setTextColor(Color.WHITE);
                    correctList.add("D");
                }

                wrongAns++;
            }
            clickOff();
        });
        ll_ans3.setOnClickListener(view -> {
            userAnswerList.add("C");
            countDownTimer.cancel();
            progressAnimator.pause();
            btn_next.setText("بعدی");
            int remaining = Integer.parseInt(timer);
            totalRemainingTime += remaining;

            if (click == numQuiz-1){
                btn_next.setText("تمام");
            }

            if(tv_ans3.getText().equals(test.getCorrectAns())){
                tv_ans3.setBackgroundResource(R.drawable.background_answer_true);
                tv_ans3.setTextColor(Color.WHITE);
                correctList.add("C");
                correctAns++;
                dbAnalytic.updateCorrectAnalytic(test.getNameTopic());

            }else {
                tv_ans3.setBackgroundResource(R.drawable.background_answer_false);
                tv_ans3.setTextColor(Color.WHITE);
                dbAnalytic.updateWrongAnalytic(test.getNameTopic());
                if (tv_ans1.getText().equals(test.getCorrectAns())){
                    tv_ans1.setBackgroundResource(R.drawable.background_answer_true);
                    tv_ans1.setTextColor(Color.WHITE);
                    correctList.add("A");
                }else if (tv_ans2.getText().equals(test.getCorrectAns())){
                    tv_ans2.setBackgroundResource(R.drawable.background_answer_true);
                    tv_ans2.setTextColor(Color.WHITE);
                    correctList.add("B");
                }else {
                    tv_ans4.setBackgroundResource(R.drawable.background_answer_true);
                    tv_ans4.setTextColor(Color.WHITE);
                    correctList.add("D");
                }
                wrongAns++;

            }
            clickOff();
        });
        ll_ans4.setOnClickListener(view -> {
            userAnswerList.add("D");
            countDownTimer.cancel();
            progressAnimator.pause();
            btn_next.setText("بعدی");
            int remaining = Integer.parseInt(timer);
            totalRemainingTime += remaining;
            if (click == numQuiz-1){
                btn_next.setText("تمام");
            }

            if(tv_ans4.getText().equals(test.getCorrectAns())){
                tv_ans4.setBackgroundResource(R.drawable.background_answer_true);
                tv_ans4.setTextColor(Color.WHITE);
                correctList.add("D");
                correctAns++;
                dbAnalytic.updateCorrectAnalytic(test.getNameTopic());

            }else {
                tv_ans4.setBackgroundResource(R.drawable.background_answer_false);
                tv_ans4.setTextColor(Color.WHITE);
                dbAnalytic.updateWrongAnalytic(test.getNameTopic());
                if (tv_ans1.getText().equals(test.getCorrectAns())){
                    tv_ans1.setBackgroundResource(R.drawable.background_answer_true);
                    tv_ans1.setTextColor(Color.WHITE);
                    correctList.add("A");
                }else if (tv_ans2.getText().equals(test.getCorrectAns())){
                    tv_ans2.setBackgroundResource(R.drawable.background_answer_true);
                    tv_ans2.setTextColor(Color.WHITE);
                    correctList.add("B");
                }else {
                    tv_ans3.setBackgroundResource(R.drawable.background_answer_true);
                    tv_ans3.setTextColor(Color.WHITE);
                    correctList.add("C");
                }
                wrongAns++;

            }
            clickOff();
        });



    }

    private void timeExam(int lessonId){

        if (lessonId == 1){
            examTime = 42000;
        }
        if (lessonId == 2){
            examTime = 48000;
        }
        if (lessonId == 3){
            examTime = 42000;
        }
        if (lessonId == 4){
            examTime = 48000;
        }
        if (lessonId == 5){
            examTime = 100000;
        }
        if (lessonId == 6){
            examTime = 82000;
        }
        if (lessonId == 7){
            examTime = 70000;
        }
        if (lessonId == 8){
            examTime = 58000;
        }
        if (lessonId == 9){
            examTime = 106000;
        }
        if (lessonId == 10){
            examTime = 52000;
        }
        if (lessonId == 11){
            examTime = 62000;
        }
        if (lessonId == 12){
            examTime = 70000;
        }
        if (lessonId == 13){
            examTime = 82000;
        }
        if (lessonId == 14){
            examTime = 46000;
        }
        if (lessonId == 15){
            examTime = 70000;
        }
        if (lessonId == 16){
            examTime = 70000;
        }
        if (lessonId == 17){
            examTime = 58000;
        }
        if (lessonId == 18){
            examTime = 58000;
        }
        if (lessonId == 19){
            examTime = 52000;
        }
        if (lessonId == 20){
            examTime = 70000;
        }
        if (lessonId == 21){
            examTime = 52000;
        }
    }

    private void clickOff() {
        ll_ans1.setClickable(false);
        ll_ans2.setClickable(false);
        ll_ans3.setClickable(false);
        ll_ans4.setClickable(false);
    }

    private void animationText() {
        YoYo.with(Techniques.FadeInUp)
                .duration(200)
                .repeat(0)
                .playOn(tv_ans1);

        YoYo.with(Techniques.FadeInUp)
                .duration(200)
                .repeat(0)
                .playOn(tv_ans2);

        YoYo.with(Techniques.FadeInUp)
                .duration(200)
                .repeat(0)
                .playOn(tv_ans3);

        YoYo.with(Techniques.FadeInUp)
                .duration(200)
                .repeat(0)
                .playOn(tv_ans4);
        YoYo.with(Techniques.ZoomIn)
                .duration(100)
                .repeat(0)
                .playOn(textViewNameLesson);

    }

    private void returnBackgroundTextView() {

        tv_ans1.setBackgroundResource(R.drawable.background_answer_text);
        tv_ans1.setTextColor(Color.parseColor("#2E2E2E"));
        tv_ans2.setBackgroundResource(R.drawable.background_answer_text);
        tv_ans2.setTextColor(Color.parseColor("#2E2E2E"));
        tv_ans3.setBackgroundResource(R.drawable.background_answer_text);
        tv_ans3.setTextColor(Color.parseColor("#2E2E2E"));
        tv_ans4.setBackgroundResource(R.drawable.background_answer_text);
        tv_ans4.setTextColor(Color.parseColor("#2E2E2E"));
    }

    private void findViews() {
        tv_quiz = findViewById(R.id.tv_exam_room_main_quiz);
        tv_ans1 = findViewById(R.id.tv_exam_room_ans1);
        tv_ans2 = findViewById(R.id.tv_exam_room_ans2);
        tv_ans3 = findViewById(R.id.tv_exam_room_ans3);
        tv_ans4 = findViewById(R.id.tv_exam_room_ans4);
        btn_next = findViewById(R.id.btn_exam_room_next);
        progressTime = findViewById(R.id.progress_time_exam);
        ll_ans1 = findViewById(R.id.ll_exam_room_ans1);
        ll_ans2 = findViewById(R.id.ll_exam_room_ans2);
        ll_ans3 = findViewById(R.id.ll_exam_room_ans3);
        ll_ans4 = findViewById(R.id.ll_exam_room_ans4);
        textViewNameLesson = findViewById(R.id.tv_title_lesson);
        loading = findViewById(R.id.lottie_loading);
        progressBarQuestion = findViewById(R.id.pb_num_question);
        tv_sub_quiz = findViewById(R.id.tv_exam_room_sub_quiz);
        image = findViewById(R.id.img_quiz);
    }

    private void appBarChange() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Window window = this.getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this,R.color.colorPrimaryLight));
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        statePause = 0;
    }

    @Override
    protected void onPause() {
        super.onPause();
        statePause = 1;
    }

    @Override
    public void onBackPressed() {
        DialogExitExam dialog = new DialogExitExam(getContext());
        dialog.setContentView(R.layout.dialog_exit_exam);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(R.color.transparentGray);
        dialog.show();
    }

    private Context getContext() {
        return ExamRoom.this;
    }
}
