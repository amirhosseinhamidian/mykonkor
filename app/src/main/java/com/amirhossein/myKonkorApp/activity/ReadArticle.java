package com.amirhossein.myKonkorApp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;

import com.amirhossein.myKonkorApp.R;
import com.amirhossein.myKonkorApp.adapter.AdapterBodyArticle;
import com.amirhossein.myKonkorApp.model.BodyArticle;
import com.amirhossein.myKonkorApp.webService.APIClient;
import com.amirhossein.myKonkorApp.webService.APIInterface;
import com.amirhossein.myKonkorApp.widget.IranSansBold;
import com.amirhossein.myKonkorApp.widget.ShabnamTextView;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ReadArticle extends AppCompatActivity  {

    private ImageView img_article_read;
    private ShabnamTextView tv_title;
    private RecyclerView recycler;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private AppBarLayout appBarLayout;
    private IranSansBold textViewSource;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_article);

        findView();

        Bundle bundle = getIntent().getExtras();

        assert bundle != null;
        int id = bundle.getInt("id");
        String title = bundle.getString("title");
        String imageUrl = bundle.getString("image");
        String source = bundle.getString("source");

        titleShow(title);
        if (source != null){
            textViewSource.setVisibility(View.VISIBLE);
        }
        Picasso.get()
                .load(imageUrl)
                .into(img_article_read);

        requestBodyArticle(id);

        textViewSource.setOnClickListener(view -> {
            String url="";
            assert source != null;
            if (!source.startsWith("http://") && !source.startsWith("https://"))
               url = "http://" + source;
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));

            startActivity(intent);

        });


    }

    private void requestBodyArticle(int id) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<List<BodyArticle>> call = apiInterface.getBodyArticle(id);
        call.enqueue(new Callback<List<BodyArticle>>() {
            @Override
            public void onResponse(Call<List<BodyArticle>> call, Response<List<BodyArticle>> response) {
                if (response.isSuccessful()){
                    List<BodyArticle> bodyArticleList = response.body();
                    setupRecyclerView(bodyArticleList);
                }
            }

            @Override
            public void onFailure(Call<List<BodyArticle>> call, Throwable t) {

            }
        });
    }

    private void setupRecyclerView(List<BodyArticle> bodyArticleList) {
        recycler.setLayoutManager(new LinearLayoutManager(this));
        recycler.setAdapter(new AdapterBodyArticle(this,bodyArticleList));
        recycler.setHasFixedSize(true);
        recycler.setNestedScrollingEnabled(false);
    }


    private void titleShow(String title) {
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.BaseOnOffsetChangedListener() {

            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int i) {

                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                    tv_title.setText(title);
                    startAlphaAnimation(tv_title,100,View.VISIBLE);

                }

                if (scrollRange + i == 0) {
                    collapsingToolbarLayout.setTitle(title);
                    isShow = true;
                    startAlphaAnimation(tv_title,100,View.INVISIBLE);

                } else if(isShow) {
                    collapsingToolbarLayout.setTitle("");
                    isShow = false;
                    startAlphaAnimation(tv_title,100,View.VISIBLE);
                }
            }
        });
    }

    public static void startAlphaAnimation (View v, long duration, int visibility) {
        AlphaAnimation alphaAnimation = (visibility == View.VISIBLE)
                ? new AlphaAnimation(0f, 1f)
                : new AlphaAnimation(1f, 0f);

        alphaAnimation.setDuration(duration);
        alphaAnimation.setFillAfter(true);
        v.startAnimation(alphaAnimation);
    }

    private void findView() {

        img_article_read = findViewById(R.id.img_article_read);
        collapsingToolbarLayout = findViewById(R.id.collapsing_toolbar);
        appBarLayout = findViewById(R.id.appbar);
        tv_title = findViewById(R.id.tv_title_article);
        recycler = findViewById(R.id.rv_body_article);
        textViewSource = findViewById(R.id.tv_source_article);
    }
}
