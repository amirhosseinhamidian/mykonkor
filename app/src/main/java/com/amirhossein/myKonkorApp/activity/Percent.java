package com.amirhossein.myKonkorApp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.amirhossein.myKonkorApp.R;
import com.amirhossein.myKonkorApp.widget.IranSansBold;
import com.amirhossein.myKonkorApp.widget.ShabnamTextView;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Percent extends AppCompatActivity {
    private boolean autoIncrement = false;
    private boolean autoDecrement = false;
    Handler handler = new Handler();

    IranSansBold textViewAll,textViewCorrect,textViewWrong,textViewPercent;
    ShabnamTextView textViewAdvice;
    RelativeLayout increaseAll,increaseCorrect,increaseWrong,decreaseAll,decreaseCorrect,decreaseWrong;
    ImageView icon;
    Button btn_calculate;

    int num_all = 25;
    int num_correct = 0;
    int num_wrong = 0;
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_percent);

        findView();
        btn_calculate.setOnClickListener(new onClickCalculate());
        appBarChange();
        handleNumbers();
        YoYo.with(Techniques.FadeOutUp)
                .repeat(0)
                .duration(3000)
                .playOn(icon);


    }

    @SuppressLint({"SetTextI18n", "ClickableViewAccessibility"})
    private void handleNumbers() {
        increaseAll.setOnClickListener(view -> {
            if (num_all < 300) {
                num_all++;
                textViewAll.setText(num_all + "");
            }
        });
        increaseCorrect.setOnClickListener(view -> {
            if (num_correct < 300) {
                num_correct++;
                textViewCorrect.setText(num_correct + "");
            }
        });
        increaseWrong.setOnClickListener(view -> {
            if (num_wrong < 300) {
                num_wrong++;
                textViewWrong.setText(num_wrong + "");
            }
        });
        decreaseAll.setOnClickListener(view -> {
            if (num_all > 0) {
                num_all--;
                textViewAll.setText(num_all + "");
            }
        });
        decreaseCorrect.setOnClickListener(view -> {
            if (num_correct > 0) {
                num_correct--;
                textViewCorrect.setText(num_correct + "");
            }
        });
        decreaseWrong.setOnClickListener(view -> {
            if (num_wrong > 0) {
                num_wrong--;
                textViewWrong.setText(num_wrong + "");
            }
        });

        class RepetitiveUpdater implements Runnable {

            @SuppressLint("ClickableViewAccessibility")
            @Override
            public void run() {
                if (autoIncrement) {
                    if (increaseAll.isPressed() && num_all<300) {
                        num_all++;
                        textViewAll.setText(num_all + "");
                    }else if (increaseCorrect.isPressed() && num_correct<300){
                        num_correct++;
                        textViewCorrect.setText(num_correct + "");
                    }else if (increaseWrong.isPressed() && num_wrong<300){
                        num_wrong++;
                        textViewWrong.setText(num_wrong + "");
                    }

                    handler.postDelayed(new RepetitiveUpdater(), 150);
                }
                else if (autoDecrement) {
                    if (decreaseAll.isPressed() && num_all>0) {
                        num_all--;
                        textViewAll.setText(num_all + "");
                    } else if (decreaseCorrect.isPressed() && num_correct>0) {
                        num_correct--;
                        textViewCorrect.setText(num_correct + "");
                    } else if (decreaseWrong.isPressed() && num_wrong>0) {
                        num_wrong--;
                        textViewWrong.setText(num_wrong + "");
                    }
                    handler.postDelayed(new RepetitiveUpdater(), 150);
                }


            }
        }

        increaseAll.setOnLongClickListener(view -> {
            autoIncrement = true;
            handler.post(new RepetitiveUpdater());
            return false;
        });

        increaseAll.setOnTouchListener((view, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_UP && autoIncrement) {
                autoIncrement = false;
            }
            return false;
        });

        increaseCorrect.setOnLongClickListener(view -> {
            autoIncrement = true;
            handler.post(new RepetitiveUpdater());
            return false;
        });

        increaseCorrect.setOnTouchListener((view, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_UP && autoIncrement) {
                autoIncrement = false;
            }
            return false;
        });

        increaseWrong.setOnLongClickListener(view -> {
            autoIncrement = true;
            handler.post(new RepetitiveUpdater());
            return false;
        });

        increaseWrong.setOnTouchListener((view, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_UP && autoIncrement) {
                autoIncrement = false;
            }
            return false;
        });

        decreaseAll.setOnLongClickListener(view -> {
            autoDecrement = true;
            handler.post(new RepetitiveUpdater());
            return false;
        });

        decreaseAll.setOnTouchListener((view, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN && autoIncrement) {
                autoDecrement = false;
            }
            return false;
        });

        decreaseCorrect.setOnLongClickListener(view -> {
            autoDecrement = true;
            handler.post(new RepetitiveUpdater());
            return false;
        });

        decreaseCorrect.setOnTouchListener((view, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN && autoIncrement) {
                autoDecrement = false;
            }
            return false;
        });

        decreaseWrong.setOnLongClickListener(view -> {
            autoDecrement = true;
            handler.post(new RepetitiveUpdater());
            return false;
        });

        decreaseWrong.setOnTouchListener((view, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN && autoIncrement) {
                autoDecrement = false;
            }
            return false;
        });
    }



    private void findView() {

        btn_calculate = findViewById(R.id.btn_calculate);
        textViewAll = findViewById(R.id.tv_all_number_percent);
        textViewCorrect = findViewById(R.id.tv_correct_percent);
        textViewWrong = findViewById(R.id.tv_wrong_percent);
        increaseAll = findViewById(R.id.rl_increase_all);
        increaseCorrect = findViewById(R.id.rl_increase_correct);
        increaseWrong = findViewById(R.id.rl_increase_wrong);
        decreaseAll = findViewById(R.id.rl_decrease_all);
        decreaseCorrect = findViewById(R.id.rl_decrease_correct);
        decreaseWrong = findViewById(R.id.rl_decrease_wrong);
        textViewPercent = findViewById(R.id.tv_percent);
        textViewAdvice = findViewById(R.id.tv_advice);
        icon = findViewById(R.id.img_icon_percent);
    }

    @SuppressLint("SetTextI18n")
    private void calculatePercent(double num_question , double num_correct , double num_wrong){
        if (!(num_question <= 0 && num_correct < 0 && num_wrong < 0)) {

            double result;
            result = ((num_correct - (num_wrong / 3)) / num_question) * 100;
            String res = String.valueOf(result);

            if (res.equals("NaN") || res.equals("Infinity") || res.equals("-Infinity")){
                Toast.makeText(Percent.this,"مقادیر را درست وارد کنید!",Toast.LENGTH_SHORT).show();
            }else {
                double r = round(result,2);
                textViewPercent.setText(r + " %");
            }


        }
    }

    private static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    private void appBarChange() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Window window = this.getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this,R.color.colorPrimaryLight));
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
    }

    private void ifNotWrong(double num_question , double num_correct, double num_wrong){
        String advice = "";
        double result =(num_correct/num_question)*100;
        double r = round(result,2);
        if (num_wrong == 0 && r>70) {
            advice = "آفرین! اگر در همین سطح ادامه بدی حتما جزو نفرات برتر کنکور خواهی بود.";
            textViewAdvice.setText(advice);
        }if (num_wrong == 0 &&70>= r&& r>40) {
            advice = "بسیار عالی! به همین روند ادامه بده سعی کن درصد خودت رو با زدن تست های درست بیشتر بالا ببری.";
            textViewAdvice.setText(advice);
        }if (num_wrong == 0 && r<=40 && r>20) {
            advice = "ایول! اینکه غلط نداری یک مزیت بزرگه اما سعی کن روی مباحث تسلط بیشتری پیدا کنی تا تعداد جواب های درستت رو افزایش بدی! همین طوری ادامه بده!";
            textViewAdvice.setText(advice);
        }if (num_wrong == 0 && r<=20) {
            advice = "بد نیست ! غلط نداشتن نکته مهمیه اما هنوز نیاز به مطالعه داری تا تسلطت رو افزایش بدی!";
            textViewAdvice.setText(advice);
        }if (num_wrong !=0 && r>70) {
            advice = "آفرین بر تو! کاملا بر مطلب مسلطی اما فراموش نکن اگه به سوالی شک داری با تکنیک هایی که وجود داره سعی کن بهتر تصمیم گیری کنی اگه سوالات غلط رو جواب نمی دادی درصد تو "+ r + "% بود. برای آگاهی از تکنیک ها یه سری به بخش دانستنی ها بزن!";
            textViewAdvice.setText(advice);
        }if (num_wrong !=0 && 70>=r && r>40) {
            advice = "بسیار خوب درصد خوبیه اما میدونستی که اگه غلط نداشتی درصدت "+r+"% بود و به همین راحتی می تونستی کلی از رقیبات جلو بیافتی!در بخش دانستنی ها می تونی با یه سری تکنیک آشنا بشی که بهت کمک کنه";
            textViewAdvice.setText(advice);
        }if (num_wrong !=0 && 40>=r && r>20) {
            advice = "خوبه! همینطوری ادامه بده اما فراموش نکن اگه غلط نداشتی با همین تعداد سوال درست درصدت "+r+"% بود. اگه دوست داری بدونی چجور میشه جلوی سوالت غلط رو بگیری در بخش دانستنی ها میتونی جواب این سوال رو پیدا کنی";
            textViewAdvice.setText(advice);
        }if (num_wrong !=0 && 20>=r ) {
            advice = "خب خب مشخصه هنوز تسلط کافی رو در این مبحث نداری پیشنهاد من اینه اول روی مباحث ساده تر تمرکز کن زمانی که کمی پیشرفت کردی در بحث عمیق تر شو!";
            textViewAdvice.setText(advice);
        }

    }

    private class onClickCalculate implements View.OnClickListener {
        @Override
        public void onClick(View view) {

            YoYo.with(Techniques.SlideInRight)
                    .repeat(0)
                    .duration(1000)
                    .playOn(textViewAdvice);
            textViewAdvice.setVisibility(View.VISIBLE);
            YoYo.with(Techniques.Tada)
                    .duration(1000)
                    .repeat(0)
                    .playOn(textViewPercent);
            textViewPercent.setVisibility(View.VISIBLE);
            try {
                double num_question = Double.parseDouble(textViewAll.getText().toString());
                double num_correct = Double.parseDouble(textViewCorrect.getText().toString());
                double num_wrong = Double.parseDouble(textViewWrong.getText().toString());
                if (num_question < num_correct+num_wrong){
                    Toast.makeText(Percent.this,"مقادیر را درست وارد کنید!",Toast.LENGTH_SHORT).show();
                }else {
                    calculatePercent(num_question,num_correct,num_wrong);
                    ifNotWrong(num_question,num_correct,num_wrong);
                }

            }catch (NumberFormatException ignored){

            }
        }
    }
}
