package com.amirhossein.myKonkorApp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.amirhossein.myKonkorApp.R;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import static com.amirhossein.myKonkorApp.widget.Session.pr_name;

public class Splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ImageView logo = findViewById(R.id.img_logo_splash);
        appBarChange();
        YoYo.with(Techniques.BounceInDown)
                .repeat(0)
                .duration(1200)
                .playOn(logo);


        int SPLASH_DISPLAY_LENGTH = 1500;
        new Handler().postDelayed((Runnable) () -> {
            SharedPreferences preferences = getSharedPreferences(pr_name,MODE_PRIVATE);
            boolean login = preferences.getBoolean("loggedIn",false);
            if (!login){
                Intent intent = new Intent(this,Intro.class);
                startActivity(intent);

            }else {
                Intent intent = new Intent(this,MainActivity.class);
                startActivity(intent);
            }
            finish();
        }, SPLASH_DISPLAY_LENGTH);

    }

    private void appBarChange() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Window window = this.getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this,R.color.splash));
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        }
    }

}
