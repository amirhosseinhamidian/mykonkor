package com.amirhossein.myKonkorApp.webService;

import com.amirhossein.myKonkorApp.model.Article;
import com.amirhossein.myKonkorApp.model.BodyArticle;
import com.amirhossein.myKonkorApp.model.Field;
import com.amirhossein.myKonkorApp.model.Test;
import com.amirhossein.myKonkorApp.model.Topic;
import com.amirhossein.myKonkorApp.model.University;
import com.amirhossein.myKonkorApp.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface APIInterface {


    @POST("topic")
    Call<List<Topic>> getTopics();

    @POST("article")
    Call<List<Article>> getArticle();

    @POST("bodyArticle/getBody/{article_id}")
    Call<List<BodyArticle>> getBodyArticle(@Path("article_id") int article_id);

    @POST("university/oneUniversity/{id}")
    Call<University> getUniversity(@Path("id") int id);

    @POST("university/list")
    Call<List<University>> getUniversityList();

    @POST("article/amir/{id}")
    Call<Article> getArticle(@Path("id") int id);

    @POST("filed")
    Call<List<Field>> getFiled();

    @POST("filed/group/{group}")
    Call<List<Field>> getFiled(@Path("group") int group);

    @POST("filed/readMore/{id}")
    Call<Field> getFiledRead(@Path("id") int id);

    @PUT("user/updateFavoriteField")
    Call<User> updateFavorite(@retrofit2.http.Field("id") int id,
                              @retrofit2.http.Field("favorite_field_id") int favoriteFieldId);

    @POST("test/size/{topicId}/{groupId}/{num}")
    Call<List<Test>> getTest(@Path("topicId") int topicId,
                             @Path("groupId") int groupId,
                             @Path("num") int num);

    @POST("test/daily/{group}")
    Call<List<Test>> getTest(@Path("group") int group);

    @POST("test/quiz/{id}")
    Call<Test> getOneTest(@Path("id") int id);

    @POST("user/insertUser")
    @FormUrlEncoded
    Call<Void> registerUserGoogle(@retrofit2.http.Field("username") String username,
                            @retrofit2.http.Field("groupId") int groupId,
                            @retrofit2.http.Field("email") String email,
                            @retrofit2.http.Field("image") String image);

    @POST("user/insertUserGuest")
    @FormUrlEncoded
    Call<Void> registerUserGuest(@retrofit2.http.Field("username") String username,
                            @retrofit2.http.Field("groupId") int groupId,
                            @retrofit2.http.Field("android_id") String androidId,
                                 @retrofit2.http.Field("image") String image);

    @POST("user/getScoreWeekly/{groupId}")
    Call<List<User>> getWeeklyScore(@Path("groupId") int groupId);

    @POST("user/getScoreMonthly/{groupId}")
    Call<List<User>> getMonthlyScore(@Path("groupId") int groupId);

    @POST("user/getScoreTotal/{groupId}")
    Call<List<User>> getTotalScore(@Path("groupId") int groupId);

    @POST("user/getMyScoreWeekly/{androidId}/{groupId}")
    Call<User> getMyScoreWeekly(@Path("androidId") String androidId,@Path("groupId") int groupId);

    @POST("user/getMyScoreMonthly/{androidId}/{groupId}")
    Call<User> getMyScoreMonthly(@Path("androidId") String androidId,@Path("groupId") int groupId);

    @POST("user/getMyScoreTotal/{androidId}/{groupId}")
    Call<User> getMyScoreTotal(@Path("androidId") String androidId,@Path("groupId") int groupId);

    @POST("user/getMyAllScore/{androidId}/{groupId}")
    Call<User> getMyAllScore(@Path("androidId") String androidId,@Path("groupId") int groupId);

    @POST("user/getMyScoreWeeklyByEmail/{email}/{groupId}")
    Call<User> getMyScoreWeeklyByEmail(@Path("email") String email,@Path("groupId") int groupId);

    @POST("user/getMyScoreMonthlyByEmail/{email}/{groupId}")
    Call<User> getMyScoreMonthlyByEmail(@Path("email") String email,@Path("groupId") int groupId);

    @POST("user/getMyScoreTotalByEmail/{email}/{groupId}")
    Call<User> getMyScoreTotalByEmail(@Path("email") String email,@Path("groupId") int groupId);

    @POST("user/getMyAllScoreByEmail/{email}/{groupId}")
    Call<User> getMyAllScoreByEmail(@Path("email") String email,@Path("groupId") int groupId);

    @POST("user/updateWeekScore")
    @FormUrlEncoded
    Call<Void> updateWeekScore(@retrofit2.http.Field("week_score") int week_score,
                               @retrofit2.http.Field("android_id") String android_id);

    @POST("user/updateMonthScore")
    @FormUrlEncoded
    Call<Void> updateMonthScore(@retrofit2.http.Field("month_score") int monthScore,
                                @retrofit2.http.Field("android_id") String android_id);

    @POST("user/updateTotalScore")
    @FormUrlEncoded
    Call<Void> updateTotalScore(@retrofit2.http.Field("total_score") int totalScore,
                                @retrofit2.http.Field("android_id") String android_id);

    @POST("user/updateWeekScoreByEmail")
    @FormUrlEncoded
    Call<Void> updateWeekScoreByEmail(@retrofit2.http.Field("week_score") int week_score,
                               @retrofit2.http.Field("email") String email);

    @POST("user/updateMonthScoreByEmail")
    @FormUrlEncoded
    Call<Void> updateMonthScoreByEmail(@retrofit2.http.Field("month_score") int monthScore,
                                @retrofit2.http.Field("email") String email);

    @POST("user/updateTotalScoreByEmail")
    @FormUrlEncoded
    Call<Void> updateTotalScoreByEmail(@retrofit2.http.Field("total_score") int totalScore,
                                @retrofit2.http.Field("email") String email);

    @POST("user/updateUsernameByEmail")
    @FormUrlEncoded
    Call<Void> updateUsernameByEmail(@retrofit2.http.Field("username") String username,
                                       @retrofit2.http.Field("email") String email);

    @POST("user/updateUsername")
    @FormUrlEncoded
    Call<Void> updateUsername(@retrofit2.http.Field("username") String username,
                                     @retrofit2.http.Field("android_id") String android_id);
}
