package com.amirhossein.myKonkorApp.model;

public class Score {
    public static final String TABLE_NAME = "scoreDay";
    public static final String COLUMN_ID = "id";
    public static final String TOTAL_SCORE = "totalScore";
    public static final String COLUMN_DAY = "day";
    public static final String COLUMN_WEEK = "week";
    public static final String COLUMN_MONTH = "month";

    private int id ;
    private int totalScore;
    private int day;
    private int week;
    private int month;

    public Score() {
    }



    public Score(int id, int totalScore, int day, int week, int month) {
        this.id = id;
        this.totalScore = totalScore;
        this.day = day;
        this.week = week;
        this.month = month;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getWeek() {
        return week;
    }

    public void setWeek(int week) {
        this.week = week;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(int totalScore) {
        this.totalScore = totalScore;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public static final String CREATE_TABLE =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + TOTAL_SCORE + " INTEGER,"
                    + COLUMN_DAY + " INTEGER,"
                    + COLUMN_WEEK+ " INTEGER,"
                    + COLUMN_MONTH+ " INTEGER"
                    + ");";
}
