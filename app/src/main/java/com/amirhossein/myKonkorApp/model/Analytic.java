package com.amirhossein.myKonkorApp.model;

public class Analytic {

    public static final String COLUMN_ID = "id";
    public static final String TBL_GROUP1 = "group1";
    public static final String TBL_GROUP2 = "group2";
    public static final String TBL_GROUP3 = "group3";
    public static final String COLUMN_TOTAL = "total";
    public static final String COLUMN_CORRECT = "correct";
    public static final String COLUMN_WRONG = "wrong";
    public static final String COLUMN_TOPIC = "topic";

    private int id;
    private String topic;
    private int total;
    private int correct;
    private int wrong;
    private int multiLesson;

    public Analytic() {
    }

    public Analytic(int id, String topic, int total, int correct, int wrong) {
        this.id = id;
        this.topic = topic;
        this.total = total;
        this.correct = correct;
        this.wrong = wrong;
    }

    public int getMultiLesson() {

        return multiLesson;
    }

    public void setMultiLesson(int multiLesson) {
        this.multiLesson = multiLesson;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getCorrect() {
        return correct;
    }

    public void setCorrect(int correct) {
        this.correct = correct;
    }

    public int getWrong() {
        return wrong;
    }

    public void setWrong(int wrong) {
        this.wrong = wrong;
    }
}
