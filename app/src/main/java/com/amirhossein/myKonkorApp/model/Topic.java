package com.amirhossein.myKonkorApp.model;

import com.google.gson.annotations.SerializedName;

public class Topic {

    @SerializedName("name")
    private String name;

    @SerializedName("image")
    private String image;

    @SerializedName("num_test")
    private int num_test;

    @SerializedName("id")
    private int id;


    public Topic() {
    }

    public Topic(String name, String image, int num_test) {
        this.name = name;
        this.image = image;
        this.num_test = num_test;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getNum_test() {
        return num_test;
    }

    public void setNum_test(int num_test) {
        this.num_test = num_test;
    }
}
