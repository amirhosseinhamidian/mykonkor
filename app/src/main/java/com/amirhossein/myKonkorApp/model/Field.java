package com.amirhossein.myKonkorApp.model;

public class Field {

    private int id;
    private String title;
    private  String imageLarge;
    private  String imageShort;
    private int groupId;
    private int description;
    private int jobs;
    private int primaryEducation;
    private int higherEducation;

    public Field() {
    }

    public Field(String title, String imageLarge, String imageShort, int groupId, int description, int jobs ,int primaryEducation, int higherEducation) {
        this.title = title;
        this.imageLarge = imageLarge;
        this.imageShort = imageShort;
        this.groupId = groupId;
        this.description = description;
        this.jobs = jobs;
        this.primaryEducation = primaryEducation;
        this.higherEducation = higherEducation;
    }

    public int getJobs() {
        return jobs;
    }

    public int getPrimaryEducation() {
        return primaryEducation;
    }

    public int getHigherEducation() {
        return higherEducation;
    }

    public int getDescription() {
        return description;
    }

    public int getGroup() {
        return groupId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageLarge() {
        return imageLarge;
    }

    public void setImageLarge(String imageLarge) {
        this.imageLarge = imageLarge;
    }

    public String getImageShort() {
        return imageShort;
    }
}
