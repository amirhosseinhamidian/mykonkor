package com.amirhossein.myKonkorApp.model;

import com.google.gson.annotations.SerializedName;

public class BodyArticle {

    @SerializedName("id")
    private int id ;
    @SerializedName("article_id")
    private int article_id;
    @SerializedName("title_case")
    private String title_case;
    @SerializedName("description")
    private String description;
    @SerializedName("image_case")
    private String image_case;

    public BodyArticle() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getArticle_id() {
        return article_id;
    }

    public void setArticle_id(int article_id) {
        this.article_id = article_id;
    }

    public String getTitle_case() {
        return title_case;
    }

    public void setTitle_case(String title_case) {
        this.title_case = title_case;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage_case() {
        return image_case;
    }

    public void setImage_case(String image_case) {
        this.image_case = image_case;
    }
}
