package com.amirhossein.myKonkorApp.model;

import android.net.Uri;

import com.google.gson.annotations.SerializedName;

public class User {
    public static final int WEEK_REQUEST = 1;
    public static final int MONTH_REQUEST = 2;
    public static final int TOTAL_REQUEST = 3;

    @SerializedName("id")
    private int id;
    @SerializedName("username")
    private String username;
    @SerializedName("groupId")
    private int groupId;
    private String result;

    private int score;
    @SerializedName("android_id")
    private String androidId;
    @SerializedName("week_rank")
    private int rankWeek;
    @SerializedName("month_rank")
    private int rankMonth;
    @SerializedName("total_rank")
    private int rankTotal;
    @SerializedName("week_score")
    private int scoreWeek;
    @SerializedName("month_score")
    private int scoreMonth;
    @SerializedName("total_score")
    private int scoreTotal;
    @SerializedName("email")
    private String email;
    @SerializedName("image")
    private String image;

    public User() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getScoreWeek() {
        return scoreWeek;
    }

    public void setScoreWeek(int scoreWeek) {
        this.scoreWeek = scoreWeek;
    }

    public int getScoreMonth() {
        return scoreMonth;
    }

    public void setScoreMonth(int scoreMonth) {
        this.scoreMonth = scoreMonth;
    }

    public int getScoreTotal() {
        return scoreTotal;
    }

    public void setScoreTotal(int scoreTotal) {
        this.scoreTotal = scoreTotal;
    }

    public int getRankWeek() {
        return rankWeek;
    }

    public void setRankWeek(int rankWeek) {
        this.rankWeek = rankWeek;
    }

    public int getRankMonth() {
        return rankMonth;
    }

    public void setRankMonth(int rankMonth) {
        this.rankMonth = rankMonth;
    }

    public int getRankTotal() {
        return rankTotal;
    }

    public void setRankTotal(int rankTotal) {
        this.rankTotal = rankTotal;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getAndroidId() {
        return androidId;
    }

    public void setAndroidId(String androidId) {
        this.androidId = androidId;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }
}
