package com.amirhossein.myKonkorApp.model;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;

public class DatabaseHelperScore extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "score_db";
    private static final int DATABASE_VERSION = 1;

    public DatabaseHelperScore(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(Score.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+ Score.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    public long insertScore(int totalScoreDay, int day , int week, int month){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Score.TOTAL_SCORE,totalScoreDay);
        contentValues.put(Score.COLUMN_DAY,day);
        contentValues.put(Score.COLUMN_WEEK,week);
        contentValues.put(Score.COLUMN_MONTH,month);
        long id = db.insert(Score.TABLE_NAME,null,contentValues);
        db.close();
        return id;
    }

    public Score getScore(long id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(Score.TABLE_NAME,
                new String[]{Score.COLUMN_ID,Score.TOTAL_SCORE,Score.COLUMN_DAY,Score.COLUMN_WEEK,Score.COLUMN_MONTH},
                Score.COLUMN_ID+"=?",
                new String[]{String.valueOf(id)},null,null,null,null);

        if (cursor != null)
            cursor.moveToFirst();

        Score score = new Score(
                cursor.getInt(cursor.getColumnIndex(Score.COLUMN_ID)),
                cursor.getInt(cursor.getColumnIndex(Score.TOTAL_SCORE)),
                cursor.getInt(cursor.getColumnIndex(Score.COLUMN_DAY)),
                cursor.getInt(cursor.getColumnIndex(Score.COLUMN_WEEK)),
                cursor.getInt(cursor.getColumnIndex(Score.COLUMN_MONTH))
                );

        cursor.close();
        return score;
    }

    public Score getHighScore(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(Score.TABLE_NAME,
                new String[]{Score.COLUMN_ID,Score.TOTAL_SCORE,Score.COLUMN_DAY,Score.COLUMN_WEEK,Score.COLUMN_MONTH},
                null,null,null,null,Score.TOTAL_SCORE+" DESC","1");
        if (cursor != null)
            cursor.moveToFirst();

        Score score = new Score(
                cursor.getInt(cursor.getColumnIndex(Score.COLUMN_ID)),
                cursor.getInt(cursor.getColumnIndex(Score.TOTAL_SCORE)),
                cursor.getInt(cursor.getColumnIndex(Score.COLUMN_DAY)),
                cursor.getInt(cursor.getColumnIndex(Score.COLUMN_WEEK)),
                cursor.getInt(cursor.getColumnIndex(Score.COLUMN_MONTH))
                );

        cursor.close();
        return score;
    }

    public Score getLastScore (){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(Score.TABLE_NAME,
                new String[]{Score.COLUMN_ID,Score.TOTAL_SCORE,Score.COLUMN_DAY,Score.COLUMN_WEEK,Score.COLUMN_MONTH},
                null,null,null,null,Score.COLUMN_ID+" DESC","1");

        if (cursor != null)
            cursor.moveToFirst();

        Score score = new Score(
                cursor.getInt(cursor.getColumnIndex(Score.COLUMN_ID)),
                cursor.getInt(cursor.getColumnIndex(Score.TOTAL_SCORE)),
                cursor.getInt(cursor.getColumnIndex(Score.COLUMN_DAY)),
                cursor.getInt(cursor.getColumnIndex(Score.COLUMN_WEEK)),
                cursor.getInt(cursor.getColumnIndex(Score.COLUMN_MONTH))
                );

        cursor.close();
        return score;
    }

    public Score getLastCounters(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(Score.TABLE_NAME,
                new String[]{Score.COLUMN_ID,Score.TOTAL_SCORE,Score.COLUMN_DAY,Score.COLUMN_WEEK,Score.COLUMN_MONTH},
                null,null,null,null,Score.COLUMN_ID+" DESC","1");
        if (cursor != null)
            cursor.moveToFirst();

        Score score = new Score(
                cursor.getInt(cursor.getColumnIndex(Score.COLUMN_ID)),
                cursor.getInt(cursor.getColumnIndex(Score.TOTAL_SCORE)),
                cursor.getInt(cursor.getColumnIndex(Score.COLUMN_DAY)),
                cursor.getInt(cursor.getColumnIndex(Score.COLUMN_WEEK)),
                cursor.getInt(cursor.getColumnIndex(Score.COLUMN_MONTH)));

        cursor.close();
        return score;
    }

    public List<Score> getAllScore(){
        List<Score> scores = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + Score.TABLE_NAME+" ORDER BY "+ Score.COLUMN_DAY +" DESC";

        SQLiteDatabase db = this.getReadableDatabase();
        @SuppressLint("Recycle")
        Cursor cursor = db.rawQuery(selectQuery,null);

        if (cursor.moveToFirst()){
            do {
                Score score = new Score();
                score.setId(cursor.getInt(cursor.getColumnIndex(Score.COLUMN_ID)));
                score.setTotalScore(cursor.getInt(cursor.getColumnIndex(Score.TOTAL_SCORE)));
                score.setDay(cursor.getInt(cursor.getColumnIndex(Score.COLUMN_DAY)));
                score.setWeek(cursor.getInt(cursor.getColumnIndex(Score.COLUMN_WEEK)));
                score.setMonth(cursor.getInt(cursor.getColumnIndex(Score.COLUMN_MONTH)));

                scores.add(score);
            }while (cursor.moveToNext());
        }
        db.close();
        return scores;

    }

    public List<Score> getAllScoreWeek(int week){
        List<Score> scores = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + Score.TABLE_NAME +" WHERE "+ Score.COLUMN_WEEK +"="+ week;
        @SuppressLint("Recycle")
        Cursor cursor = db.rawQuery(selectQuery,null);
        if (cursor.moveToFirst()){
            do {
                Score score = new Score();
                score.setId(cursor.getInt(cursor.getColumnIndex(Score.COLUMN_ID)));
                score.setTotalScore(cursor.getInt(cursor.getColumnIndex(Score.TOTAL_SCORE)));
                score.setDay(cursor.getInt(cursor.getColumnIndex(Score.COLUMN_DAY)));
                score.setDay(cursor.getInt(cursor.getColumnIndex(Score.COLUMN_WEEK)));

                scores.add(score);
            }while (cursor.moveToNext());
        }
        db.close();
        return scores;
    }

    public List<Score> getAllScoreMonth(int month){
        List<Score> scores = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + Score.TABLE_NAME +" WHERE "+ Score.COLUMN_MONTH +"="+ month;
        @SuppressLint("Recycle")
        Cursor cursor = db.rawQuery(selectQuery,null);
        if (cursor.moveToFirst()){
            do {
                Score score = new Score();
                score.setId(cursor.getInt(cursor.getColumnIndex(Score.COLUMN_ID)));
                score.setTotalScore(cursor.getInt(cursor.getColumnIndex(Score.TOTAL_SCORE)));
                score.setDay(cursor.getInt(cursor.getColumnIndex(Score.COLUMN_DAY)));
                score.setDay(cursor.getInt(cursor.getColumnIndex(Score.COLUMN_WEEK)));

                scores.add(score);
            }while (cursor.moveToNext());
        }
        db.close();
        return scores;
    }



    public int updateScore(int id,int newScore){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Score.TOTAL_SCORE,newScore);
        return db.update(Score.TABLE_NAME,values,Score.COLUMN_ID+"=?",
                new String[]{String.valueOf(id)});
    }

    public void deleteScore(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Score.TABLE_NAME,null,
                null);
        db.close();
    }

    public boolean columnExists() {
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT EXISTS (SELECT * FROM '"+Score.TABLE_NAME+"'  LIMIT 1)";
        Cursor cursor = db.rawQuery(sql, null);
        cursor.moveToFirst();

        if (cursor.getInt(0) == 1) {
            cursor.close();
            return true;
        } else {
            cursor.close();
            return false;
        }
    }
}
