package com.amirhossein.myKonkorApp.model;

import com.amirhossein.myKonkorApp.R;

import java.util.ArrayList;
import java.util.List;

public class DataFieldList {
    private List<Field> fields;
    private DataFieldList(){
        fields = new ArrayList<>();

        fields.add(new Field("مهندسی برق","math_field_1.png","math_field_1.jpg",1, R.string.aboutField_m1,R.string.jobs_m1,R.string.primaryEducation_m1,R.string.higherEducation_m1));
        fields.add(new Field("مهندسی کامپیوتر","math_field_2.png","math_field_2.jpg",1,R.string.aboutField_m2,R.string.jobs_m2,R.string.primaryEducation_m2,R.string.higherEducation_m2));
        fields.add(new Field("مهندسی مکانیک","math_field_3.png","math_field_3.jpg",1,R.string.aboutField_m3,R.string.jobs_m3,R.string.primaryEducation_m3,R.string.higherEducation_m3));
        fields.add(new Field("مهندسی عمران","math_field_4.png","math_field_4.jpg",1,R.string.aboutField_m4,R.string.jobs_m4,R.string.primaryEducation_m4,R.string.higherEducation_m4));
        fields.add(new Field("مهندسی معماری","math_field_5.png","math_field_5.jpg",1,R.string.aboutField_m5,R.string.jobs_m5,R.string.primaryEducation_m5,R.string.higherEducation_m5));
        fields.add(new Field("مهندسی کشتی","math_field_6.png","math_field_6.jpg",1,R.string.aboutField_m6,R.string.jobs_m6,R.string.primaryEducation_m6,R.string.higherEducation_m6));
        fields.add(new Field("مهندسی هوافضا","math_field_7.png","math_field_7.jpg",1,R.string.aboutField_m7,R.string.jobs_m7,R.string.primaryEducation_m7,R.string.higherEducation_m7));
        fields.add(new Field("مهندسی انرژی","math_field_8.png","math_field_8.jpg",1,R.string.aboutField_m8,R.string.jobs_m8,R.string.primaryEducation_m8,R.string.higherEducation_m8));
        fields.add(new Field("مهندسی شهرسازی","math_field_9.png","math_field_9.jpg",1,R.string.aboutField_m9,R.string.jobs_m9,R.string.primaryEducation_m9,R.string.higherEducation_m9));
        fields.add(new Field("مهندسی فناوری اطلاعات","math_field_10.png","math_field_10.jpg",1,R.string.aboutField_m10,R.string.jobs_m10,R.string.primaryEducation_m10,R.string.higherEducation_m10));
        fields.add(new Field("مهندسی صنایع","math_field_11.png","math_field_11.jpg",1,R.string.aboutField_m11,R.string.jobs_m11,R.string.primaryEducation_m11,R.string.higherEducation_m11));
        fields.add(new Field("مهندسی شیمی","math_field_12.png","math_field_12.jpg",1,R.string.aboutField_m12,R.string.jobs_m12,R.string.primaryEducation_m12,R.string.higherEducation_m12));
        fields.add(new Field("مهندسی پزشکی","math_field_13.png","math_field_13.jpg",1,R.string.aboutField_m13,R.string.jobs_m13,R.string.primaryEducation_m13,R.string.higherEducation_m13));
        fields.add(new Field("مهندسی دریا","math_field_14.png","math_field_14.jpg",1,R.string.aboutField_m14,R.string.jobs_m14,R.string.primaryEducation_m14,R.string.higherEducation_m14));
        fields.add(new Field("مهندسی نساجی","math_field_15.png","math_field_15.jpg",1,R.string.aboutField_m15,R.string.jobs_m15,R.string.primaryEducation_m15,R.string.higherEducation_m15));
        fields.add(new Field("مهندسی رباتیک","math_field_16.png","math_field_16.jpg",1,R.string.aboutField_m16,R.string.jobs_m16,R.string.primaryEducation_m16,R.string.higherEducation_m16));
        fields.add(new Field("مهندسی نفت","math_field_17.png","math_field_17.jpg",1,R.string.aboutField_m17,R.string.jobs_m17,R.string.primaryEducation_m17,R.string.higherEducation_m17));
        fields.add(new Field("مهندسی ایمنی","math_field_18.png","math_field_18.jpg",1,R.string.aboutField_m18,R.string.jobs_m18,R.string.primaryEducation_m18,R.string.higherEducation_m18));
        fields.add(new Field("مهندسی معدن","math_field_19.png","math_field_19.jpg",1,R.string.aboutField_m19,R.string.jobs_m19,R.string.primaryEducation_m19,R.string.higherEducation_m19));
        fields.add(new Field("مهندسی پلیمر","math_field_20.png","math_field_20.jpg",1,R.string.aboutField_m20,R.string.jobs_m20,R.string.primaryEducation_m20,R.string.higherEducation_m20));
        fields.add(new Field("مهندسی مواد","math_field_21.png","math_field_21.jpg",1,R.string.aboutField_m21,R.string.jobs_m21,R.string.primaryEducation_m21,R.string.higherEducation_m21));
        fields.add(new Field("مهندسی مدیریت پروژه","math_field_22.png","math_field_22.jpg",1,R.string.aboutField_m22,R.string.jobs_m22,R.string.primaryEducation_m22,R.string.higherEducation_m22));
        fields.add(new Field("مهندسی راه آهن","math_field_23.png","math_field_23.jpg",1,R.string.aboutField_m23,R.string.jobs_m23,R.string.primaryEducation_m23,R.string.higherEducation_m23));
        fields.add(new Field("مهندسی کشاورزی","math_field_24.png","math_field_24.jpg",1,R.string.aboutField_m24,R.string.jobs_m24,R.string.primaryEducation_m24,R.string.higherEducation_m24));
        fields.add(new Field("علوم مهندسی","math_field_25.png","math_field_25.jpg",1,R.string.aboutField_m25,R.string.jobs_m25,R.string.primaryEducation_m25,R.string.higherEducation_m25));
        fields.add(new Field("معماری داخلی","math_field_26.png","math_field_26.jpg",1,R.string.aboutField_m26,R.string.jobs_m26,R.string.primaryEducation_m26,R.string.higherEducation_m26));
        fields.add(new Field("کاردان فنی مکانیک","math_field_27.png","math_field_27.jpg",1,R.string.aboutField_m27,R.string.jobs_m27,R.string.primaryEducation_m27,R.string.higherEducation_m27));
        fields.add(new Field("کاردان فنی عمران","math_field_28.png","math_field_28.jpg",1,R.string.aboutField_m28,R.string.jobs_m28,R.string.primaryEducation_m28,R.string.higherEducation_m28));
        fields.add(new Field("کاردانی مراقبت پرواز","math_field_29.png","math_field_29.jpg",1,R.string.aboutField_m29,R.string.jobs_m29,R.string.primaryEducation_m29,R.string.higherEducation_m29));
        fields.add(new Field("کاردان فنی برق","math_field_30.png","math_field_30.jpg",1,R.string.aboutField_m30,R.string.jobs_m30,R.string.primaryEducation_m30,R.string.higherEducation_m30));
        fields.add(new Field("کاردانی هواپیما","math_field_31.png","math_field_31.jpg",1,R.string.aboutField_m31,R.string.jobs_m31,R.string.primaryEducation_m31,R.string.higherEducation_m31));
        fields.add(new Field("کاردان فنی معدن","math_field_32.png","math_field_32.jpg",1,R.string.aboutField_m32,R.string.jobs_m32,R.string.primaryEducation_m32,R.string.higherEducation_m32));
        fields.add(new Field("مکاترونیک","math_field_33.png","math_field_33.jpg",1,R.string.aboutField_m33,R.string.jobs_m33,R.string.primaryEducation_m33,R.string.higherEducation_m33));
        fields.add(new Field("علوم کامپیوتر","math_field_34.png","math_field_34.jpg",1,R.string.aboutField_m34,R.string.jobs_m34,R.string.primaryEducation_m34,R.string.higherEducation_m34));
        fields.add(new Field("ریاضی","math_field_35.png","math_field_35.jpg",1,R.string.aboutField_m35,R.string.jobs_m35,R.string.primaryEducation_m35,R.string.higherEducation_m35));
        fields.add(new Field("شیمی","math_field_36.png","math_field_36.jpg",1,R.string.aboutField_m36,R.string.jobs_m36,R.string.primaryEducation_m36,R.string.higherEducation_m36));
        fields.add(new Field("آمار","math_field_37.png","math_field_37.jpg",1,R.string.aboutField_m37,R.string.jobs_m37,R.string.primaryEducation_m37,R.string.higherEducation_m37));
        fields.add(new Field("حسابداری","math_field_38.png","math_field_38.jpg",1,R.string.aboutField_m38,R.string.jobs_m38,R.string.primaryEducation_m38,R.string.higherEducation_m38));
        fields.add(new Field("فیزیک","math_field_39.png","math_field_39.jpg",1,R.string.aboutField_m39,R.string.jobs_m39,R.string.primaryEducation_m39,R.string.higherEducation_m39));
        fields.add(new Field("فیزیک مهندسی","math_field_40.png","math_field_40.jpg",1,R.string.aboutField_m40,R.string.jobs_m40,R.string.primaryEducation_m40,R.string.higherEducation_m40));
        fields.add(new Field("ریاضیات و کاربردها","math_field_41.png","math_field_41.jpg",1,R.string.aboutField_m41,R.string.jobs_m41,R.string.primaryEducation_m41,R.string.higherEducation_m41));
        fields.add(new Field("کارشناسی چند رسانه ای","math_field_42.png","math_field_42.jpg",1,R.string.aboutField_m42,R.string.jobs_m42,R.string.primaryEducation_m42,R.string.higherEducation_m42));
        fields.add(new Field("هوانوردی - خلبانی","math_field_43.png","math_field_43.jpg",1,R.string.aboutField_m43,R.string.jobs_m43,R.string.primaryEducation_m43,R.string.higherEducation_m43));
        fields.add(new Field("مهندسی بازرسی فنی","math_field_44.png","math_field_44.jpg",1,R.string.aboutField_m44,R.string.jobs_m44,R.string.primaryEducation_m44,R.string.higherEducation_m44));
        fields.add(new Field("مهندسی ماشینهای کشاورزی","math_field_45.png","math_field_45.jpg",1,R.string.aboutField_m45,R.string.jobs_m45,R.string.primaryEducation_m45,R.string.higherEducation_m45));
        fields.add(new Field("هوانوردی-ناوبری هوایی","math_field_46.png","math_field_46.jpg",1,R.string.aboutField_m46,R.string.jobs_m46,R.string.primaryEducation_m46,R.string.higherEducation_m46));
        fields.add(new Field("مهندسی مدیریت اجرایی","math_field_47.png","math_field_47.jpg",1,R.string.aboutField_m47,R.string.jobs_m47,R.string.primaryEducation_m47,R.string.higherEducation_m47));
        fields.add(new Field("مهندسی مواد و طراحی صنایع غذایی","math_field_48.png","math_field_48.jpg",1,R.string.aboutField_m48,R.string.jobs_m48,R.string.primaryEducation_m48,R.string.higherEducation_m48));
        fields.add(new Field("مهندسی مکانیک نیروگاه","math_field_49.png","math_field_49.jpg",1,R.string.aboutField_m49,R.string.jobs_m49,R.string.primaryEducation_m49,R.string.higherEducation_m49));
        fields.add(new Field("علوم و فنون هوانوردی - خلبانی هلیکوپتری","math_field_50.png","math_field_50.jpg",1,R.string.aboutField_m50,R.string.jobs_m50,R.string.primaryEducation_m50,R.string.higherEducation_m50));
        fields.add(new Field("مهندسی فرماندهی و کنترل هوایی","math_field_51.png","math_field_51.jpg",1,R.string.aboutField_m51,R.string.jobs_m51,R.string.primaryEducation_m51,R.string.higherEducation_m51));
        fields.add(new Field("مهندسی اپتیک و لیزر","math_field_52.png","math_field_52.jpg",1,R.string.aboutField_m52,R.string.jobs_m52,R.string.primaryEducation_m52,R.string.higherEducation_m52));
        fields.add(new Field("پزشکی- دکتری","empirical_field_1.png","empirical_field_1.jpg",2,R.string.aboutField_e1,R.string.jobs_e1,R.string.primaryEducation_e1,R.string.higherEducation_e1));
        fields.add(new Field("دندانپزشکی- دکتری","empirical_field_2.png","empirical_field_2.jpg",2,R.string.aboutField_e2,R.string.jobs_e2,R.string.primaryEducation_e2,R.string.higherEducation_e2));
        fields.add(new Field("داروسازی- دکتری","empirical_field_3.png","empirical_field_3.jpg",2,R.string.aboutField_e3,R.string.jobs_e3,R.string.primaryEducation_e3,R.string.higherEducation_e3));
        fields.add(new Field("دکتری پیوسته بیوتکنولوژی","empirical_field_4.png","empirical_field_4.jpg",2,R.string.aboutField_e4,R.string.jobs_e4,R.string.primaryEducation_e4,R.string.higherEducation_e4));
        fields.add(new Field("دامپزشکی- دکتری","empirical_field_5.png","empirical_field_5.jpg",2,R.string.aboutField_e5,R.string.jobs_e5,R.string.primaryEducation_e5,R.string.higherEducation_e5));
        fields.add(new Field("زیست شناسی","empirical_field_6.png","empirical_field_6.jpg",2,R.string.aboutField_e6,R.string.jobs_e6,R.string.primaryEducation_e6,R.string.higherEducation_e6));
        fields.add(new Field("فیزیوتراپی","empirical_field_7.png","empirical_field_7.jpg",2,R.string.aboutField_e7,R.string.jobs_e7,R.string.primaryEducation_e7,R.string.higherEducation_e7));
        fields.add(new Field("اعضای مصنوعی","empirical_field_8.png","empirical_field_8.jpg",2,R.string.aboutField_e8,R.string.jobs_e8,R.string.primaryEducation_e8,R.string.higherEducation_e8));
        fields.add(new Field("بهداشت مواد غذایی","empirical_field_9.png","empirical_field_9.jpg",2,R.string.aboutField_e9,R.string.jobs_e9,R.string.primaryEducation_e9,R.string.higherEducation_e9));
        fields.add(new Field("مدیریت و کمیسر دریایی","empirical_field_10.png","empirical_field_10.jpg",2,R.string.aboutField_e10,R.string.jobs_e10,R.string.primaryEducation_e10,R.string.higherEducation_e10));
        fields.add(new Field("دبیری زیست شناسی","empirical_field_12.png","empirical_field_12.jpg",2,R.string.aboutField_e12,R.string.jobs_e12,R.string.primaryEducation_e12,R.string.higherEducation_e12));
        fields.add(new Field("تکنولوژی مرتع و آبخیزداری","empirical_field_13.png","empirical_field_13.jpg",2,R.string.aboutField_e13,R.string.jobs_e13,R.string.primaryEducation_e13,R.string.higherEducation_e13));
        fields.add(new Field("ارگونومی","empirical_field_14.png","empirical_field_14.jpg",2,R.string.aboutField_e14,R.string.jobs_e14,R.string.primaryEducation_e14,R.string.higherEducation_e14));
        fields.add(new Field("اتاق عمل","empirical_field_15.png","empirical_field_.15jpg",2,R.string.aboutField_e15,R.string.jobs_e15,R.string.primaryEducation_e15,R.string.higherEducation_e15));
        fields.add(new Field("هوشبری","empirical_field_16.png","empirical_field_16.jpg",2,R.string.aboutField_e16,R.string.jobs_e16,R.string.primaryEducation_e16,R.string.higherEducation_e16));
        fields.add(new Field("پروتزهای دندانی","empirical_field_17.png","empirical_field_17.jpg",2,R.string.aboutField_e17,R.string.jobs_e17,R.string.primaryEducation_e17,R.string.higherEducation_e17));
        fields.add(new Field("بینایی سنجی","empirical_field_18.png","empirical_field_18.jpg",2,R.string.aboutField_e18,R.string.jobs_e18,R.string.primaryEducation_e18,R.string.higherEducation_e18));
        fields.add(new Field("شنوایی شناسی","empirical_field_19.png","empirical_field_19.jpg",2,R.string.aboutField_e19,R.string.jobs_e19,R.string.primaryEducation_e19,R.string.higherEducation_e19));
        fields.add(new Field("علوم آزمایشگاهی","empirical_field_20.png","empirical_field_20.jpg",2,R.string.aboutField_e20,R.string.jobs_e20,R.string.primaryEducation_e20,R.string.higherEducation_e20));
        fields.add(new Field("علوم آزمایشگاهی دامپزشکی","empirical_field_21.png","empirical_field_21.jpg",2,R.string.aboutField_e21,R.string.jobs_e21,R.string.primaryEducation_e21,R.string.higherEducation_e21));
        fields.add(new Field("پزشکی - کتابداری","empirical_field_22.png","empirical_field_22.jpg",2,R.string.aboutField_e22,R.string.jobs_e22,R.string.primaryEducation_e22,R.string.higherEducation_e22));
        fields.add(new Field("مامایی","empirical_field_23.png","empirical_field_23.jpg",2,R.string.aboutField_e23,R.string.jobs_e23,R.string.primaryEducation_e23,R.string.higherEducation_e23));
        fields.add(new Field("علوم تغذیه","empirical_field_24.png","empirical_field_24.jpg",2,R.string.aboutField_e24,R.string.jobs_e24,R.string.primaryEducation_e24,R.string.higherEducation_e24));
        fields.add(new Field("علوم و صنایع غذایی","empirical_field_25.png","empirical_field_25.jpg",2,R.string.aboutField_e25,R.string.jobs_e25,R.string.primaryEducation_e25,R.string.higherEducation_e25));
        fields.add(new Field("مهندسی بهداشت محیط","empirical_field_26.png","empirical_field_26.jpg",2,R.string.aboutField_e26,R.string.jobs_e26,R.string.primaryEducation_e26,R.string.higherEducation_e26));
        fields.add(new Field("مهندسی فضای سبز","empirical_field_27.png","empirical_field_27.jpg",2,R.string.aboutField_e27,R.string.jobs_e27,R.string.primaryEducation_e27,R.string.higherEducation_e27));
        fields.add(new Field("کارشناسی بهداشت عمومی","empirical_field_28.png","empirical_field_28.jpg",2,R.string.aboutField_e28,R.string.jobs_e28,R.string.primaryEducation_e28,R.string.higherEducation_e28));
        fields.add(new Field("مهندسی منابع طبیعی","empirical_field_29.png","empirical_field_29.jpg",2,R.string.aboutField_e29,R.string.jobs_e29,R.string.primaryEducation_e29,R.string.higherEducation_e29));
        fields.add(new Field("کاردانی دامپزشکی","empirical_field_30.png","empirical_field_30.jpg",2,R.string.aboutField_e30,R.string.jobs_e30,R.string.primaryEducation_e30,R.string.higherEducation_e30));
        fields.add(new Field("پرستاری","empirical_field_31.png","empirical_field_31.jpg",2,R.string.aboutField_e31,R.string.jobs_e31,R.string.primaryEducation_e31,R.string.higherEducation_e31));
        fields.add(new Field("مدارک پزشکی","empirical_field_32.png","empirical_field_32.jpg",2,R.string.aboutField_e32,R.string.jobs_e32,R.string.primaryEducation_e32,R.string.higherEducation_e32));
        fields.add(new Field("زیست شناسی سلولی مولکولی","empirical_field_33.png","empirical_field_33.jpg",2,R.string.aboutField_e33,R.string.jobs_e33,R.string.primaryEducation_e33,R.string.higherEducation_e33));
        fields.add(new Field("مهندسی بهداشت حرفه ای","empirical_field_34.png","empirical_field_34.jpg",2,R.string.aboutField_e34,R.string.jobs_e34,R.string.primaryEducation_e34,R.string.higherEducation_e34));
        fields.add(new Field("مهندسی تولیدات گیاهی","empirical_field_35.png","empirical_field_35.jpg",2,R.string.aboutField_e35,R.string.jobs_e35,R.string.primaryEducation_e35,R.string.higherEducation_e35));
        fields.add(new Field("کارشناسی فناوری اطلاعات سلامت","empirical_field_36.png","empirical_field_36.jpg",2,R.string.aboutField_e36,R.string.jobs_e36,R.string.primaryEducation_e36,R.string.higherEducation_e36));
        fields.add(new Field("علوم مهندسی زیست محیطی","empirical_field_37.png","empirical_field_37.jpg",2,R.string.aboutField_e37,R.string.jobs_e37,R.string.primaryEducation_e37,R.string.higherEducation_e37));
        fields.add(new Field("دبیری شیمی","empirical_field_38.png","empirical_field_38.jpg",2,R.string.aboutField_e38,R.string.jobs_e38,R.string.primaryEducation_e38,R.string.higherEducation_e38));
        fields.add(new Field("شیمی","empirical_field_39.png","empirical_field_39.jpg",2,R.string.aboutField_e39,R.string.jobs_e39,R.string.primaryEducation_e39,R.string.higherEducation_e39));
        fields.add(new Field("گفتار درمانی","empirical_field_40.png","empirical_field_40.jpg",2,R.string.aboutField_e40,R.string.jobs_e40,R.string.primaryEducation_e40,R.string.higherEducation_e40));
        fields.add(new Field("تکنولوژی تولیدات دامی","empirical_field_41.png","empirical_field_41.jpg",2,R.string.aboutField_e41,R.string.jobs_e41,R.string.primaryEducation_e41,R.string.higherEducation_e41));
        fields.add(new Field("کاردانی فوریت\u200Cهای پزشکی","empirical_field_42.png","empirical_field_42.jpg",2,R.string.aboutField_e42,R.string.jobs_e42,R.string.primaryEducation_e42,R.string.higherEducation_e42));
        fields.add(new Field("مهندسی منابع طبیعی-شیلات","empirical_field_43.png","empirical_field_43.jpg",2,R.string.aboutField_e43,R.string.jobs_e43,R.string.primaryEducation_e43,R.string.higherEducation_e43));
        fields.add(new Field("مهندسی کشاورزی-زراعت و اصلاح نباتات","empirical_field_44.png","empirical_field_44.jpg",2,R.string.aboutField_e44,R.string.jobs_e44,R.string.primaryEducation_e44,R.string.higherEducation_e44));
        fields.add(new Field("مهندسی کشاورزی-علوم دامی","empirical_field_45.png","empirical_field_45.jpg",2,R.string.aboutField_e45,R.string.jobs_e45,R.string.primaryEducation_e45,R.string.higherEducation_e45));
        fields.add(new Field("اقیانوس شناسی","empirical_field_46.png","empirical_field_46.jpg",2,R.string.aboutField_e46,R.string.jobs_e46,R.string.primaryEducation_e46,R.string.higherEducation_e46));
        fields.add(new Field("زمین شناسی","empirical_field_47.png","empirical_field_47.jpg",2,R.string.aboutField_e47,R.string.jobs_e47,R.string.primaryEducation_e47,R.string.higherEducation_e47));
        fields.add(new Field("کاردرمانی","empirical_field_48.png","empirical_field_48.jpg",2,R.string.aboutField_e48,R.string.jobs_e48,R.string.primaryEducation_e48,R.string.higherEducation_e48));
        fields.add(new Field("مهندسی کشاورزی-علوم و صنایع غذایی","empirical_field_49.png","empirical_field_49.jpg",2,R.string.aboutField_e49,R.string.jobs_e49,R.string.primaryEducation_e49,R.string.higherEducation_e49));
        fields.add(new Field("کارشناسی تکنولوژی پرتوشناسی- رادیولوژی","empirical_field_50.png","empirical_field_50.jpg",2,R.string.aboutField_e50,R.string.jobs_e50,R.string.primaryEducation_e50,R.string.higherEducation_e50));
        fields.add(new Field("رشته ایمنی شناسی (ایمونولوژی)","empirical_field_51.png","empirical_field_51.jpg",2,R.string.aboutField_e51,R.string.jobs_e51,R.string.primaryEducation_e51,R.string.higherEducation_e51));
        fields.add(new Field("کارشناسی تکنولوژی پزشکی هسته\u200Cای","empirical_field_52.png","empirical_field_52.jpg",2,R.string.aboutField_e52,R.string.jobs_e52,R.string.primaryEducation_e52,R.string.higherEducation_e52));
        fields.add(new Field("مهندسی کشاورزی-ترویج و آموزش کشاورزی","empirical_field_53.png","empirical_field_53.jpg",2,R.string.aboutField_e53,R.string.jobs_e53,R.string.primaryEducation_e53,R.string.higherEducation_e53));
        fields.add(new Field("مهندسی منابع طبیعی-مهندسی چوب","empirical_field_54.png","empirical_field_54.jpg",2,R.string.aboutField_e54,R.string.jobs_e54,R.string.primaryEducation_e54,R.string.higherEducation_e54));
        fields.add(new Field("مهندسی منابع طبیعی-جنگلداری","empirical_field_55.png","empirical_field_55.jpg",2,R.string.aboutField_e55,R.string.jobs_e55,R.string.primaryEducation_e55,R.string.higherEducation_e55));
        fields.add(new Field("کاردانی شیمی-پدافند جنگهای میکروبی","empirical_field_56.png","empirical_field_56.jpg",2,R.string.aboutField_e56,R.string.jobs_e56,R.string.primaryEducation_e56,R.string.higherEducation_e56));
        fields.add(new Field("کاردانی ناپیوسته علمی-کاربردی پرورش زنبورعسل","empirical_field_57.png","empirical_field_57.jpg",2,R.string.aboutField_e57,R.string.jobs_e57,R.string.primaryEducation_e57,R.string.higherEducation_e57));
        fields.add(new Field("کاردانی علمی، کاربردی تولید و بهره\u200Cبرداری گیاهان دارویی و معطر","empirical_field_58.png","empirical_field_58.jpg",2,R.string.aboutField_e58,R.string.jobs_e58,R.string.primaryEducation_e58,R.string.higherEducation_e58));
        fields.add(new Field("حقوق","human_field_1.png","human_field_1.jpg",3,R.string.aboutField_h1,R.string.jobs_h1,R.string.primaryEducation_h1,R.string.higherEducation_h1));
        fields.add(new Field("کارشناسی ارشد الهیات","human_field_2.png","human_field_2.jpg",3,R.string.aboutField_h2,R.string.jobs_h2,R.string.primaryEducation_h2,R.string.higherEducation_h2));
        fields.add(new Field("مطالعات ارتباطی","human_field_3.png","human_field_3.jpg",3,R.string.aboutField_h3,R.string.jobs_h3,R.string.primaryEducation_h3,R.string.higherEducation_h3));
        fields.add(new Field("علوم اقتصادی","human_field_4.png","human_field_4.jpg",3,R.string.aboutField_h4,R.string.jobs_h4,R.string.primaryEducation_h4,R.string.higherEducation_h4));
        fields.add(new Field("مدیریت","human_field_5.png","human_field_5.jpg",3,R.string.aboutField_h5,R.string.jobs_h5,R.string.primaryEducation_h5,R.string.higherEducation_h5));
        fields.add(new Field("علوم سیاسی","human_field_6.png","human_field_6.jpg",3,R.string.aboutField_h6,R.string.jobs_h6,R.string.primaryEducation_h6,R.string.higherEducation_h6));
        fields.add(new Field("علوم قضایی","human_field_7.png","human_field_7.jpg",3,R.string.aboutField_h7,R.string.jobs_h7,R.string.primaryEducation_h7,R.string.higherEducation_h7));
        fields.add(new Field("تربیت بدنی","human_field_8.png","human_field_8.jpg",3,R.string.aboutField_h8,R.string.jobs_h8,R.string.primaryEducation_h8,R.string.higherEducation_h8));
        fields.add(new Field("علوم اجتماعی","human_field_9.png","human_field_9.jpg",3,R.string.aboutField_h9,R.string.jobs_h9,R.string.primaryEducation_h9,R.string.higherEducation_h9));
        fields.add(new Field("تفسیر قرآن مجید","human_field_10.png","human_field_10.jpg",3,R.string.aboutField_h10,R.string.jobs_h10,R.string.primaryEducation_h10,R.string.higherEducation_h10));
        fields.add(new Field("روان\u200Cشناسی","human_field_11.png","human_field_11.jpg",3,R.string.aboutField_h11,R.string.jobs_h11,R.string.primaryEducation_h11,R.string.higherEducation_h11));
        fields.add(new Field("زبان و ادبیات فارسی","human_field_12.png","human_field_12.jpg",3,R.string.aboutField_h12,R.string.jobs_h12,R.string.primaryEducation_h12,R.string.higherEducation_h12));
        fields.add(new Field("زبان و ادبیات عربی","human_field_13.png","human_field_13.jpg",3,R.string.aboutField_h13,R.string.jobs_h13,R.string.primaryEducation_h13,R.string.higherEducation_h13));
        fields.add(new Field("روابط عمومی","human_field_14.png","human_field_14.jpg",3,R.string.aboutField_h14,R.string.jobs_h14,R.string.primaryEducation_h14,R.string.higherEducation_h14));
        fields.add(new Field("مطالعات خانواده","human_field_15.png","human_field_15.jpg",3,R.string.aboutField_h15,R.string.jobs_h15,R.string.primaryEducation_h15,R.string.higherEducation_h15));
        fields.add(new Field("کتابداری","human_field_16.png","human_field_16.jpg",3,R.string.aboutField_h16,R.string.jobs_h16,R.string.primaryEducation_h16,R.string.higherEducation_h16));
        fields.add(new Field("روزنامه نگاری","human_field_17.png","human_field_17.jpg",3,R.string.aboutField_h17,R.string.jobs_h17,R.string.primaryEducation_h17,R.string.higherEducation_h17));
        fields.add(new Field("مددکاری اجتماعی","human_field_18.png","human_field_18.jpg",3,R.string.aboutField_h18,R.string.jobs_h18,R.string.primaryEducation_h18,R.string.higherEducation_h18));
        fields.add(new Field("راهنمایی و مشاوره","human_field_19.png","human_field_19.jpg",3,R.string.aboutField_h19,R.string.jobs_h19,R.string.primaryEducation_h19,R.string.higherEducation_h19));
        fields.add(new Field("ادبیات داستانی","human_field_20.png","human_field_20.jpg",3,R.string.aboutField_h20,R.string.jobs_h20,R.string.primaryEducation_h20,R.string.higherEducation_h20));
        fields.add(new Field("آب و هوا شناسی","human_field_21.png","human_field_21.jpg",3,R.string.aboutField_h21,R.string.jobs_h21,R.string.primaryEducation_h21,R.string.higherEducation_h21));
        fields.add(new Field("ژئومورفولوژی","human_field_22.png","human_field_22.jpg",3,R.string.aboutField_h22,R.string.jobs_h22,R.string.primaryEducation_h22,R.string.higherEducation_h22));
        fields.add(new Field("فقه و حقوق","human_field_23.png","human_field_23.jpg",3,R.string.aboutField_h23,R.string.jobs_h23,R.string.primaryEducation_h23,R.string.higherEducation_h23));
        fields.add(new Field("جغرافیا","human_field_24.png","human_field_24.jpg",3,R.string.aboutField_h24,R.string.jobs_h24,R.string.primaryEducation_h24,R.string.higherEducation_h24));
        fields.add(new Field("مدیریت هتلداری","human_field_25.png","human_field_25.jpg",3,R.string.aboutField_h25,R.string.jobs_h25,R.string.primaryEducation_h25,R.string.higherEducation_h25));
        fields.add(new Field("علوم تربیتی","human_field_26.png","human_field_26.jpg",3,R.string.aboutField_h26,R.string.jobs_h26,R.string.primaryEducation_h26,R.string.higherEducation_h26));
        fields.add(new Field("مدیریت جهانگردی","human_field_27.png","human_field_27.jpg",3,R.string.aboutField_h27,R.string.jobs_h27,R.string.primaryEducation_h27,R.string.higherEducation_h27));
        fields.add(new Field("دبیری جغرافیا","human_field_28.png","human_field_28.jpg",3,R.string.aboutField_h28,R.string.jobs_h28,R.string.primaryEducation_h28,R.string.higherEducation_h28));
        fields.add(new Field("امور گمرکی","human_field_29.png","human_field_29.jpg",3,R.string.aboutField_h29,R.string.jobs_h29,R.string.primaryEducation_h29,R.string.higherEducation_h29));
        fields.add(new Field("تاریخ","human_field_30.png","human_field_30.jpg",3,R.string.aboutField_h30,R.string.jobs_h30,R.string.primaryEducation_h30,R.string.higherEducation_h30));
        fields.add(new Field("علوم حدیث","human_field_31.png","human_field_31.jpg",3,R.string.aboutField_h31,R.string.jobs_h31,R.string.primaryEducation_h31,R.string.higherEducation_h31));
        fields.add(new Field("فلسفه","human_field_32.png","human_field_32.jpg",3,R.string.aboutField_h32,R.string.jobs_h32,R.string.primaryEducation_h32,R.string.higherEducation_h32));
        fields.add(new Field("رشد و پرورش کودکان پیش دبستانی","human_field_33.png","human_field_33.jpg",3,R.string.aboutField_h33,R.string.jobs_h33,R.string.primaryEducation_h33,R.string.higherEducation_h33));
        fields.add(new Field("دبیری زبان و ادبیات عربی","human_field_34.png","human_field_34.jpg",3,R.string.aboutField_h34,R.string.jobs_h34,R.string.primaryEducation_h34,R.string.higherEducation_h34));
        fields.add(new Field("دبیری زبان و ادبیات فارسی","human_field_35.png","human_field_35.jpg",3,R.string.aboutField_h35,R.string.jobs_h35,R.string.primaryEducation_h35,R.string.higherEducation_h35));
        fields.add(new Field("علوم قرآنی و حدیث","human_field_36.png","human_field_36.jpg",3,R.string.aboutField_h36,R.string.jobs_h36,R.string.primaryEducation_h36,R.string.higherEducation_h36));
        fields.add(new Field("علم اطلاعات ودانش شناسی","human_field_37","human_field_37.jpg",3,R.string.aboutField_h37,R.string.jobs_h37,R.string.primaryEducation_h37,R.string.higherEducation_h37));
        fields.add(new Field("مدیریت - بازرگانی دریایی","human_field_38.png","human_field_38.jpg",3,R.string.aboutField_h38,R.string.jobs_h38,R.string.primaryEducation_h38,R.string.higherEducation_h38));
        fields.add(new Field("علوم ارتباطات اجتماعی","human_field_39.png","human_field_39.jpg",3,R.string.aboutField_h39,R.string.jobs_h39,R.string.primaryEducation_h39,R.string.higherEducation_h39));
        fields.add(new Field("الهیات و معارف اسلامی","human_field_40.png","human_field_40.jpg",3,R.string.aboutField_h40,R.string.jobs_h40,R.string.primaryEducation_h40,R.string.higherEducation_h40));

    }
    public List<Field> getDataList(int groupId){
        List<Field> fieldList;
        switch (groupId){
            case 1:
                fieldList=fields.subList(0,52);
                break;
            case 2:
                fieldList=fields.subList(52,109);
                break;
            case 3:
                fieldList=fields.subList(109,149);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + groupId);
        }
        return fieldList;
    }
    public Field getDataSingle(int id , int groupId){
        List<Field> fieldList;
        Field field;
        switch (groupId){
            case 1:
                fieldList=fields.subList(0,52);
                field = fieldList.get(id);
                break;
            case 2:
                fieldList=fields.subList(52,109);
                field = fieldList.get(id);
                break;
            case 3:
                fieldList=fields.subList(109,149);
                field = fieldList.get(id);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + groupId);
        }
        return field;
    }

    private static DataFieldList mDatas;
    public static DataFieldList get(){
        if (mDatas == null){
            mDatas = new DataFieldList();

        }
        return mDatas;
    }


}
