package com.amirhossein.myKonkorApp.model;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.List;

public class DBAnalytic extends SQLiteAssetHelper {
    private static final String DATABASE_NAME = "analytic.db";
    private static final int DATABASE_VERSION = 1;
    int groupId ;
    Context context;

    public DBAnalytic(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    public DBAnalytic(Context context,int groupId) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.groupId = groupId;
        this.context = context;
    }

    public List<Analytic> getAllAnalytic(){
        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(context);
        databaseAccess.openDatabase();
        List<Analytic> analyticList = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + tblName();

        @SuppressLint("Recycle")
        Cursor cursor = databaseAccess.getReadableDataBase().rawQuery(selectQuery,null);
        if (cursor.moveToFirst()){
            do {
                Analytic analytic = new Analytic();
                analytic.setId(cursor.getInt(cursor.getColumnIndex(Analytic.COLUMN_ID)));
                analytic.setTopic(cursor.getString(cursor.getColumnIndex(Analytic.COLUMN_TOPIC)));
                analytic.setTotal(cursor.getInt(cursor.getColumnIndex(Analytic.COLUMN_TOTAL)));
                analytic.setCorrect(cursor.getInt(cursor.getColumnIndex(Analytic.COLUMN_CORRECT)));
                analytic.setWrong(cursor.getInt(cursor.getColumnIndex(Analytic.COLUMN_WRONG)));

                analyticList.add(analytic);
            }while (cursor.moveToNext());
        }
        databaseAccess.closeDatabase();
        return analyticList;
    }

    public Analytic getAnalytic(String topic){
        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(context);
        databaseAccess.openDatabase();
        Cursor cursor = databaseAccess.getReadableDataBase().query(tblName(),
                new String[]{Analytic.COLUMN_ID,Analytic.COLUMN_TOPIC,Analytic.COLUMN_TOTAL,Analytic.COLUMN_CORRECT,Analytic.COLUMN_WRONG},
                Analytic.COLUMN_TOPIC+"=?",
                new String[]{topic},null,null,null,null);

        if (cursor != null)
            cursor.moveToFirst();

        assert cursor != null;
        Analytic analytic = new Analytic(
                cursor.getInt(cursor.getColumnIndex(Analytic.COLUMN_ID)),
                cursor.getString(cursor.getColumnIndex(Analytic.COLUMN_TOPIC)),
                cursor.getInt(cursor.getColumnIndex(Analytic.COLUMN_TOTAL)),
                cursor.getInt(cursor.getColumnIndex(Analytic.COLUMN_CORRECT)),
                cursor.getInt(cursor.getColumnIndex(Analytic.COLUMN_WRONG))
        );

        cursor.close();
        databaseAccess.closeDatabase();
        return analytic;
    }

    public int updateCorrectAnalytic(String topic){
        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(context);
        databaseAccess.openDatabase();
        Analytic analytic = getAnalytic(topic);
        int correct = analytic.getCorrect()+1;
        ContentValues contentValues = new ContentValues();
        contentValues.put(Analytic.COLUMN_CORRECT,correct);
        return databaseAccess.getWritableDatabase().update(tblName(),contentValues,Analytic.COLUMN_TOPIC+"=?",
                new String[]{topic});
    }

    public int updateWrongAnalytic(String topic){
        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(context);
        databaseAccess.openDatabase();
        Analytic analytic = getAnalytic(topic);
        int wrong = analytic.getWrong()+1;
        ContentValues contentValues = new ContentValues();
        contentValues.put(Analytic.COLUMN_WRONG,wrong);
        return databaseAccess.getWritableDatabase().update(tblName(),contentValues,Analytic.COLUMN_TOPIC+"=?",
                new String[]{topic});
    }

    public int updateTotalAnalytic(String topic){
        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(context);
        databaseAccess.openDatabase();
        Analytic analytic = getAnalytic(topic);
        int total = analytic.getTotal()+1;
        ContentValues contentValues = new ContentValues();
        contentValues.put(Analytic.COLUMN_TOTAL,total);
        return databaseAccess.getWritableDatabase().update(tblName(),contentValues,Analytic.COLUMN_TOPIC+"=?",
                new String[]{topic});
    }

    private String tblName(){
        String tblName = Analytic.TBL_GROUP1;
        if (groupId==1){
            tblName = Analytic.TBL_GROUP1;
        } else if (groupId==2) {
            tblName = Analytic.TBL_GROUP2;
        } else if (groupId==3) {
            tblName = Analytic.TBL_GROUP3;
        }
        return tblName;
    }



}
