package com.amirhossein.myKonkorApp.model;

import com.google.gson.annotations.SerializedName;

public class University {

    private int id;
    private String name;
    private String imageLarge;
    private String imageShort;
    private String rating;
    private String logo;
    private String city;
    private String state;
    private int description;
    private int college;
    private String link;
    @SerializedName("location")
    private String location;
    @SerializedName("latitude")
    private String latitude;
    @SerializedName("longitude")
    private String longitude ;


    public University() {
    }

    public University(int id, String imageLarge, String imageShort, int description, int college, String link, String name) {
        this.id = id;
        this.imageLarge = imageLarge;
        this.imageShort = imageShort;
        this.college = college;
        this.description = description;
        this.link = link;
        this.name = name;
    }

    public University(int id, String image, int description, String link) {
        this.id = id;
        this.imageLarge = image;
        this.description = description;
        this.link = link;

    }

    public University(String name, String rating, String logo, String city, String state, int id) {
        this.name = name;
        this.rating = rating;
        this.logo = logo;
        this.city = city;
        this.state = state;
        this.id = id;
    }

    public int getCollege() {
        return college;
    }



    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLocation() {
        return location;
    }

    public int getDescription() {
        return description;
    }

    public String getLink() {
        return link;
    }

    public String getLogo() {
        return logo;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageLarge() {
        return imageLarge;
    }


    public String getRating() {
        return rating;
    }

    public String getImageShort() {
        return imageShort;
    }
}



