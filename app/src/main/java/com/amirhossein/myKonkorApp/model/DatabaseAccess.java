package com.amirhossein.myKonkorApp.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseAccess {
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static DatabaseAccess instance;

    private DatabaseAccess(Context context) {
        this.openHelper = new DBAnalytic(context);
    }

    public static DatabaseAccess getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseAccess(context);
        }
        return instance;
    }

    public void openDatabase() {
        this.database = openHelper.getWritableDatabase();
    }

    public SQLiteDatabase getWritableDatabase(){

        return openHelper.getWritableDatabase();

    }

    public SQLiteDatabase getReadableDataBase()
    {
        return openHelper.getReadableDatabase();
    }

    public void closeDatabase() {
        if (database != null) {
            this.database.close();
        }
    }
}
