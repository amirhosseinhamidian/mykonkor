package com.amirhossein.myKonkorApp.model;

import com.google.gson.annotations.SerializedName;

public class Article {

    @SerializedName("id")
    private int id;
    @SerializedName("title")
    private String title;
    @SerializedName("date")
    private String date;
    @SerializedName("image")
    private String image;
    @SerializedName("source")
    private String source;
    private int like;

    public Article() {
    }

    public String getSource() {
        return source;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getLike() {
        return like;
    }

    public void setLike(int like) {
        this.like = like;
    }
}
