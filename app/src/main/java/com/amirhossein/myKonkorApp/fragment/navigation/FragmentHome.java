package com.amirhossein.myKonkorApp.fragment.navigation;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.amirhossein.myKonkorApp.activity.AnalyticActivity;
import com.amirhossein.myKonkorApp.activity.FieldList;
import com.amirhossein.myKonkorApp.activity.ReadField;
import com.amirhossein.myKonkorApp.activity.ReadUniversity;
import com.amirhossein.myKonkorApp.activity.UniversityList;
import com.amirhossein.myKonkorApp.activity.Percent;
import com.amirhossein.myKonkorApp.R;
import com.amirhossein.myKonkorApp.fragment.DialogTestCondition;
import com.amirhossein.myKonkorApp.model.DataFieldList;
import com.amirhossein.myKonkorApp.model.DataUniversityRead;
import com.amirhossein.myKonkorApp.model.Field;
import com.amirhossein.myKonkorApp.model.University;
import com.amirhossein.myKonkorApp.widget.LalezarTextView;
import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import static android.content.Context.MODE_PRIVATE;
import static com.amirhossein.myKonkorApp.activity.ReadField.PREFS_FIELD;
import static com.amirhossein.myKonkorApp.activity.ReadUniversity.PREFS_UNIVERSITY;
import static com.amirhossein.myKonkorApp.activity.Welcome.USER_INFO;
import static com.amirhossein.myKonkorApp.common.Constants.EXTRA_IS_ROOT_FRAGMENT;
import static com.amirhossein.myKonkorApp.common.Constants.HOME_FRAGMENT;

public class FragmentHome extends BaseFragment {

    public static final String ACTION_ARTICLE = HOME_FRAGMENT + "action.article";

    private ImageView img_percent , img_analytic,img_daily_exam,img_favorite_field,img_favorite_university;
    private RelativeLayout rl_university ,rl_university_first, rl_field, rl_field_first;
    private LalezarTextView tv_multi_home , tv_favorite_field , tv_favorite_university;
    private Button btn_multi;
    private Context getContext;

    public static final String SHARED_PREFS = "sharedPrefs";
    private SharedPreferences.Editor editor ;

    private float multi = 1.0f ;
    private long clickTime;
    private long resetMultiTime;
    private String countDownTimer;

    public FragmentHome(){}

    public static FragmentHome newInstance(boolean isRoot) {
        Bundle args = new Bundle();
        args.putBoolean(EXTRA_IS_ROOT_FRAGMENT, isRoot);
        FragmentHome fragment = new FragmentHome();
        fragment.setArguments(args);
        return fragment;
    }
    @SuppressLint("SetTextI18n")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home , container,false);
        findView(view);

        assert container != null;
        getContext = container.getContext();
        SharedPreferences sharedPreferences = getContext.getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        editor = sharedPreferences.edit();

        multi = sharedPreferences.getFloat("multi",1.0f);
        clickTime = sharedPreferences.getLong("clickTime",0);
        resetMultiTime = sharedPreferences.getLong("resetMulti",0);

        //setting of multi
        long remainingTimeMulti = clickTime - timeNow();
        long remainingResetMultiTime = resetMultiTime - timeNow();
        if (remainingTimeMulti >0){
             btn_multi.setEnabled(false);
             new CountDownTimer(86400000-remainingTime(), 1000) {

                 @SuppressLint("DefaultLocale")
                 public void onTick(long millisUntilFinished) {
                     long secondsInMilli = 1000;
                     long minutesInMilli = secondsInMilli * 60;
                     long hoursInMilli = minutesInMilli * 60;

                     long elapsedHours = millisUntilFinished / hoursInMilli;
                     millisUntilFinished = millisUntilFinished % hoursInMilli;

                     long elapsedMinutes = millisUntilFinished / minutesInMilli;
                     millisUntilFinished = millisUntilFinished % minutesInMilli;

                     long elapsedSeconds = millisUntilFinished / secondsInMilli;

                     countDownTimer = String.format("%02d:%02d:%02d", elapsedHours, elapsedMinutes,elapsedSeconds);
                     btn_multi.setText(countDownTimer);

                 }
                 public void onFinish() {

                     btn_multi.setText("ضریب بزن!");
                     btn_multi.setEnabled(true);
                 }
             }.start();

        }else {
            btn_multi.setEnabled(true);
            btn_multi.setText("ضریب بزن!");
            if(remainingResetMultiTime <0){
                multi = 1.0f;
                editor.putFloat("multi",multi);
                editor.apply();
            }
        }
        tv_multi_home.setText("× "+multi);
        btn_multi.setTypeface(Typeface.createFromAsset(getContext.getAssets(),"Lalezar-Regular.otf"));
        btn_multi.setOnClickListener(new onClickMulti());

        rl_university_first.setOnClickListener(view14 -> {
            Intent intent = new Intent(getContext, UniversityList.class);
            startActivity(intent);
        });
        rl_field.setOnClickListener(view13 -> {
            Intent intent = new Intent(getContext, FieldList.class);
            startActivity(intent);
            Objects.requireNonNull(getActivity()).finish();
        });
        img_daily_exam.setOnClickListener(new OnClickDailyExam());
        img_percent.setOnClickListener(view12 -> {
            Intent intent = new Intent(getContext,Percent.class);
            startActivity(intent);
        });
        img_analytic.setOnClickListener(view1 -> {
            Intent intent = new Intent(getContext , AnalyticActivity.class);
            startActivity(intent);
        });

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        setTargetUniversity();
        setTargetField();

    }

    private void setTargetUniversity() {

        SharedPreferences preferences =getContext.getSharedPreferences(PREFS_UNIVERSITY,MODE_PRIVATE);
        int universityFavoriteId = preferences.getInt("university_favorite_id",-1);

        int id = preferences.getInt("id",0);
        int position = preferences.getInt("position",-1);
        String rate = preferences.getString("rate","");
        String name = preferences.getString("name","");
        String city = preferences.getString("city","");
        String state = preferences.getString("state","");

        if (universityFavoriteId>=0) {
            University university = DataUniversityRead.get().getData(universityFavoriteId);

            String imageBaseUrl = "http://amirhosseinhamidian.ir/image/university_short/";
            Picasso.get().load(imageBaseUrl + university.getImageShort()).into(img_favorite_university);
            tv_favorite_university.setText(university.getName());
            rl_university_first.setVisibility(View.GONE);
            rl_university.setVisibility(View.VISIBLE);

            img_favorite_university.setOnClickListener(view -> {
                Intent intent = new Intent(getContext, ReadUniversity.class);
                intent.putExtra("id",id);
                intent.putExtra("position",position);
                intent.putExtra("rate",rate);
                intent.putExtra("name",name);
                intent.putExtra("city",city);
                intent.putExtra("state",state);
                startActivity(intent);
            });
        }else {
            rl_university.setVisibility(View.GONE);
            rl_university_first.setVisibility(View.VISIBLE);
            img_favorite_university.setOnClickListener(view -> {
                Intent intent = new Intent(getContext, UniversityList.class);
                startActivity(intent);
            });
        }
    }

    private void setTargetField() {
        SharedPreferences prefer = getContext.getSharedPreferences(USER_INFO,MODE_PRIVATE);
        int groupId = prefer.getInt("groupId",0);

        SharedPreferences preferences =getContext.getSharedPreferences(PREFS_FIELD,MODE_PRIVATE);
        int fieldFavoriteId = preferences.getInt("field_favorite_id",-1);
        int id = preferences.getInt("idField",0);
        String title = preferences.getString("titleField","");

        if (fieldFavoriteId>=0) {
            Field field = DataFieldList.get().getDataSingle(fieldFavoriteId, groupId);

            String imageBaseUrl = "http://amirhosseinhamidian.ir/image/field_short/";
            Picasso.get().load(imageBaseUrl + field.getImageShort()).into(img_favorite_field);
            tv_favorite_field.setText(field.getTitle());
            rl_field.setVisibility(View.VISIBLE);
            rl_field_first.setVisibility(View.GONE);

            img_favorite_field.setOnClickListener(view -> {
                Intent intent = new Intent(getContext, ReadField.class);
                intent.putExtra("idField",id);
                intent.putExtra("titleField",title);
                startActivity(intent);
            });
        }else {
            rl_field.setVisibility(View.GONE);
            rl_field_first.setVisibility(View.VISIBLE);

            rl_field_first.setOnClickListener(view -> {
                Intent intent = new Intent(getContext, FieldList.class);
                startActivity(intent);
            });
        }

    }

    private long timeNow() {

        Calendar nowGMT = Calendar.getInstance();
        long offset = nowGMT.get(Calendar.ZONE_OFFSET) +
                nowGMT.get(Calendar.DST_OFFSET);

        return  (nowGMT.getTimeInMillis() + offset)/1000;
    }

    private void findView(View view) {

        img_percent = view.findViewById(R.id.img_percent);
        img_analytic = view.findViewById(R.id.img_analytic);
        img_daily_exam = view.findViewById(R.id.img_daily_exam);
        rl_university_first = view.findViewById(R.id.rl_uni_first);
        rl_university = view.findViewById(R.id.rl_uni);
        rl_field = view.findViewById(R.id.rl_field);
        rl_field_first = view.findViewById(R.id.rl_field_first);
        tv_multi_home = view.findViewById(R.id.tv_multi_home);
        btn_multi = view.findViewById(R.id.btn_multi);
        tv_favorite_field = view.findViewById(R.id.tv_favorite_field);
        tv_favorite_university = view.findViewById(R.id.tv_favorite_university);
        img_favorite_field = view.findViewById(R.id.img_favorite_field);
        img_favorite_university = view.findViewById(R.id.img_favorite_university);
    }

    private class OnClickDailyExam implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            DialogTestCondition dialog = new DialogTestCondition(getContext);
            dialog.setContentView(R.layout.dialog_test_condition);
            Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(R.color.transparentGray);
            dialog.show();

        }
    }

    private class onClickMulti implements View.OnClickListener {


        @SuppressLint("SetTextI18n")
        @Override
        public void onClick(View view) {


            long timeUntilMidnight = (86400000 - remainingTime()) / 1000;
            clickTime = timeNow() + timeUntilMidnight;
            resetMultiTime = clickTime+86400;
            multi += 0.1f;
            tv_multi_home.setText("× "+multi);

            editor.putLong("clickTime",clickTime);
            editor.putFloat("multi",multi);
            editor.putLong("resetMulti",resetMultiTime);
            editor.apply();

            new CountDownTimer(86400000-remainingTime(), 1000) {

                @SuppressLint("DefaultLocale")
                public void onTick(long millisUntilFinished) {
                    long secondsInMilli = 1000;
                    long minutesInMilli = secondsInMilli * 60;
                    long hoursInMilli = minutesInMilli * 60;

                    long elapsedHours = millisUntilFinished / hoursInMilli;
                    millisUntilFinished = millisUntilFinished % hoursInMilli;

                    long elapsedMinutes = millisUntilFinished / minutesInMilli;
                    millisUntilFinished = millisUntilFinished % minutesInMilli;

                    long elapsedSeconds = millisUntilFinished / secondsInMilli;

                    countDownTimer = String.format("%02d:%02d:%02d", elapsedHours, elapsedMinutes,elapsedSeconds);
                    btn_multi.setText(countDownTimer);
                    btn_multi.setEnabled(false);


                }


                public void onFinish() {

                    btn_multi.setText("ضریب بزن!");
                    btn_multi.setEnabled(true);
                }
            }.start();

        }
    }

    private long remainingTime(){
        Calendar rightNow = Calendar.getInstance();


        long offset = rightNow.get(Calendar.ZONE_OFFSET) +
                rightNow.get(Calendar.DST_OFFSET);

        return (rightNow.getTimeInMillis() + offset) %
                (24 * 60 * 60 * 1000);
    }



}
