package com.amirhossein.myKonkorApp.fragment.navigation;

import android.content.Context;
import android.os.Bundle;

import org.jetbrains.annotations.NotNull;

import androidx.fragment.app.Fragment;

public class BaseFragment extends Fragment {
    private FragmentInteractionCallback fragmentInteractionCallback;
    private static String currentTab;

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        try {
            fragmentInteractionCallback = (FragmentInteractionCallback) context;
        } catch (ClassCastException e) {
            throw new RuntimeException(context.toString() + " must implement " + FragmentInteractionCallback.class.getName());
        }
    }

    @Override
    public void onDetach() {
        fragmentInteractionCallback = null;
        super.onDetach();
    }

    public interface FragmentInteractionCallback {

        void onFragmentInteractionCallback(Bundle bundle);
    }

    public static void setCurrentTab(String currentTab) {
        BaseFragment.currentTab = currentTab;
    }


}
