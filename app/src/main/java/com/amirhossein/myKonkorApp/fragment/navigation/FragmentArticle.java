package com.amirhossein.myKonkorApp.fragment.navigation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.airbnb.lottie.LottieAnimationView;
import com.amirhossein.myKonkorApp.R;
import com.amirhossein.myKonkorApp.activity.FieldList;
import com.amirhossein.myKonkorApp.activity.UniversityList;
import com.amirhossein.myKonkorApp.adapter.AdapterAllArticle;
import com.amirhossein.myKonkorApp.model.Article;
import com.amirhossein.myKonkorApp.webService.APIClient;
import com.amirhossein.myKonkorApp.webService.APIInterface;
import com.amirhossein.myKonkorApp.widget.MyEditText;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.amirhossein.myKonkorApp.common.Constants.ARTICLE_FRAGMENT;
import static com.amirhossein.myKonkorApp.common.Constants.EXTRA_IS_ROOT_FRAGMENT;

public class FragmentArticle extends BaseFragment {

    public static final String ACTION_RATING = ARTICLE_FRAGMENT + "action.rating";
    private RecyclerView rv_all;
    private Context getContext;
    private RelativeLayout layoutUniversity,layoutField;
    private MyEditText search;
    private LottieAnimationView animationView;

    public FragmentArticle(){}

    public static FragmentArticle newInstance(boolean isRoot) {
        Bundle args = new Bundle();
        args.putBoolean(EXTRA_IS_ROOT_FRAGMENT, isRoot);
        FragmentArticle fragment = new FragmentArticle();
        fragment.setArguments(args);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_article, container , false);
        findView(view);
        assert container != null;
        getContext = container.getContext();
        layoutUniversity.setOnClickListener(view1 -> {
            startActivity(new Intent(getContext, UniversityList.class));
        });
        layoutField.setOnClickListener(view1 -> {
            startActivity(new Intent(getContext, FieldList.class));
        });
        return view;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestAllArticle();

    }

    private void requestAllArticle() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<List<Article>> call = apiInterface.getArticle();
        call.enqueue(new Callback<List<Article>>() {
            @Override
            public void onResponse(@NotNull Call<List<Article>> call, @NotNull Response<List<Article>> response) {
                if (response.isSuccessful()){
                    animationView.setVisibility(View.GONE);
                    List<Article> articles = response.body();
                    setUpRecyclerView(articles);

                }
            }

            @Override
            public void onFailure(@NotNull Call<List<Article>> call, @NotNull Throwable t) {

            }
        });
    }

    private void setUpRecyclerView(List<Article> articles) {
        AdapterAllArticle adapter = new AdapterAllArticle(getContext,articles);
        rv_all.setLayoutManager(new LinearLayoutManager(getContext));
        rv_all.setAdapter(adapter);
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adapter.getFilter().filter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void findView(View view) {
        layoutUniversity = view.findViewById(R.id.rl_university_article);
        layoutField = view.findViewById(R.id.rl_field_article);
        rv_all = view.findViewById(R.id.rv_vertical_article);
        search = view.findViewById(R.id.edt_search_article_list);
        animationView = view.findViewById(R.id.lottie_loading_article);
    }


}
