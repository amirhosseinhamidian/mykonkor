package com.amirhossein.myKonkorApp.fragment.navigation.utils;

import android.os.Bundle;

import com.amirhossein.myKonkorApp.fragment.navigation.BaseFragment;

import java.util.Map;
import java.util.Objects;
import java.util.Stack;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import static com.amirhossein.myKonkorApp.common.Constants.ACTION;
import static com.amirhossein.myKonkorApp.common.Constants.DATA_KEY_1;
import static com.amirhossein.myKonkorApp.common.Constants.DATA_KEY_2;

public class FragmentUtils {

    private static final String TAG_SEPARATOR = ":";

    public static void addInitialTabFragment(FragmentManager fragmentManager,
                                             Map<String, Stack<String>> tagStacks,
                                             String tag,
                                             Fragment fragment,
                                             int layoutId,
                                             boolean shouldAddToStack) {
        fragmentManager
                .beginTransaction()
                .add(layoutId, fragment, fragment.getClass().getName() + TAG_SEPARATOR + fragment.hashCode())
                .commit();
        if (shouldAddToStack) tagStacks.get(tag).push(fragment.getClass().getName() + TAG_SEPARATOR + fragment.hashCode());
    }

    public static void addAdditionalTabFragment(FragmentManager fragmentManager,
                                                Map<String, Stack<String>> tagStacks,
                                                String tag,
                                                Fragment show,
                                                Fragment hide,
                                                int layoutId,
                                                boolean shouldAddToStack) {
        fragmentManager
                .beginTransaction()
                .add(layoutId, show, show.getClass().getName() + TAG_SEPARATOR + show.hashCode())
                .show(show)
                .hide(hide)
                .commit();
        if (shouldAddToStack) tagStacks.get(tag).push(show.getClass().getName() + TAG_SEPARATOR + show.hashCode());
    }

    public static void showHideTabFragment(FragmentManager fragmentManager,
                                           Fragment show,
                                           Fragment hide) {
        fragmentManager
                .beginTransaction()
                .hide(hide)
                .show(show)
                .commit();
    }

    public static void addShowHideFragment(FragmentManager fragmentManager,
                                           Map<String, Stack<String>> tagStacks,
                                           String tag,
                                           Fragment show,
                                           Fragment hide,
                                           int layoutId,
                                           boolean shouldAddToStack) {
        fragmentManager
                .beginTransaction()
                .add(layoutId, show, show.getClass().getName() + TAG_SEPARATOR + show.hashCode())
                .show(show)
                .hide(hide)
                .commit();
        if (shouldAddToStack) Objects.requireNonNull(tagStacks.get(tag)).push(show.getClass().getName() + TAG_SEPARATOR + show.hashCode());
    }

    public static void removeFragment(FragmentManager fragmentManager, Fragment show, Fragment remove) {
        fragmentManager
                .beginTransaction()
                .remove(remove)
                .show(show)
                .commit();
    }

    public static void sendActionToActivity(String action, String tab, boolean shouldAdd, BaseFragment.FragmentInteractionCallback fragmentInteractionCallback) {
        Bundle bundle = new Bundle();
        bundle.putString(ACTION, action);
        bundle.putString(DATA_KEY_1, tab);
        bundle.putBoolean(DATA_KEY_2, shouldAdd);
        fragmentInteractionCallback.onFragmentInteractionCallback(bundle);
    }


}
