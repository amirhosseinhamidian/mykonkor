package com.amirhossein.myKonkorApp.fragment.navigation;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.amirhossein.myKonkorApp.R;
import com.amirhossein.myKonkorApp.model.DatabaseHelperScore;
import com.amirhossein.myKonkorApp.model.Score;
import com.amirhossein.myKonkorApp.model.User;
import com.amirhossein.myKonkorApp.webService.APIClient;
import com.amirhossein.myKonkorApp.webService.APIInterface;
import com.amirhossein.myKonkorApp.widget.IranSansBold;
import com.amirhossein.myKonkorApp.widget.LalezarTextView;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;
import static com.amirhossein.myKonkorApp.activity.Welcome.USER_INFO;
import static com.amirhossein.myKonkorApp.common.Constants.EDIT_PROFILE_PICTURE;
import static com.amirhossein.myKonkorApp.common.Constants.EXTRA_IS_ROOT_FRAGMENT;
import static com.amirhossein.myKonkorApp.common.Constants.PROFILE_FRAGMENT;
import static com.amirhossein.myKonkorApp.common.Constants.SIGN_IN_GOOGLE;
import static com.amirhossein.myKonkorApp.common.Constants.SIGN_IN_GUEST;
import static com.amirhossein.myKonkorApp.common.Constants.SOUND;


public class FragmentProfile extends BaseFragment {

    public static final String ACTION_RATING = PROFILE_FRAGMENT + "action.rating";

    private CircleImageView profileImage;
    private Button save;
    private IranSansBold numberLevel ,totalScore , textViewEmail;
    private LalezarTextView textGoogleLogIn;
    private EditText editTextUsername;
    private RoundCornerProgressBar progressBar;
    private Switch sound;
    private LinearLayout googleLogIn,linearLayout;
    private RelativeLayout layoutImage;

    private int groupId;
    private String typeSignIn;
    private String image;
    private int RQE_CODE = 11;
    private int GALLERY_CODE = 101;
    private static final int RC_SIGN_IN =1;
    private Context getContext;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private GoogleSignInClient mGoogleSignInClient;
    private GoogleSignInOptions gso;


    public FragmentProfile(){}

    public static FragmentProfile newInstance(boolean isRoot) {
        Bundle args = new Bundle();
        args.putBoolean(EXTRA_IS_ROOT_FRAGMENT, isRoot);
        FragmentProfile fragment = new FragmentProfile();
        fragment.setArguments(args);
        return fragment;
    }

    @SuppressLint("CommitPrefEdits")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile,container,false);
        findView(view);
        assert container != null;
        getContext = container.getContext();

        preferences = getContext.getSharedPreferences(USER_INFO, MODE_PRIVATE);
        editor = preferences.edit();
        typeSignIn = preferences.getString("signIn",SIGN_IN_GUEST);
        image = preferences.getString("image","");

        setProfilePicture();
        editProfilePicture();
        setInfo();
        setRateTotal();
        googleLogInButton();
        switchSound();
        rateToMe();
        saveChange();

        return view;
    }

    private void saveChange() {
        save.setOnClickListener(view -> {
            String username = String.valueOf(editTextUsername.getText());
            if (!editTextUsername.getText().equals(username)) {
                editor.putString("username", username);
                editor.apply();
                Snackbar snackbar =Snackbar.make(linearLayout,"تغییرات با موفقیت ذخیره شد!",Snackbar.LENGTH_SHORT);
                snackbar.show();
                updateRequestUsername(username);



            }
        });
    }

    private void updateRequestUsername(String username) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        if (typeSignIn.equals(SIGN_IN_GOOGLE)){
            Call<Void> call = apiInterface.updateUsernameByEmail(username,preferences.getString("email",""));
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {

                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {

                }
            });
        }else {
            @SuppressLint("HardwareIds")
            String android_id = Settings.Secure.getString(getContext().getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            Call<Void> call = apiInterface.updateUsername(username,android_id);
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {

                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {

                }
            });

        }
    }

    @SuppressLint("SetTextI18n")
    private void setInfo() {
        String email = preferences.getString("email","");
        String username = preferences.getString("username","");
        Typeface typeface = Typeface.createFromAsset(getContext.getAssets(),"IRANSansBold.ttf");
        if (!email.equals("")) {
            textViewEmail.setText(email + "@gmail.com");
        }
        editTextUsername.setTypeface(typeface);
        editTextUsername.setText(username);
    }

    private void rateToMe() {
    }

    private void switchSound() {

        boolean p = preferences.getBoolean(SOUND,true);
        sound.setChecked(p);
        sound.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            editor.putBoolean(SOUND,isChecked);
            editor.apply();
        });
    }

    private void googleLogInButton() {
        if (typeSignIn.equals(SIGN_IN_GOOGLE)){

            textGoogleLogIn.setText("وارد شدید!");
        }else{

            gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();
            mGoogleSignInClient = GoogleSignIn.getClient(getContext, gso);
            googleLogIn.setOnClickListener(view -> {

                Intent intent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(intent,RC_SIGN_IN);
            });

        }
    }

    private void setRateTotal() {
        DatabaseHelperScore db = new DatabaseHelperScore(getContext);
        List<Score> scores = db.getAllScore();
        int total=0;
        int max = preferences.getInt("max",0);
        int min = preferences.getInt("min",0);
        int level = preferences.getInt("level",0);
        for (int i=0 ; i<scores.size() ; i++){
            Score score = scores.get(i);
            total += score.getTotalScore();
        }

        if (total >= max){

            int temp = min;
            min = max;
            double change = temp+(425* Math.pow(1.1,level));
            max = (int) Math.round(change);
            level++;
            editor.putInt("max",max);
            editor.putInt("min",min);
            editor.putInt("level",level);
            editor.apply();
        }

        progressBar.setProgressColor(Color.parseColor("#5362FB"));
        progressBar.setProgressBackgroundColor(Color.parseColor("#F4F5F9"));
        progressBar.setMax(max);
        progressBar.setProgress(total-min);
        ObjectAnimator progressAnimator = ObjectAnimator.ofFloat(progressBar,"progress",0,total-min);
        progressAnimator.setDuration(1000);
        progressAnimator.setInterpolator(new LinearInterpolator());
        progressAnimator.start();


        numberLevel.setText(String.valueOf(level));
        totalScore.setText(String.valueOf(total));

    }

    private void editProfilePicture() {
        layoutImage.setOnClickListener(view -> {
            if (Build.VERSION.SDK_INT>Build.VERSION_CODES.M){
                if (ContextCompat.checkSelfPermission(getContext, Manifest.permission.READ_EXTERNAL_STORAGE)
                        !=PackageManager.PERMISSION_GRANTED){

                    ActivityCompat.requestPermissions((Activity) getContext,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},RQE_CODE);
                }else {
                    Intent galleryIntent = new Intent();
                    galleryIntent.setAction(Intent.ACTION_PICK);
                    galleryIntent.setType("image/*");
                    startActivityForResult(galleryIntent,GALLERY_CODE);
                }
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_CODE && data != null){

            if (resultCode == RESULT_OK){

                Uri selectedImage = data.getData();
                InputStream in;
                try {
                    assert selectedImage != null;
                    in = getContext.getContentResolver().openInputStream(selectedImage);
                    Bitmap bitmap = BitmapFactory.decodeStream(in);
                    profileImage.setImageBitmap(bitmap);

                    //convert bitmap to base64 string for to save in share preferences
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    byte[] b = byteArrayOutputStream.toByteArray();
                    String encoded = Base64.encodeToString(b, Base64.DEFAULT);


                    editor.putString("editProfile",EDIT_PROFILE_PICTURE);
                    editor.putString("bitmapEditProfile",encoded);
                    editor.apply();



                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }

            if (resultCode == RESULT_CANCELED){

            }


        }

        if (requestCode == RC_SIGN_IN) {
            if (resultCode == RESULT_OK) {
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                GoogleSignInAccount account = task.getResult();

                assert account != null;
                String username = account.getGivenName();
                String email = account.getEmail();
                String image = String.valueOf(account.getPhotoUrl());
                registerUserGoogle(username, email, image);
                SharedPreferences.Editor editor = getContext.getSharedPreferences(USER_INFO, MODE_PRIVATE).edit();
                editor.putString("username", username);
                editor.putString("email", email);
                editor.putString("image", image);
                editor.apply();
            }
        }
    }

    private void registerUserGoogle(String username, String email, String image) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<Void> call = apiInterface.registerUserGoogle(username,groupId,email,image);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NotNull Call<Void> call, @NotNull Response<Void> response) {
                if (response.isSuccessful()) {
                    setProfilePicture();
                    setInfo();
                    textGoogleLogIn.setText("وارد شدید!");
                }
            }

            @Override
            public void onFailure(@NotNull Call<Void> call, @NotNull Throwable t) {
                Snackbar snackbar = Snackbar.make(linearLayout,"اتصال اینترنت برقرار نیست!",Snackbar.LENGTH_SHORT);
                snackbar.show();
            }
        });
    }

    private void setProfilePicture() {

        String editProfile = preferences.getString("editProfile","");
        String encoded = preferences.getString("bitmapEditProfile","");

        if (typeSignIn.equals(SIGN_IN_GOOGLE)){
            if (editProfile.equals(EDIT_PROFILE_PICTURE)){

                byte[] imageAsBytes = Base64.decode(encoded.getBytes(), Base64.DEFAULT);
                profileImage.setImageBitmap(BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));

            }else {
                Picasso.get()
                        .load(image)
                        .placeholder(R.drawable.notprofile)
                        .error(R.drawable.notprofile)
                        .into(profileImage);
            }


        }else {
            if (editProfile.equals(EDIT_PROFILE_PICTURE)){

                byte[] imageAsBytes = Base64.decode(encoded.getBytes(), Base64.DEFAULT);
                profileImage.setImageBitmap(BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));

            }else {
                profileImage.setImageResource(R.drawable.notprofile);
            }
        }
    }

    private void findView(View view) {
        profileImage = view.findViewById(R.id.img_profile);
        save = view.findViewById(R.id.btn_save_profile);
        editTextUsername = view.findViewById(R.id.edt_username_profile);
        numberLevel = view.findViewById(R.id.tv_level_number_profile);
        progressBar = view.findViewById(R.id.pb_level_profile);
        sound = view.findViewById(R.id.switch_sound_profile);
        googleLogIn = view.findViewById(R.id.ll_login_google_profile);
        layoutImage = view.findViewById(R.id.rl_image_profile);
        totalScore = view.findViewById(R.id.tv_total_score_profile);
        textGoogleLogIn = view.findViewById(R.id.tv_google_log_in);
        textViewEmail = view.findViewById(R.id.tv_email_profile);
        linearLayout = view.findViewById(R.id.ll_profile);
    }




}
