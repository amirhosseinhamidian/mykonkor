package com.amirhossein.myKonkorApp.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.LinearLayout;

import com.amirhossein.myKonkorApp.R;
import com.amirhossein.myKonkorApp.widget.MyButton;

import androidx.annotation.NonNull;

public class DialogExitExam extends Dialog {
    private Context mContext;
    private MyButton leave , noContinue;
    private LinearLayout linearLayoutDialog;
    public DialogExitExam(@NonNull Context context) {
        super(context);
        this.mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_exit_exam);
        findViews();
        linearLayoutDialog.setBackgroundResource(R.drawable.background_exit_exam);

        leave.setOnClickListener(view -> ((Activity) mContext).finish());

        noContinue.setOnClickListener(view -> dismiss());
    }

    private void findViews() {
        linearLayoutDialog = findViewById(R.id.ll_dialog_exit_exam);
        leave = findViewById(R.id.btn_yes_leave);
        noContinue = findViewById(R.id.btn_no_continue);
    }
}
