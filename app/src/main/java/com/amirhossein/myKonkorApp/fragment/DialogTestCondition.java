package com.amirhossein.myKonkorApp.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.amirhossein.myKonkorApp.R;
import com.amirhossein.myKonkorApp.activity.ExamRoom;
import com.amirhossein.myKonkorApp.activity.NoInternetConnection;
import com.amirhossein.myKonkorApp.widget.ShabnamTextView;
import androidx.annotation.NonNull;

import static com.amirhossein.myKonkorApp.activity.Welcome.USER_INFO;

public class DialogTestCondition extends Dialog  {

    private ShabnamTextView textViewMinus5 , textViewPlus5, textViewDisplay, textViewStart,
            textViewTime;
    private Switch pLesson,sLesson;
    private LinearLayout linearLayoutDialog;
    private Context mContext;

    private int numberOfQuestion = 5 , topicId ,groupId;

    public DialogTestCondition(@NonNull Context context) {
        super(context);
        this.mContext = context;
    }

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_test_condition);
        findView();


        linearLayoutDialog.setBackgroundResource(R.drawable.background_dialog);

        textViewPlus5.setOnClickListener(view -> {
            if (numberOfQuestion >= 5 && numberOfQuestion < 30){
                numberOfQuestion += 5;
                textViewDisplay.setText(String.valueOf(numberOfQuestion));
                calculateTimeExamWithNumberButton(numberOfQuestion);
            }

        });

        textViewMinus5.setOnClickListener(view -> {
            if (numberOfQuestion > 5 && numberOfQuestion <= 30){
                numberOfQuestion -= 5;
                textViewDisplay.setText(String.valueOf(numberOfQuestion));
                calculateTimeExamWithNumberButton(numberOfQuestion);
            }
        });

        SharedPreferences prefer = mContext.getSharedPreferences(USER_INFO, Context.MODE_PRIVATE);
        groupId = prefer.getInt("groupId",0);

       calculateTimeExam();

        //انتخاب نوع آزمون
        textViewStart.setOnClickListener(view -> {
            if (sLesson.isChecked() ||pLesson.isChecked()){
                if (!sLesson.isChecked() && pLesson.isChecked()){
                    topicId = 1;
                }else if (sLesson.isChecked() && !pLesson.isChecked()){
                    topicId = 2;
                }else if (sLesson.isChecked() && pLesson.isChecked()){
                    topicId = 3;
                }

                if (isNetworkConnected()){
                    Intent intent = new Intent(mContext, ExamRoom.class);
                    intent.putExtra("topicId",topicId);
                    intent.putExtra("numberOfQuestion",numberOfQuestion);
                    mContext.startActivity(intent);
                    dismiss();

                }else {
                    Intent intent = new Intent(mContext, NoInternetConnection.class);
                    mContext.startActivity(intent);
                }




            }else {
                Toast.makeText(getContext(),"مبحث آزمون را انتخاب کنید!",Toast.LENGTH_SHORT)
                        .show();



            }
        });

    }

    private boolean isNetworkConnected() {
        boolean result = false;
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (cm != null) {
                NetworkCapabilities capabilities = cm.getNetworkCapabilities(cm.getActiveNetwork());
                if (capabilities != null) {
                    if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                        result = true;
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                        result = true;
                    }
                }
            }
        } else {
            if (cm != null) {
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                if (activeNetwork != null) {
                    // connected to the internet
                    if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                        result = true;
                    } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                        result = true;
                    }
                }
            }
        }
        return result;
    }

    private void calculateTimeExam() {

        pLesson.setOnCheckedChangeListener((compoundButton, b) -> {

            if (b && !sLesson.isChecked()){

                double time = (numberOfQuestion * 45)/60;
                int round = (int) Math.round(time);
                textViewTime.setText(String.valueOf(round));

            }else if (b && groupId == 1){

                double time = (numberOfQuestion * 60)/60;
                int round = (int) Math.round(time);
                textViewTime.setText(String.valueOf(round));

            }else if (b && groupId == 2){

                double time = (numberOfQuestion * 57)/60;
                int round = (int) Math.round(time);
                textViewTime.setText(String.valueOf(round));

            }else if (b && groupId == 3){

                double time = (numberOfQuestion * 55)/60;
                int round = (int) Math.round(time);
                textViewTime.setText(String.valueOf(round));

            }else if (!b && sLesson.isChecked() && groupId == 1){

                double time = (numberOfQuestion * 75)/60;
                int round = (int) Math.round(time);
                textViewTime.setText(String.valueOf(round));

            }else if (!b && sLesson.isChecked() && groupId == 2){

                double time = (numberOfQuestion * 70)/60;
                int round = (int) Math.round(time);
                textViewTime.setText(String.valueOf(round));

            }else if (!b && sLesson.isChecked() && groupId == 3){

                double time = (numberOfQuestion * 60)/60;
                int round = (int) Math.round(time);
                textViewTime.setText(String.valueOf(round));

            }else if (!b && !sLesson.isChecked() ){

                textViewTime.setText("0");

            }
        });

        sLesson.setOnCheckedChangeListener((compoundButton, b) -> {

            if (b && !pLesson.isChecked() && groupId == 1){

                double time = (numberOfQuestion * 75)/60;
                int round = (int) Math.round(time);
                textViewTime.setText(String.valueOf(round));

            }else if(b && !pLesson.isChecked() && groupId == 2){

                double time = (numberOfQuestion * 70)/60;
                int round = (int) Math.round(time);
                textViewTime.setText(String.valueOf(round));

            }else if(b && !pLesson.isChecked() && groupId == 3){

                double time = (numberOfQuestion * 60)/60;
                int round = (int) Math.round(time);
                textViewTime.setText(String.valueOf(round));

            }else if (b && pLesson.isChecked() && groupId == 1){

                double time = (numberOfQuestion * 60)/60;
                int round = (int) Math.round(time);
                textViewTime.setText(String.valueOf(round));

            }else if (b && pLesson.isChecked() && groupId == 2){

                double time = (numberOfQuestion * 57)/60;
                int round = (int) Math.round(time);
                textViewTime.setText(String.valueOf(round));

            }else if (b && pLesson.isChecked() && groupId == 3){

                double time = (numberOfQuestion * 55)/60;
                int round = (int) Math.round(time);
                textViewTime.setText(String.valueOf(round));

            }else if (!b && pLesson.isChecked() ){

                double time = (numberOfQuestion * 45)/60;
                int round = (int) Math.round(time);
                textViewTime.setText(String.valueOf(round));

            }else if (!b && !pLesson.isChecked() ){

                textViewTime.setText("0");

            }
        });
    }

    private void calculateTimeExamWithNumberButton(int numOfQuestion){

        if (!sLesson.isChecked() && pLesson.isChecked()){

            double time = (numOfQuestion * 45)/60;
            int round = (int) Math.round(time);
            textViewTime.setText(String.valueOf(round));

        }else if(sLesson.isChecked() && !pLesson.isChecked() && groupId == 1){

            double time = (numOfQuestion * 75)/60;
            int round = (int) Math.round(time);
            textViewTime.setText(String.valueOf(round));

        }else if(sLesson.isChecked() && !pLesson.isChecked() && groupId == 2){

            double time = (numOfQuestion * 70)/60;
            int round = (int) Math.round(time);
            textViewTime.setText(String.valueOf(round));

        }else if(sLesson.isChecked() && !pLesson.isChecked() && groupId == 3){

            double time = (numOfQuestion * 60)/60;
            int round = (int) Math.round(time);
            textViewTime.setText(String.valueOf(round));

        }else if(sLesson.isChecked() && pLesson.isChecked() && groupId == 1){

            double time = (numOfQuestion * 60)/60;
            int round = (int) Math.round(time);
            textViewTime.setText(String.valueOf(round));

        }else if(sLesson.isChecked() && pLesson.isChecked() && groupId == 2){

            double time = (numOfQuestion * 60)/60;
            int round = (int) Math.round(time);
            textViewTime.setText(String.valueOf(round));

        }else if(sLesson.isChecked() && pLesson.isChecked() && groupId == 3){

            double time = (numOfQuestion * 55)/60;
            int round = (int) Math.round(time);
            textViewTime.setText(String.valueOf(round));

        }
    }


    private void findView() {
        textViewPlus5 = findViewById(R.id.tv_plus_five);
        textViewMinus5 = findViewById(R.id.tv_minus_five);
        textViewDisplay = findViewById(R.id.tv_display_number_quiz);
        sLesson = findViewById(R.id.sw_s_lesson);
        pLesson = findViewById(R.id.sw_p_lesson);
        textViewStart = findViewById(R.id.tv_start_test);
        linearLayoutDialog = findViewById(R.id.ll_dialog_select_exam);
        textViewTime = findViewById(R.id.tv_time_exam);
    }

}
