package com.amirhossein.myKonkorApp.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.airbnb.lottie.LottieAnimationView;
import com.amirhossein.myKonkorApp.R;
import com.amirhossein.myKonkorApp.adapter.AdapterRateList;
import com.amirhossein.myKonkorApp.model.User;
import com.amirhossein.myKonkorApp.webService.APIClient;
import com.amirhossein.myKonkorApp.webService.APIInterface;
import com.amirhossein.myKonkorApp.widget.ShabnamTextView;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;
import static com.amirhossein.myKonkorApp.activity.Welcome.USER_INFO;
import static com.amirhossein.myKonkorApp.common.Constants.SIGN_IN_GOOGLE;
import static com.amirhossein.myKonkorApp.common.Constants.SIGN_IN_GUEST;

public class TotalRating extends Fragment {
    private RecyclerView recyclerView;
    private ShabnamTextView textViewMyScore , textViewMyNumber , textViewMyName;
    private LinearLayout linearLayoutMyScore , linearLayoutImage;
    private CircleImageView imageProfile;
    private LottieAnimationView loading;
    private Context getContext;
    private int groupId;
    private SharedPreferences prefer;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.total_rating, container, false);
        findViews(view);
        assert container != null;
        getContext = container.getContext();

        prefer = getContext.getSharedPreferences(USER_INFO,MODE_PRIVATE);
        groupId = prefer.getInt("groupId",0);
        @SuppressLint("HardwareIds")
        String android_id = Settings.Secure.getString(Objects.requireNonNull(getContext()).getContentResolver(),
                Settings.Secure.ANDROID_ID);

        requestBestStudent();
        requestMyScore(android_id);
        return view;
    }

    private void requestMyScore(String android_id) {

        String email = prefer.getString("email","");
        String typeSignIn = prefer.getString("signIn",SIGN_IN_GUEST);

        if (typeSignIn.equals(SIGN_IN_GOOGLE)){
            APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
            Call<User> call = apiInterface.getMyScoreTotalByEmail(email,groupId);
            call.enqueue(new Callback<User>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(@NotNull Call<User> call, @NotNull Response<User> response) {
                    if (response.isSuccessful()){
                        User user = response.body();
                        assert user != null;
                        if (user.getRankMonth() > 15){
                            linearLayoutMyScore.setVisibility(View.VISIBLE);
                            linearLayoutImage.setVisibility(View.VISIBLE);
                            textViewMyName.setText(user.getUsername());
                            textViewMyNumber.setText(user.getRankMonth()+"");
                            textViewMyScore.setText(user.getScoreMonth()+"");
                            linearLayoutMyScore.setBackgroundResource(R.color.colorPrimaryLight);
                            Picasso.get()
                                    .load(user.getImage())
                                    .error(R.drawable.notprofile)
                                    .placeholder(R.drawable.notprofile)
                                    .into(imageProfile);
                        }else {
                            linearLayoutMyScore.setVisibility(View.GONE);
                            linearLayoutImage.setVisibility(View.GONE);
                        }

                    }
                }

                @Override
                public void onFailure(@NotNull Call<User> call, @NotNull Throwable t) {

                }
            });
        }else {
            APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
            Call<User> call = apiInterface.getMyScoreTotal(android_id,groupId);
            call.enqueue(new Callback<User>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(@NotNull Call<User> call, @NotNull Response<User> response) {
                    if (response.isSuccessful()){
                        User user = response.body();
                        assert user != null;
                        if (user.getRankMonth() > 15){
                            linearLayoutMyScore.setVisibility(View.VISIBLE);
                            linearLayoutImage.setVisibility(View.VISIBLE);
                            textViewMyName.setText(user.getUsername());
                            textViewMyNumber.setText(user.getRankMonth()+"");
                            textViewMyScore.setText(user.getScoreMonth()+"");
                            linearLayoutMyScore.setBackgroundResource(R.color.colorPrimaryLight);
                            Picasso.get()
                                    .load(user.getImage())
                                    .error(R.drawable.notprofile)
                                    .placeholder(R.drawable.notprofile)
                                    .into(imageProfile);
                        }else {
                            linearLayoutMyScore.setVisibility(View.GONE);
                            linearLayoutImage.setVisibility(View.GONE);
                        }

                    }
                }

                @Override
                public void onFailure(@NotNull Call<User> call, @NotNull Throwable t) {

                }
            });
        }
    }

    private void requestBestStudent() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<List<User>> call = apiInterface.getTotalScore(groupId);
        call.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(@NotNull Call<List<User>> call, @NotNull Response<List<User>> response) {
                if (response.isSuccessful()){
                    loading.setVisibility(View.GONE);
                    List<User> userList = response.body();
                    setupRecyclerView(userList);
                }
            }

            @Override
            public void onFailure(@NotNull Call<List<User>> call, @NotNull Throwable t) {

            }
        });
    }

    private void setupRecyclerView(List<User> userList) {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext));
        recyclerView.setAdapter(new AdapterRateList(getContext,userList,userList.size(),User.TOTAL_REQUEST));
    }

    private void findViews(View view) {
        recyclerView = view.findViewById(R.id.rv_total_rate);
        textViewMyScore = view.findViewById(R.id.tv_score_rate_total);
        textViewMyNumber = view.findViewById(R.id.tv_num_rate_total);
        textViewMyName = view.findViewById(R.id.tv_name_rate_total);
        linearLayoutImage = view.findViewById(R.id.ll_image_until_my_rank_total);
        linearLayoutMyScore = view.findViewById(R.id.ll_my_score_total);
        loading = view.findViewById(R.id.lottie_loading_rating_total);
        imageProfile = view.findViewById(R.id.img_profile_rate_total);
    }

}
