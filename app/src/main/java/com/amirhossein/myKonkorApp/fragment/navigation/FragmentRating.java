package com.amirhossein.myKonkorApp.fragment.navigation;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.amirhossein.myKonkorApp.R;
import com.amirhossein.myKonkorApp.fragment.MonthRating;
import com.amirhossein.myKonkorApp.fragment.TotalRating;
import com.amirhossein.myKonkorApp.fragment.WeeklyRating;
import com.google.android.material.tabs.TabLayout;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import static com.amirhossein.myKonkorApp.common.Constants.EXTRA_IS_ROOT_FRAGMENT;
import static com.amirhossein.myKonkorApp.common.Constants.RATING_FRAGMENT;

public class FragmentRating extends BaseFragment {

    public static final String ACTION_PROFILE = RATING_FRAGMENT + "action.profile";
    private ViewPager viewPager;
    private TabLayout tabLayout;

    public FragmentRating(){}

    public static FragmentRating newInstance(boolean isRoot) {
        Bundle args = new Bundle();
        args.putBoolean(EXTRA_IS_ROOT_FRAGMENT, isRoot);
        FragmentRating fragment = new FragmentRating();
        fragment.setArguments(args);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rating , container , false);
        findViews(view);
        setUpViewPager();
        return view;
    }



    private void setUpViewPager() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getFragmentManager());
        adapter.addFragment(new WeeklyRating(),"هفتگی");
        adapter.addFragment(new MonthRating(),"ماهانه");
        adapter.addFragment(new TotalRating(),"کل");
        viewPager.setAdapter(adapter);

        tabLayout.setupWithViewPager(viewPager);

    }

    private void findViews(View view) {
        viewPager = view.findViewById(R.id.vp_rating);
        tabLayout = view.findViewById(R.id.tab_layout_rating);
    }

    private class ViewPagerAdapter extends FragmentStatePagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();
        ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @NotNull
        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        void addFragment(Fragment fragment, String title){
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }
    }


}
