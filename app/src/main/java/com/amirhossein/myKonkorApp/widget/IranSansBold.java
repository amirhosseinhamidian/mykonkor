package com.amirhossein.myKonkorApp.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.annotation.Nullable;

public class IranSansBold extends TextView {
    public IranSansBold(Context context) {
        super(context);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(),"IRANSansBold.ttf");
        this.setTypeface(typeface);
    }

    public IranSansBold(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(),"IRANSansBold.ttf");
        this.setTypeface(typeface);
    }

    public IranSansBold(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(),"IRANSansBold.ttf");
        this.setTypeface(typeface);
    }
}
