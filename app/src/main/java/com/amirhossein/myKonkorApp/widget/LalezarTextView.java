package com.amirhossein.myKonkorApp.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.annotation.Nullable;

public class LalezarTextView extends TextView {
    public LalezarTextView(Context context) {
        super(context);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(),"Lalezar-Regular.otf");
        this.setTypeface(typeface);
    }

    public LalezarTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(),"Lalezar-Regular.otf");
        this.setTypeface(typeface);
    }

    public LalezarTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(),"Lalezar-Regular.otf");
        this.setTypeface(typeface);
    }
}
