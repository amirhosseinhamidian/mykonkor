package com.amirhossein.myKonkorApp.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.annotation.Nullable;

public class ShabnamTextView extends TextView {
    public ShabnamTextView(Context context) {
        super(context);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(),"IRANSans .ttf");
        this.setTypeface(typeface);
    }

    public ShabnamTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(),"IRANSans .ttf");
        this.setTypeface(typeface);
    }

    public ShabnamTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(),"IRANSans .ttf");
        this.setTypeface(typeface);
    }
}
