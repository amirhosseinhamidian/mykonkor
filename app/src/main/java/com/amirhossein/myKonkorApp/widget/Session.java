package com.amirhossein.myKonkorApp.widget;

import android.content.Context;
import android.content.SharedPreferences;

public class Session {
    Context context;
    SharedPreferences preferences;
    public final static String pr_name = "pref";

    boolean loggedIn = false;

    String prl = "loggedIn";

    public Session(Context context) {
        this.context = context;
        preferences = context.getSharedPreferences(pr_name,Context.MODE_PRIVATE);

    }

    public boolean isLoggedIn(){
        return preferences.getBoolean(prl,false);
    }

    public void setLoggedIn(boolean loggedIn){
        preferences.edit().putBoolean(prl,loggedIn).apply();

    }
}
