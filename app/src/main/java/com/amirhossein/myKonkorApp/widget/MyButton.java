package com.amirhossein.myKonkorApp.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;


public class MyButton extends Button {
    public MyButton(Context context) {
        super(context);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(),"Lalezar-Regular.otf");
        this.setTypeface(typeface);
    }

    public MyButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(),"Lalezar-Regular.otf");
        this.setTypeface(typeface);
    }

    public MyButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(),"Lalezar-Regular.otf");
        this.setTypeface(typeface);
    }
}
