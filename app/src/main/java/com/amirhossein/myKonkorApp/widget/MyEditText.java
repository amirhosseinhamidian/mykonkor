package com.amirhossein.myKonkorApp.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

public class MyEditText extends EditText {
    public MyEditText(Context context) {
        super(context);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(),"IRANSans .ttf");
        this.setTypeface(typeface);
    }

    public MyEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(),"IRANSans .ttf");
        this.setTypeface(typeface);
    }

    public MyEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(),"IRANSans .ttf");
        this.setTypeface(typeface);
    }
}
