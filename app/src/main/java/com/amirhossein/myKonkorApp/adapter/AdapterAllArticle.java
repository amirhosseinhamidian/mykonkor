package com.amirhossein.myKonkorApp.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;

import com.amirhossein.myKonkorApp.R;
import com.amirhossein.myKonkorApp.activity.ReadArticle;
import com.amirhossein.myKonkorApp.model.Article;
import com.amirhossein.myKonkorApp.widget.ShabnamTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AdapterAllArticle extends RecyclerView.Adapter<AdapterAllArticle.ViewHolder> {

    Context context;
    private List<Article> articles;
    private List<Article> articleFilter;

    public AdapterAllArticle(Context context, List<Article> articles) {
        this.context = context;
        this.articles = articles;
        this.articleFilter = articles;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.all_article_item , parent , false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Article article = articleFilter.get(position);
        holder.titleArticle.setText(article.getTitle());
        holder.dateArticle.setText(article.getDate());
        Picasso.get().load(article.getImage()).into(holder.imageArticle);

        holder.itemView.setOnClickListener(view -> {
            Intent intent = new Intent(context , ReadArticle.class);
            intent.putExtra("id",article.getId());
            intent.putExtra("title",article.getTitle());
            intent.putExtra("image",article.getImage());
            intent.putExtra("date",article.getDate());
            intent.putExtra("source",article.getSource());
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return articleFilter.size();
    }

     public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageArticle;
        ShabnamTextView titleArticle , dateArticle;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageArticle = itemView.findViewById(R.id.img_article_list_all);
            titleArticle = itemView.findViewById(R.id.tv_title_article_list_all);
            dateArticle = itemView.findViewById(R.id.tv_date_article_list_all);
        }
    }

    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    articleFilter = articles;
                } else {
                    List<Article> filteredList = new ArrayList<>();
                    for (Article row : articles) {

                        if (row.getTitle().toLowerCase().contains(charString.toLowerCase()) ) {
                            filteredList.add(row);
                        }
                    }

                    articleFilter = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = articleFilter;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                articleFilter = (ArrayList<Article>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
