package com.amirhossein.myKonkorApp.adapter;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.amirhossein.myKonkorApp.R;
import com.amirhossein.myKonkorApp.model.Analytic;
import com.amirhossein.myKonkorApp.widget.IranSansBold;
import com.github.lzyzsd.circleprogress.DonutProgress;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AdapterAnalytic extends RecyclerView.Adapter<AdapterAnalytic.ViewHolder>{
    Context context;
    private List<Analytic> analyticList;
    private List<Float> percentList;

    public AdapterAnalytic(Context context, List<Analytic> analyticList,List<Float> percentList) {
        this.context = context;
        this.analyticList = analyticList;
        this.percentList = percentList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.analytic_item, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("ObjectAnimatorBinding")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Analytic analytic = analyticList.get(position);
        holder.textViewName.setText(analytic.getTopic());
        holder.textViewAll.setText(String.valueOf(analytic.getTotal()));
        holder.textViewCorrect.setText(String.valueOf(analytic.getCorrect()));
        holder.textViewWrong.setText(String.valueOf(analytic.getWrong()));
        float result = percentList.get(position);
        if (!String.valueOf(result).equals("NaN")){
            result = round(result,2);
        }else {
            result = 0;
        }


        ObjectAnimator progressAnimator;
        if (result>0){
            progressAnimator = ObjectAnimator.ofFloat(holder.progressPositive,"progress",0,result);
            holder.progressPositive.setText(result+"%");
            holder.progressPositive.setTextSize(50);
            holder.progressPositive.setStartingDegree(180);

            progressAnimator.setDuration(1800);
            progressAnimator.start();
        }else if (result<0){
            float minus = result*(-3);

            progressAnimator = ObjectAnimator.ofFloat(holder.progressNegative,"progress",0,minus);
            holder.progressPositive.setVisibility(View.GONE);
            holder.progressNegative.setVisibility(View.VISIBLE);
            holder.progressNegative.setText(result+"%");
            holder.progressNegative.setTextSize(50);
            holder.progressNegative.setStartingDegree(180);

            progressAnimator.setDuration(1800);
            progressAnimator.start();

        }

    }


    @Override
    public int getItemCount() {
        return analyticList.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {
        DonutProgress progressPositive,progressNegative;
        IranSansBold textViewAll,textViewCorrect,textViewWrong,textViewName;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            progressPositive = itemView.findViewById(R.id.progress_result_lesson_positive);
            progressNegative = itemView.findViewById(R.id.progress_result_lesson_negative);
            textViewAll = itemView.findViewById(R.id.tv_all_question);
            textViewCorrect = itemView.findViewById(R.id.tv_correct_lesson);
            textViewWrong = itemView.findViewById(R.id.tv_wrong_lesson);
            textViewName = itemView.findViewById(R.id.tv_name_lesson);
        }
    }

    private static float round(float value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return (float) bd.doubleValue();
    }


}
