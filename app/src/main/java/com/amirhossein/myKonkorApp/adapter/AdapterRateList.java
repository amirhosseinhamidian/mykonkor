package com.amirhossein.myKonkorApp.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.amirhossein.myKonkorApp.R;
import com.amirhossein.myKonkorApp.model.User;
import com.amirhossein.myKonkorApp.widget.ShabnamTextView;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.MODE_PRIVATE;
import static com.amirhossein.myKonkorApp.activity.Welcome.USER_INFO;
import static com.amirhossein.myKonkorApp.common.Constants.SIGN_IN_GOOGLE;
import static com.amirhossein.myKonkorApp.common.Constants.SIGN_IN_GUEST;

public class AdapterRateList extends RecyclerView.Adapter<AdapterRateList.ViewHolder> {

    private Context context;
    private List<User> rateList;
    private int size;
    private int codeRequest;

    public AdapterRateList(Context context, List<User> rateList, int size, int codeRequest) {
        this.context = context;
        this.rateList = rateList;
        this.size = size;
        this.codeRequest = codeRequest;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.rate_item, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        SharedPreferences prefer = context.getSharedPreferences(USER_INFO,MODE_PRIVATE);
        String typeSignIn = prefer.getString("signIn",SIGN_IN_GUEST);
        String email = prefer.getString("email","");

        @SuppressLint("HardwareIds")
        String android_id = Settings.Secure.getString(Objects.requireNonNull(context).getContentResolver(),
                Settings.Secure.ANDROID_ID);
        User rate = rateList.get(position);
        holder.textViewName.setText(rate.getUsername());
        holder.textViewNumber.setText(position+1+"");
        Picasso.get()
                .load(rate.getImage())
                .placeholder(R.drawable.notprofile)
                .error(R.drawable.notprofile)
                .into(holder.imageProfile);

        if (typeSignIn.equals(SIGN_IN_GOOGLE)){
            if ((rate.getEmail() + "").equals(email)){
                holder.linearLayout.setBackgroundResource(R.color.colorPrimaryLightSecond);
            }
            if (codeRequest==1){
                holder.textViewScore.setText(rate.getScoreWeek()+"");
            }else if (codeRequest==2){
                holder.textViewScore.setText(rate.getScoreMonth()+"");
            }else if (codeRequest==3){
                holder.textViewScore.setText(rate.getScoreTotal()+"");
            }

        }else {
            if ((rate.getAndroidId()+"").equals(android_id)){
                holder.linearLayout.setBackgroundResource(R.color.colorPrimaryLightSecond);
            }
            if (codeRequest==1){
                holder.textViewScore.setText(rate.getScoreWeek()+"");
            }else if (codeRequest==2){
                holder.textViewScore.setText(rate.getScoreMonth()+"");
            }else if (codeRequest==3){
                holder.textViewScore.setText(rate.getScoreTotal()+"");
            }
        }



    }

    @Override
    public int getItemCount() {
        return size;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ShabnamTextView textViewScore, textViewNumber, textViewName;
        LinearLayout linearLayout;
        CircleImageView imageProfile;
        ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.tv_name_rate);
            textViewNumber = itemView.findViewById(R.id.tv_num_rate);
            textViewScore = itemView.findViewById(R.id.tv_score_rate);
            linearLayout = itemView.findViewById(R.id.ll_rate_item);
            imageProfile = itemView.findViewById(R.id.img_profile_rate);
        }
    }
}
