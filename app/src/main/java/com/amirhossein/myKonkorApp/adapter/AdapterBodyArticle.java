package com.amirhossein.myKonkorApp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.amirhossein.myKonkorApp.R;
import com.amirhossein.myKonkorApp.model.BodyArticle;
import com.amirhossein.myKonkorApp.widget.IranSansBold;
import com.amirhossein.myKonkorApp.widget.ShabnamTextView;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AdapterBodyArticle extends RecyclerView.Adapter<AdapterBodyArticle.ViewHolder> {
    Context context;
    private List<BodyArticle> bodyArticleList;

    public AdapterBodyArticle(Context context, List<BodyArticle> bodyArticleList) {
        this.context = context;
        this.bodyArticleList = bodyArticleList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.body_article_item, parent , false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        BodyArticle bodyArticle = bodyArticleList.get(position);
        if (bodyArticle.getTitle_case() != null){
            holder.tv_title_item.setVisibility(View.VISIBLE);
            holder.tv_title_item.setText(bodyArticle.getTitle_case());
        }
        if (bodyArticle.getDescription() != null) {
            holder.tv_desc_item.setVisibility(View.VISIBLE);
            holder.tv_desc_item.setText(bodyArticle.getDescription());
        }
        if (bodyArticle.getImage_case() != null){
            holder.img_item.setVisibility(View.VISIBLE);
            Picasso.get().load(bodyArticle.getImage_case()).into(holder.img_item);
        }
    }

    @Override
    public int getItemCount() {
        return bodyArticleList.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img_item;
        IranSansBold tv_title_item;
        ShabnamTextView tv_desc_item;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img_item = itemView.findViewById(R.id.img_case_article);
            tv_title_item = itemView.findViewById(R.id.tv_title_case_article);
            tv_desc_item = itemView.findViewById(R.id.tv_desc_case_article);
        }
    }
}
