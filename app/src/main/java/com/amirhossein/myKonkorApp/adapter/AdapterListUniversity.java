package com.amirhossein.myKonkorApp.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.amirhossein.myKonkorApp.R;
import com.amirhossein.myKonkorApp.activity.ReadUniversity;
import com.amirhossein.myKonkorApp.model.University;
import com.amirhossein.myKonkorApp.widget.ShabnamTextView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import static com.amirhossein.myKonkorApp.activity.ReadUniversity.PREFS_UNIVERSITY;

public class AdapterListUniversity extends RecyclerView.Adapter<AdapterListUniversity.ViewHolder>  {

    private Context context;
    private List<University> universityList;
    private List<University> universityFilter;



    public AdapterListUniversity(Context context, List<University> universityList) {
        this.context = context;
        this.universityList = universityList;
        this.universityFilter = universityList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.university_list_itme , parent , false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        University university = universityFilter.get(position);
        holder.name.setText(university.getName());
        holder.city.setText(university.getCity());
        holder.state.setText(university.getState());
        holder.rate.setText(university.getRating());
        int resourceId = context.getResources().getIdentifier(university.getLogo(),"drawable",context.getPackageName());
        holder.logo.setImageResource(resourceId);

        holder.itemView.setOnClickListener(view -> {
            YoYo.with(Techniques.Pulse)
                    .duration(200)
                    .repeat(0)
                    .playOn(view);

            SharedPreferences preferences = context.getSharedPreferences(PREFS_UNIVERSITY,Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();

            editor.apply();

            Intent intent = new Intent(context , ReadUniversity.class);
            intent.putExtra("id",university.getId());
            intent.putExtra("position",position);
            intent.putExtra("rate",university.getRating());
            intent.putExtra("name",university.getName());
            intent.putExtra("city",university.getCity());
            intent.putExtra("state",university.getState());
            context.startActivity(intent);
        });

    }

    @Override
    public int getItemCount() {
        return universityFilter.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView logo;
        ShabnamTextView name , city , state , rate;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            logo = itemView.findViewById(R.id.img_logo_university);
            name = itemView.findViewById(R.id.tv_name_university_list);
            city = itemView.findViewById(R.id.tv_city_university_list);
            state = itemView.findViewById(R.id.tv_state_university_list);
            rate = itemView.findViewById(R.id.tv_rate_university_list);

        }
    }


    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    universityFilter = universityList;
                } else {
                    List<University> filteredList = new ArrayList<>();
                    for (University row : universityList) {

                        if (row.getName().toLowerCase().contains(charString.toLowerCase()) ) {
                            filteredList.add(row);
                        }
                    }

                    universityFilter = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = universityFilter;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                universityFilter = (ArrayList<University>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
