package com.amirhossein.myKonkorApp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.amirhossein.myKonkorApp.R;
import com.amirhossein.myKonkorApp.widget.IranSansBold;
import com.amirhossein.myKonkorApp.widget.LalezarTextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AdapterResultList extends RecyclerView.Adapter<AdapterResultList.ViewHolder> {
    private Context context;
    private List<String> correctList;
    private List<String> userAnswerList;

    public AdapterResultList(Context context, List<String> correctList, List<String> userAnswerList) {
        this.context = context;
        this.correctList = correctList;
        this.userAnswerList = userAnswerList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.result_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewNum.setText(String.valueOf(position+1));
        String correct = correctList.get(position);
        String userAnswer = userAnswerList.get(position);

        switch (correct) {
            case "A":
                holder.textViewFirst.setBackgroundResource(R.drawable.background_result_item_success);
                holder.textViewFirst.setTextColor(Color.WHITE);
                switch (userAnswer) {
                    case "B":
                        holder.textViewSecond.setBackgroundResource(R.drawable.background_result_item_error);
                        holder.textViewSecond.setTextColor(Color.WHITE);

                        break;
                    case "C":
                        holder.textViewThird.setBackgroundResource(R.drawable.background_result_item_error);
                        holder.textViewThird.setTextColor(Color.WHITE);

                        break;
                    case "D":
                        holder.textViewForth.setBackgroundResource(R.drawable.background_result_item_error);
                        holder.textViewForth.setTextColor(Color.WHITE);
                        break;

                    case "notAns":
                        holder.textViewFirst.setBackgroundResource(R.drawable.background_result_item_not);
                        holder.textViewFirst.setTextColor(Color.WHITE);
                }
                break;

            case "B":
                holder.textViewSecond.setBackgroundResource(R.drawable.background_result_item_success);
                holder.textViewSecond.setTextColor(Color.WHITE);

                switch (userAnswer) {
                    case "A":
                        holder.textViewFirst.setBackgroundResource(R.drawable.background_result_item_error);
                        holder.textViewFirst.setTextColor(Color.WHITE);
                        break;
                    case "C":
                        holder.textViewThird.setBackgroundResource(R.drawable.background_result_item_error);
                        holder.textViewThird.setTextColor(Color.WHITE);
                        break;
                    case "D":
                        holder.textViewForth.setBackgroundResource(R.drawable.background_result_item_error);
                        holder.textViewForth.setTextColor(Color.WHITE);
                        break;
                    case "notAns":
                        holder.textViewSecond.setBackgroundResource(R.drawable.background_result_item_not);
                        holder.textViewSecond.setTextColor(Color.WHITE);
                }

                break;
            case "C":
                holder.textViewThird.setBackgroundResource(R.drawable.background_result_item_success);
                holder.textViewThird.setTextColor(Color.WHITE);
                switch (userAnswer) {
                    case "A":
                        holder.textViewFirst.setBackgroundResource(R.drawable.background_result_item_error);
                        holder.textViewFirst.setTextColor(Color.WHITE);
                        break;
                    case "B":
                        holder.textViewSecond.setBackgroundResource(R.drawable.background_result_item_error);
                        holder.textViewSecond.setTextColor(Color.WHITE);
                        break;
                    case "D":
                        holder.textViewForth.setBackgroundResource(R.drawable.background_result_item_error);
                        holder.textViewForth.setTextColor(Color.WHITE);
                        break;
                    case "notAns":
                        holder.textViewThird.setBackgroundResource(R.drawable.background_result_item_not);
                        holder.textViewThird.setTextColor(Color.WHITE);
                }

                break;
            case "D":
                holder.textViewForth.setBackgroundResource(R.drawable.background_result_item_success);
                holder.textViewForth.setTextColor(Color.WHITE);
                switch (userAnswer) {
                    case "A":
                        holder.textViewFirst.setBackgroundResource(R.drawable.background_result_item_error);
                        holder.textViewFirst.setTextColor(Color.WHITE);
                        break;
                    case "B":
                        holder.textViewSecond.setBackgroundResource(R.drawable.background_result_item_error);
                        holder.textViewSecond.setTextColor(Color.WHITE);
                        break;
                    case "C":
                        holder.textViewThird.setBackgroundResource(R.drawable.background_result_item_error);
                        holder.textViewThird.setTextColor(Color.WHITE);
                        break;
                    case "notAns":
                        holder.textViewForth.setBackgroundResource(R.drawable.background_result_item_not);
                        holder.textViewForth.setTextColor(Color.WHITE);
                }

                break;


        }


    }

    @Override
    public int getItemCount() {
        return correctList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        IranSansBold textViewNum;
        LalezarTextView textViewFirst,textViewSecond,textViewThird,textViewForth;
        ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewFirst = itemView.findViewById(R.id.tv_first_answer_result);
            textViewSecond = itemView.findViewById(R.id.tv_second_answer_result);
            textViewThird = itemView.findViewById(R.id.tv_third_answer_result);
            textViewForth = itemView.findViewById(R.id.tv_forth_answer_result);
            textViewNum = itemView.findViewById(R.id.tv_num_test_result);
        }
    }
}
