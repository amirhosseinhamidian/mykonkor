package com.amirhossein.myKonkorApp.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.amirhossein.myKonkorApp.R;
import com.amirhossein.myKonkorApp.model.DatabaseHelperScore;
import com.amirhossein.myKonkorApp.model.Score;
import com.amirhossein.myKonkorApp.widget.IranSansBold;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AdapterDailyRate extends RecyclerView.Adapter<AdapterDailyRate.ViewHolder>{
    Context context;
    private List<Score> scoreList;

    public AdapterDailyRate(Context context, List<Score> scoreList) {
        this.context = context;
        this.scoreList = scoreList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.daily_rate_item, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Score score = scoreList.get(position);
        holder.textViewScore.setText(score.getTotalScore()+"");
        holder.textViewDay.setText("روز "+score.getDay());
        DatabaseHelperScore db = new DatabaseHelperScore(context);
        if (score.getTotalScore()==db.getHighScore().getTotalScore() && score.getDay()!=1){
            holder.imageViewHigh.setVisibility(View.VISIBLE);
            holder.linearLayout.setBackgroundResource(R.drawable.background_item_daily_rate_high_score);
            holder.textViewDay.setTextColor(Color.parseColor("#00B020"));
        }
        if (score.getDay()==db.getLastCounters().getDay()){
            holder.textViewDay.setText("امروز");
        }
    }

    @Override
    public int getItemCount() {
        return scoreList.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {
        IranSansBold textViewDay,textViewScore;
        ImageView imageViewHigh;
        LinearLayout linearLayout;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewDay = itemView.findViewById(R.id.tv_day_item_rate);
            textViewScore = itemView.findViewById(R.id.tv_score_item_rate_day);
            imageViewHigh = itemView.findViewById(R.id.img_high_score);
            linearLayout = itemView.findViewById(R.id.ll_item_day_rate);
        }
    }
}
