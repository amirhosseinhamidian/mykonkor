package com.amirhossein.myKonkorApp.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.amirhossein.myKonkorApp.R;
import com.amirhossein.myKonkorApp.activity.ReadField;
import com.amirhossein.myKonkorApp.model.Field;
import com.amirhossein.myKonkorApp.widget.ShabnamTextView;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import static com.amirhossein.myKonkorApp.activity.Welcome.USER_INFO;

public class AdapterListField extends RecyclerView.Adapter<AdapterListField.ViewHolder>  {

    private Context context;
    private List<Field> fields;

    public AdapterListField(Context context, List<Field> fields) {
        this.context = context;
        this.fields = fields;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_field_item, parent , false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Field field = fields.get(position);
        holder.tv_title_field_math.setText(field.getTitle());

        String imageBaseUrl="http://amirhosseinhamidian.ir/image/field_short/";
        Picasso.get().load(imageBaseUrl+field.getImageShort()).into(holder.img_field_math);

        holder.itemView.setOnClickListener(view -> {
            Intent intent = new Intent(context, ReadField.class);
            intent.putExtra("idField",position);
            intent.putExtra("titleField",field.getTitle());
            intent.putExtra("imageField",field.getImageLarge());
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return fields.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img_field_math;
        ShabnamTextView tv_title_field_math;
        ViewHolder(@NonNull View itemView) {
            super(itemView);
            img_field_math = itemView.findViewById(R.id.img_field_math);
            tv_title_field_math = itemView.findViewById(R.id.tv_title_field_math);
        }
    }

}
