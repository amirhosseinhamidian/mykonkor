package com.amirhossein.myKonkorApp.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.amirhossein.myKonkorApp.R;
import com.amirhossein.myKonkorApp.activity.ReadField;
import com.amirhossein.myKonkorApp.model.Field;
import com.amirhossein.myKonkorApp.widget.ShabnamTextView;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AdapterEmpiricalField extends RecyclerView.Adapter<AdapterEmpiricalField.ViewHolder> {

    private Context context;
    private List<Field> fields;

    public AdapterEmpiricalField(Context context, List<Field> fields) {
        this.context = context;
        this.fields = fields;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.empirical_field_item , parent ,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Field field = fields.get(position);
        holder.tv_title_field_empirical.setText(field.getTitle());
        Picasso.get()
                .load(field.getImageLarge())
                .into(holder.img_field_empirical);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ReadField.class);
                intent.putExtra("idField",field.getId());
                intent.putExtra("titleField",field.getTitle());
                intent.putExtra("imageField",field.getImageLarge());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return fields.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img_field_empirical;
        ShabnamTextView tv_title_field_empirical;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img_field_empirical = itemView.findViewById(R.id.img_field_empirical);
            tv_title_field_empirical = itemView.findViewById(R.id.tv_title_field_empirical);
        }
    }
}
