package com.amirhossein.myKonkorApp.common;

public class Constants {

    public static final String ACTION = ".action";
    public static final String DATA_KEY_1 = ".data_key_1";
    public static final String DATA_KEY_2 = ".data_key_2";

    public static final String TAB_HOME = "fragment_home";
    public static final String TAB_ARTICLE = "fragment_article";
    public static final String TAB_RATING = "fragment_rating";
    public static final String TAB_PROFILE = "fragment_profile";

    public static final String HOME_FRAGMENT = "FragmentHome";
    public static final String ARTICLE_FRAGMENT = "FragmentArticle";
    public static final String RATING_FRAGMENT = "FragmentRating";
    public static final String PROFILE_FRAGMENT = "FragmentProfile";

    public static final String EXTRA_IS_ROOT_FRAGMENT = ".extra_is_root_fragment";

    public static final String SIGN_IN_GOOGLE = "sign_in_google";
    public static final String SIGN_IN_GUEST = "sign_in_guest";
    public static final String NOT_PROFILE_PICTURE = "not profile";
    public static final String EDIT_PROFILE_PICTURE = "edit profile";
    public static final String SOUND = "sound";


}
